#OUTDATED VERSION!
# About SADE
The Scalable Architecture for Digital Editions (SADE) tries to meet the requirement for an easy to use publication system for electronic resources and Digital Editions. It is an attempt to provide a modular concept for publishing scholarly editions in a digital medium, based on open standards. Furthermore it is a distribution of open source software tools for the easy publication of Digital Editions and digitized texts in general out of the box. SADE is a scalable system that can be adapted by different projects which follow the TEI guidelines or other XML based formats.

This is the modified version for Fontane Notizbücher.

# Fontane-Build
* call `ant sade-dist-build` to build the latest supported eXist db and all SADE packages
* run `./dist-utils/antbuild/build/sade/bin/startup.sh`
* deploy the xar package from `SADE-Project-develop`
  * deployment needs approx. 45 min due to republication of all the data. this is because the TEI documents needs to be stored from in memory, anything else will place whitespaces in the rendered xhtml output.
  * there is fastdeploy build target that includes all non TEI-Data + Register and a single notebook (C07). deployment needs about 10 min.
* see http://localhost:8080/exist/apps/SADE/textgrid/index.html

## Tests
- [Markdown rendering](http://localhost:8080/exist/apps/SADE/textgrid/content.html?id=institutionen.md)
- [Dokuwiki (w/o reload)](http://localhost:8080/exist/apps/SADE/textgrid/doku.html)
- [Dokuwiki (w reload)](http://localhost:8080/exist/apps/SADE/textgrid/doku.html?id=gesamtdokumentation_ii._tei-header)
- [Image reload](http://localhost:8080/exist/apps/SADE/textgrid/doku.html?id=gesamtdokumentation_iii.3.21&reload)
- [Search](http://localhost:8080/exist/apps/SADE/textgrid/results.html?q=luther)
- [Literaturverzeichnis](http://localhost:8080/exist/apps/SADE/textgrid/literaturvz.html)
- [Register](http://localhost:8080/exist/apps/SADE/textgrid/register.html)
- [Auth on develop (see cookie)](http://localhost:8080/exist/apps/SADE/textgrid/edition.html?id=/xml/data/16b00.xml&page=)
- [TEI rendering](http://localhost:8080/exist/apps/SADE/textgrid/edition.html?id=/xml/data/16b00.xml&page=1r)
  - [Cover](http://localhost:8080/exist/apps/SADE/textgrid/edition.html?id=/xml/data/16b00.xml&page=)
  - [TBLE](http://localhost:8080/exist/apps/SADE/textgrid/edition.html?id=/xml/data/16b00.xml&page=60v)
  - [single page]()
- [XML-View](http://localhost:8080/exist/apps/SADE/textgrid/xml.html?id=/xml/data/16b00.xml)
- [REST](http://localhost:8080/exist/apps/SADE/textgrid/rest/data/16b00.xml)
