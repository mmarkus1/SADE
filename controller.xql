xquery version "3.1";

import module namespace config="http://textgrid.de/ns/SADE/config" at "modules/config/config.xqm";
import module namespace digilib="http://textgrid.info/namespaces/xquery/digilib" at "../textgrid-connect/digilibProxy.xqm";
import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo" at "modules/fontane/transform.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace app="http://textgrid.de/ns/SADE/templates" at "modules/app.xql";

declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;



let $project := tokenize( $exist:path, "/" )[. != ""][not(ends-with(., "html"))][1]
let $listproject := config:list-projects()[1]
return

    (: forward requests without resources to index.html :)
if ($exist:path eq '') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{request:get-uri()}/"/>
    </dispatch>

    (: forward requests without project to the first project listed :)
else if (empty($project)) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{config:list-projects()[1]}/index.html"/>
    </dispatch>

    (: forward requests without resources to index.html :)
else if (ends-with($exist:path, "/") or empty($exist:resource)) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.html"/>
    </dispatch>

    (: forward REST requests to data collection of the project:)
else if (tokenize($exist:path, '/') = "rest") then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="/../sade-projects/{$project}/data/xml/{substring-after($exist:path, "/rest/")}"/>
    </dispatch>

    (: forward DIGILIB requests to the proxy function :)
else if (tokenize($exist:path, '/') = "digilib") then
    let $path := tokenize($exist:path, '/'),
        $id := $path[last()],
        $if-modified-since := request:get-header("if-modified-since")
    return
        digilib:proxy($project, $id, $if-modified-since)

    (: REST interface of the interface! :)
else if (tokenize($exist:path, '/') = "get") then
    let $reqid := request:get-parameter("id", "/xml/data/16b00.xml"),
        $id := $reqid => substring-after("/xml/data/") => substring-before(".xml"),
        $pagereq := request:get-parameter("page", "1r"),
        $page := if( $pagereq = "" ) then "outer_front_cover" else $pagereq,
        $docCollection := $config:projects-dir || $project ||"/data/xml/xhtml/"|| $id ||"/",
        $docpath := $docCollection || $page ||".xml",
        $doc := doc( $docpath ),
        $content :=
            switch ($exist:resource)
                case "code.html" return
                    replace(serialize($doc//xhtml:div[@class="teixml"]), "xhtml:", "")

                case "trans.html" return
                    replace(serialize($doc/xhtml:body/xhtml:div/xhtml:div[not(@class="teixml")][not(@class="facs")]), "xhtml:", "")

                case "facs.html" return
                    replace(serialize($doc//xhtml:div[@class="facs"]), "xhtml:", "")

                case "toc.html" return
                    replace(serialize(doc( $docCollection || "toc.xml" )), "xhtml:", "")

                case "comm.html" return
                    replace(serialize($doc//xhtml:div[@class="notes"]), "xhtml:", "")

                default return "supported requests are: facs, trans, code"
        return
            response:stream($content, "method=text media-type=text/plain")

    (: the important stuff :)
else if (ends-with($exist:resource, ".html")) then
    let $doc-path := $config:projects-dir || $project || config:get("template") || $exist:resource
    return
    (: the html page is run through view.xql to expand templates :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <!--view-->
            <forward url="{$exist:controller}/modules/view.xql" method="get">
                <add-parameter name="doc-path" value="{$doc-path}"/>
                <add-parameter name="lang" value="{app:getLanguage()}"/>
            </forward>
        <!--/view-->
		<error-handler>
			<forward url="{$exist:controller}/error-page.html" method="get"/>
			<forward url="{$exist:controller}/modules/view.xql"/>
		</error-handler>
    </dispatch>

    (: resource paths starting with $shared are loaded from the shared-resources app :)
else if (contains($exist:path, "/$shared/")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="/shared-resources/{substring-after($exist:path, '/$shared/')}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>

else
    (:  check if the resource comes from the template or the data
        we try to leave the template manipulation as minimal as
        possible
    :)
    let $path := "/../sade-projects/"|| $project || "/" || config:get("data-dir") || "/" || substring-after($exist:path, $project)
    let $path := if( doc-available( $path ) ) then $path else "/../sade-projects/"|| $project || config:get("template") || substring-after($exist:path, $project)
    return
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
        <forward url="{$path}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>
