xquery version "3.1";

module namespace app="http://textgrid.de/ns/SADE/templates";

import module namespace templates="http://exist-db.org/xquery/templates" ;
import module namespace config="http://textgrid.de/ns/SADE/config" at "config/config.xqm";
import module namespace console="http://exist-db.org/xquery/console";
declare namespace functx = "http://www.functx.com";

declare function functx:substring-before-if-contains
  ( $arg as xs:string? ,
    $delim as xs:string )  as xs:string? {

   if (contains($arg,$delim))
   then substring-before($arg,$delim)
   else $arg
 } ;

(:~
 : returns an html snippet with the current project title from conf.xml, can be
 : can be a clickable title within a navbar that triggers a sidebar
 :
 : @param $node the HTML node with the attribute which triggered this call
 : @param $model a map containing arbitrary data - used to pass information between template calls
:)
declare function app:project-id($node as node(), $model as map(*)) {
    element {$node/name()} { $node/@*[not(local-name() => starts-with("data-template")) ],
<div data-target=".sidebar-collapse" data-toggle="collapse" id="sidebar-toggle">
<span class="glyphicon glyphicon-list"> </span> {' ' || config:get('project-title')}
</div>}
};

(:~
 : Collects information from <a href="https://www.uni-goettingen.de/de/publikationen/303721.html">
 : Fontane-Arbeitstelle website</a>.
 :
 : @param $node the HTML node with the attribute which triggered this call
 : @param $model a map containing arbitrary data - used to pass information between template calls
 :)
declare function app:publications($node as node(), $model as map(*)) {
httpclient:get(xs:anyURI('https://www.uni-goettingen.de/de/publikationen/303721.html'), true(), ())//ul[@class="txtlist"][1]/li[position() < 4]
};

(:~
 : Collects information from <a href="https://www.uni-goettingen.de/de/publikationen/303721.html">
 : Fontane-Arbeitstelle website</a>.
 :
 : @param $node the HTML node with the attribute which triggered this call
 : @param $model a map containing arbitrary data - used to pass information between template calls
 :)
declare function app:presentations($node as node(), $model as map(*)) {
for $item in httpclient:get(xs:anyURI('https://www.uni-goettingen.de/de/vortr%C3%A4ge-und-pr%C3%A4sentationen/303717.html'), true(), ())//ul[@class="txtlist"][1]/li[position() < 5]
return
    element li { for $i in $item/node() return local:nodeTest($i) }
};

declare function local:nodeTest($node){
typeswitch ($node)
    case element(a) return element a {
        attribute href {if(starts-with($node/@href, '/de/document')) then 'https://www.uni-goettingen.de'||$node/@href else $node/@href},
        attribute target {'_blank'},
        if (starts-with($node/text(), 'Power Point' )) then substring-after($node/text(), 'Power Point') else $node/text()
    }
    default return $node
};

declare function app:getLanguage() {
    let $http-header := request:get-header("Accept-Language")
    let $http-header := functx:substring-before-if-contains($http-header,"-")
    let $http-header := functx:substring-before-if-contains($http-header,";")
    let $http-header := functx:substring-before-if-contains($http-header,",")
    let $param-lang := request:get-parameter("lang", "")
    let $lang := if ($param-lang != "") then ($param-lang) else ($http-header)
    let $langConfigured := (config:get("lang.default"), tokenize(config:get("lang.alt"), ";"))
    let $result := if( $lang = $langConfigured ) then $lang else $langConfigured[1]

    return ($result)
};

declare function app:getAllLanguages() {
    let $languages := config:get("lang.default")
    let $languages := fn:insert-before($languages,0,tokenize(config:get("lang.alt"), ";"))
    return $languages
};
