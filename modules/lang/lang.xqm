xquery version "3.1";

module namespace lang="http://sade/lang";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace app="http://textgrid.de/ns/SADE/templates" at "../app.xql";
declare namespace functx = "http://www.functx.com";

declare variable $lang:lang := app:getLanguage();

declare function lang:translate( $node as node(), $model as map(*), $content as xs:string ) as node() {
    typeswitch($node)
        case element (span) return element {name($node)} {
            doc("/db/sade-projects/textgrid/lang.xml" )//word[@key=$content]/lang[@key=$lang:lang]
        }
        (: Language stuff for search in navbar :)
        case element (div) return element div {
            attribute class {"form-group"},
            <input id="searchInput" type="text" class="form-control" placeholder="   {doc("/db/sade-projects/textgrid/lang.xml" )//word[@key="Search"]/lang[@key=$lang:lang]}" data-template="templates:form-control" name="q"/>,
            <input id="searchLang" type="hidden" name="lang" value="{$lang:lang}"/>
        }
        case element (input) return element input {
            (: Language stuff for search in results.html :)
            if ($content = "SearchResults") then (
                attribute id {"searchLang"},
                attribute type {"hidden"},
                attribute name {"lang"},
                attribute value {$lang:lang}
            )
            (: Placeholder stuff for input items :)
            else (
                attribute placeholder {doc("/db/sade-projects/textgrid/lang.xml" )//word[@key=$content]/lang[@key=$lang:lang]},
                for $att in $node/@*
                    let $att-name := name($att)
                    return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                    else ()
            )
        }
        case element (textarea) return element textarea {
            attribute placeholder {doc("/db/sade-projects/textgrid/lang.xml" )//word[@key=$content]/lang[@key=$lang:lang]},
            for $att in $node/@*
                let $att-name := name($att)
                return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                else ()
        }
        case element (button) return element button {
            for $att in $node/@*
                let $att-name := name($att)
                return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                else (),
            doc("/db/sade-projects/textgrid/lang.xml" )//word[@key=$content]/lang[@key=$lang:lang]
        }
        default return element {name($node)} {console:log("default return error")}
};

declare function functx:update-attributes
  ( $elements as element()* ,
    $attrNames as xs:QName* ,
    $attrValues as xs:anyAtomicType* )  as element()? {

   for $element in $elements
   return element { node-name($element)}
                  { for $attrName at $seq in $attrNames
                    return if ($element/@*[node-name(.) = $attrName])
                           then attribute {$attrName}
                                     {$attrValues[$seq]}
                           else (),
                    $element/@*[not(node-name(.) = $attrNames)],
                    $element/node() }
 } ;
