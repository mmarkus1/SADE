xquery version "3.1";

module namespace blog="http://sade/blog";
declare namespace content="http://purl.org/rss/1.0/modules/content/";

declare option exist:serialize "method=xhtml media-type=text/html";
declare variable $blog:titleLength := 34;
declare variable $blog:descriptionLength := 120;
declare variable $blog:maxNumberItems := 3;

declare function local:getTitle($title as xs:string) {
    let $titleTooLong := if (fn:string-length($title) > $blog:titleLength) then true() else false()
    let $result := if ($titleTooLong) then substring($title,1,$blog:titleLength) || "..." else $title
    return $result
};

declare function local:getDescription($desc as xs:string) {
    let $result := local:parseCharErrors($desc)
    let $descTooLong := if (fn:string-length($result) > $blog:descriptionLength) then true() else false()
    let $result := if ($descTooLong) then substring($result,1,$blog:descriptionLength) || "..." else $result
    return $result
};

declare function blog:readBlog($node as node(), $model as map(*)) as node() {
    let $news := httpclient:get(xs:anyURI("https://architrave.hypotheses.org/feed"),false(),())//httpclient:body/*
    let $dateTime := $news/channel/lastBuildDate


    return element div {
        for $newsItem in $news/channel/item[position() < ($blog:maxNumberItems + 1)]
            let $content := $newsItem//content:encoded
            let $content := util:parse-html( "<top>" || $content || "</top>")
            let $imgCount := count($content//img)
            let $img := ($content//img)[1]/@src
            let $space := " "
            return if ($imgCount > 0) then (
                <div class="media">
                    <div class="media-left">
                        <a href="{$newsItem/link}">
                            <img class="media-object" src="{$img}" width="75px" alt="..."/>
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="{$newsItem/link}">
                            <h4 class="media-heading">{local:getTitle($newsItem/title)}</h4>
                        </a>
                        <p style="font-size:13px;color:#777777">
                            <i class="fa fa-calendar" aria-hidden="true"/>
                            {$space} {local:getRSSDate($newsItem/pubDate/text())}
                        </p>
                        <p>{local:getDescription($newsItem/description)}</p>
                    </div>
                </div>)
            else (
                <div class="media">
                    <div class="media-left">
                        <a href="{$newsItem/link}">
                            <i class="fa fa-rss" style="font-size: 4em;width:75px"></i>
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="{$newsItem/link}">
                            <h4 class="media-heading">{local:getTitle($newsItem/title)}</h4>
                        </a>
                        <p style="font-size:13px;color:#777777">
                            <i class="fa fa-calendar" aria-hidden="true"/>
                            {$space} {local:getRSSDate($newsItem/pubDate/text())}
                        </p>
                        <p>{local:getDescription($newsItem/description)}</p>
                    </div>
                </div>)
    }
};

declare function local:parseCharErrors($text) {
    let $result := replace($text,"&amp;#8217;", "'")
    return $result
};

declare function local:getRSSDate($rssInput as xs:string) as xs:string {
    let $rssInput := substring-before($rssInput,":")  
    let $rssInput := substring($rssInput,1,(string-length($rssInput)-2))
    return $rssInput
};