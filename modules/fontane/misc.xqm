xquery version "3.1";

module namespace f-misc="http://fontane-nb.dariah.eu/ns/SADE/misc";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare function f-misc:cite($node as node(), $model as map(*)) {
let $url := request:get-url(),
    $res := tokenize($url, '/')[last()],
    $id := request:get-parameter('id', ''),
    $page := request:get-parameter('page', 'outer_front_cover'),
    $page := if($page = '') then 'outer_front_cover' else $page,
    $page := switch ($page)
               case "outer_front_cover" return "vordere Einbanddecke außen"
               case "inner_front_cover" return "vordere Einbanddecke innen"
               case "inner_back_cover" return "hintere Einbanddecke außen"
               case "outer_back_cover" return "hintere Einbanddecke innen"
               case "spine" return "Buchrücken"
               default return 'Blatt '||$page,
    $doc := doc( config:get("data-dir")||$id),
    $title := $doc//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/string(),
    $version := string( ($doc//tei:TEI/tei:teiHeader/tei:revisionDesc/tei:change/text())[1] ),
    $date := current-date(),
    $date := day-from-date($date)||'.'||month-from-date($date)||'.'||year-from-date($date),
    $url := replace($url, 'localhost:8080/exist/apps/SADE/textgrid', 'fontane-nb.dariah.eu') ||(if (request:get-query-string()) then '?' || request:get-query-string() else '')

return
    switch ($res)
    case 'edition.html'
        return
            <span>
                <a href="#" data-toggle="modal" data-target="#citation">Zitationsempfehlung</a>
                <!-- Modal window: -->
                <div class="modal fade" id="citation" tabindex="-1" role="dialog" aria-labelledby="citationrecommendation" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">Zitationsempfehlung</h4>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-body">
                        Theodor Fontane: {$title}. Hrsg. von Gabriele Radecke.
                        In: Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition.
                        Hrsg. von Gabriele Radecke, {$page}.
                        {$url},
                        abgerufen am: {$date}
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End of modal window -->
            </span>
    case 'inhalt.html'
        return
            <span>
                <a href="#" data-toggle="modal" data-target="#citation">Zitationsempfehlung</a>
                <!-- Modal window: -->
                <div class="modal fade" id="citation" tabindex="-1" role="dialog" aria-labelledby="citationrecommendation" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">Zitationsempfehlung</h4>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-body">
                        Theodor Fontane: {$title} (Inhaltsverzeichnis). Hrsg. von Gabriele Radecke.
                        {$url},
                        abgerufen am: {$date}
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End of modal window -->
            </span>
    case 'doku.html'
        return
            <span>
                <a href="#" data-toggle="modal" data-target="#citation">Zitationsempfehlung</a>
                <!-- Modal window: -->
                <div class="modal fade" id="citation" tabindex="-1" role="dialog" aria-labelledby="citationrecommendation" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">Zitationsempfehlung</h4>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-body">
                        Gabriele Radecke unter Mitarbeit von Martin de la Iglesia und Mathias Göbel: Editorische Gesamtdokumentation.
                        Editorische Assistenz: Rahel Rami und Judith Michaelis. In: Theodor Fontane: Notizbücher. Digitale
                        genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke.
                        {$url}, abgerufen am {$date}
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End of modal window -->
            </span>
    default
        return
            <span>
                <a href="#" data-toggle="modal" data-target="#citation">Zitationsempfehlung</a>
                <!-- Modal window: -->
                <div class="modal fade" id="citation" tabindex="-1" role="dialog" aria-labelledby="citationrecommendation" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">Zitationsempfehlung</h4>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-body">
                        Theodor Fontane: Notizbücher. Hrsg. von Gabriele Radecke.<br/>
                        {$url}<br/>
                        abgerufen am: {$date}
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End of modal window -->
            </span>
};

declare function f-misc:kaesten($node as node(), $model as map(*)){
    let $q := request:get-parameter('n', '')
    let $q := if(string-length($q) = 1) then $q||"0" else $q
    let $num := if(matches($q, "\d+$")) then number(substring($q, 2, 2)) else 0
    let $q := substring($q, 1, 1)
    let $data-dir := config:get("data-dir")
return

    (<ul class="nav nav-tabs">
        {for $kasten in (('a', 'b', 'c', 'd', 'e'))
        return
            if ($q = $kasten)
            then
                <li class="active">
                    <a href="#{$kasten}" data-toggle="tab">Kasten {upper-case($kasten)}</a>
                </li>
            else
                <li>
                    <a href="#{$kasten}" data-toggle="tab">Kasten {upper-case($kasten)}</a>
                </li>
        }
    </ul>,
    <div class="tab-content">
    {
        for $kasten in (('a', 'b', 'c', 'd', 'e'))
            return
                if ($q = $kasten)
            then
        <div class="tab-pane active" id="{$kasten}">
            <div class="panel-group" id="accordion{upper-case($kasten)}" role="tablist" aria-multiselectable="true">
                    {f-misc:list( $data-dir || '/xml/meta', upper-case($kasten), $num)}
            </div>
        </div>
        else
        <div class="tab-pane" id="{$kasten}">
            <div class="panel-group" id="accordion{upper-case($kasten)}" role="tablist" aria-multiselectable="true">
                {f-misc:list($data-dir || '/xml/meta', upper-case($kasten), $num)}
            </div>
        </div>
    }
    </div>)
};
declare function f-misc:list($datadir, $param, $num as xs:integer) {
    let $tgnav:= doc('/db/sade-projects/textgrid/navigation-fontane.xml')

    for $item at $pos in distinct-values($tgnav//object[string(@type)="text/xml"][starts-with(string(@title), 'Notizbuch ' || $param)]/string(@title))
(:
 :  use these lines, when more than on revision is in the database
 :    let $maxRev:=  max($tgnav//object[@title = $item]/number(substring-after(@uri, '.'))),:)
(:    $uri:= $tgnav//object[@title = $item][number(substring-after(@uri, '.')) = $maxRev]/substring-after(@uri, 'textgrid:'):)
    let $uri:= $tgnav//object[string(@title) = $item]/substring-before(substring-after(@uri, 'textgrid:'), '.')
    let $thisIn := $param||$pos = $param||$num
    return
        <div class="panel panel-dark">
                    <div class="panel-heading" role="tab" id="heading{$param||$pos}">
                      <h3 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion{$param}" href="#collapse{$param||$pos}" aria-expanded="false" aria-controls="collapse{$param||$pos}">
                      { if (matches($item, '0\d')) then replace($item, '0', '') else $item }{if($param||$pos = ('C5', 'C6', 'C7')) then <xhtml:sup>beta</xhtml:sup> else ()}
                        </a>
                      </h3>
                    </div>
                    <div id="collapse{$param||$pos}" class="panel-collapse collapse{if ($thisIn) then ' in' else ()}" role="tabpanel" aria-labelledby="heading{$param||$pos}">
                    <div class="panel-body">
                    <div class="row">
                        <div class="col-md-9">
                                <a href="edition.html?id=/xml/data/{$uri[last()]}.xml&amp;page=">Synoptische Ansicht</a>
                                <span style="margin-left:20px;"/>
                                <form class="form-inline" style="display:inline;" action="edition.html" metod="get">
                                    <input type="hidden" name="id" value="/xml/data/{$uri[last()]}.xml"/>
                                    <div class="form-group">

                                    <input name="page" type="text" style="border-radius:0;border-style:solid;width:40px;" placeholder="1r" data-toggle="tooltip" data-placement="bottom" title="Blattnummer"/>
                                    <button type="submit" class="btn btn-default"><i class="fa fa-chevron-right"></i></button>

                                    </div>
                                </form>
                              <br/>
                                <ul>
                                    <li><a href="mirador.html?n={$param||$pos}">Digitalisate</a></li>
                                    <li><a href="edition.html?id=/xml/data/{$uri[last()]}.xml&amp;page="
                                    onclick="goldenState('trans')">Transkriptionsansicht</a></li>
                                    <li>Edierter Text/Textkritischer Apparat</li>
                                    <li>TEI/XML-Ansicht: <a href="edition.html?id=/xml/data/{$uri[last()]}.xml&amp;page="
                                    onclick="goldenState('code')"><i title="seitenweise" class="fa fa-file-o"></i></a> | <a href="xml.html?id=/xml/data/{$uri[last()]}.xml"><i title="gesamtes XML auf der Webseite betrachten" class="fa fa-file-code-o"></i></a> |<a target="_blank" href="/rest/data/{$uri[last()]}.xml">REST</a></li>
                                    <li>Kommentare und Register
                                        <ul>
                                            <li><a href="ueberblickskommentar.html?id=/xml/data/{$uri[last()]}.xml">Überblickskommentar</a></li>
                                            <li>Stellenkommentar</li>
                                            <li>Register
                                                <ul>
                                                    <li><a href="register.html?nb={$uri[last()]}&amp;r=253sx">Register der Personen und Werke</a></li>
                                                    <li><a href="register.html?nb={$uri[last()]}&amp;r=253t3">Register der Werke</a></li>
                                                    <li>Register der Werke Theodor Fontanes</li>
                                                    <li>Register der Periodika</li>
                                                    <li><a href="register.html?nb={$uri[last()]}&amp;r=253t2">Geographisches Register</a></li>
                                                    <li><a href="register.html?nb={$uri[last()]}&amp;r=253t0">Register der Ereignisse</a></li>
                                                    <li><a href="register.html?nb={$uri[last()]}&amp;r=253t1">Register der Institutionen und Körperschaften</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="inhalt.html?id=/xml/data/{$uri[last()]}.xml">Inhaltsverzeichnis</a></li>
                                </ul>
                        </div>
                        <div class="col-md-3">
                            <div id="thumb">{
                                let $img := "digilib/"||substring-after($item, ' ')||"_001.jpg?dh=350&amp;mo=jpg"
                                return
                                element xhtml:img {
                                    attribute class {"imgLazy"},
                                    attribute src {
                                        if ($thisIn) then $img else "/public/img/loader.svg"
                                    },
                                    if(not($thisIn)) then
                                        attribute data-original {$img}
                                    else ()
                                    }
                                }
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                  </div>
};

declare function f-misc:content($node as node(), $model as map(*), $id, $page){
<div id="nb-title" class="col-xs-3 col-md-2" title="reset layout">
    <h1 class="animated slideInLeft">
        <span>{f-misc:nbTitle(request:get-parameter('id', ''), $model)}
{if(matches($page, '\d+(r|v)')) then ': '||$page else ()}</span>
    </h1>
</div>,
<div id="facsBtn" class="col-xs-1 col-md-2{ if(contains(request:get-cookie-value('facs'), 'inactive')) then ' inactive' else ()}">
    <h1 class="animated slideInLeft">
        <span class="hidden-md hidden-lg"><i class="fa fa-file-image-o"></i></span>
        <span class="hidden-sm hidden-xs">Faksimile</span>
    </h1>
</div>,
<div id="transBtn" class="col-xs-1 col-md-2 col-md-offset-1 { if(contains(request:get-cookie-value('trans'), 'inactive')) then ' inactive' else ()} ">
    <h1 class="animated slideInLeft">
        <span class="hidden-md hidden-lg"><i class="fa fa-file-text-o"></i></span>
        <span class="hidden-sm hidden-xs">Transkription</span>
    </h1>
</div>,
<div id="xmlBtn" class="col-xs-1 col-md-2 col-md-offset-1 {if(contains(request:get-cookie-value('xml'), 'inactive')) then ' inactive' else ()}">
    <h1 class="animated slideInLeft">
        <span class="hidden-md hidden-lg"><i class="fa fa-file-code-o"></i></span>
        <span class="hidden-sm hidden-xs">XML</span>
    </h1>
</div>
};
declare function f-misc:buttons($node as node(), $model as map(*)){
let $cookies := request:get-cookie-names()
let $pagenum := request:get-parameter('page', 'outer_front_cover')
let $pagenum := if ($pagenum = '') then 'outer_front_cover' else $pagenum
let $page := doc( config:get("data-dir") || request:get-parameter('id', '/xml/data/16b00.xml'))//tei:surface[@n = $pagenum]
let $entities := distinct-values($page//tei:rs/substring-before(@ref, ':'))
let $rslistItems := for $e in $entities
                    order by $e
                    return
                        switch($e)
                            case 'psn' return <li class="psn rsHigh">Personen</li>
                            case 'plc' return <li class="plc rsHigh">Orte</li>
                            case 'wrk' return <li class="wrk rsHigh">Werke</li>
                            case 'eve' return <li class="eve rsHigh">Ereignisse</li>
                            case 'org' return <li class="org rsHigh">Institutionen</li>
                            default return <li class="none">Keine</li>
let $datelistItems := if($page//tei:date) then for $item in distinct-values( $page//tei:date/string(@when-iso) ) return <li>{$item}</li> else <li class="none">Keine</li>
let $reflistItems := if($page//tei:ref[not(parent::tei:figDesc)]) then for $item in distinct-values( $page//tei:ref[not(parent::tei:figDesc)]/string-join(., ' ') ) return <li>{$item}</li> else <li class="none">Keine</li>

return
switch ( config:get('sade.develop') )
    case "true" return
		<div class="row">
			<div id="rsBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'rs') or contains(request:get-cookie-value('rs'), 'inactive')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Entitäten</span>
				</h1>
				<div id="rsBtnRefine">
				    <ul>
				        {$rslistItems}
				    </ul>
				</div>
			</div>
			<div id="dateBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'date') or contains(request:get-cookie-value('date'), 'inactive')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Datierung</span>
				</h1>
				<div id="dateBtnRefine">
				    <ul>
				        {$datelistItems}
				    </ul>
				</div>
			</div>
			<div id="refBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'ref') or contains(request:get-cookie-value('ref'), 'none')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Referenzierungen</span>
				</h1>
				<div id="refBtnRefine">
				    <ul>
				        {$reflistItems}
				    </ul>
				</div>
			</div>
			<div id="rotateBtn" class="col-xs-2">
				<input
				    id="RotateSlide"
				    type="range"
				    min="0"
				    max="359"
				    step="5"
				    defaultValue="0"
				    style="height:0;"/>
			</div>
		</div>
		default return
		<div class="row">
			<div id="rsBtn" class="col-xs-3{ if(not(request:get-cookie-names() = 'rs') or contains(request:get-cookie-value('rs'), 'inactive')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Entitäten</span>
				</h1>
				<div id="rsBtnRefine">
				    <ul>
				        {$rslistItems}
				    </ul>
				</div>
			</div>
			<div id="dateBtn" class="col-xs-3{ if(not(request:get-cookie-names() = 'date') or contains(request:get-cookie-value('date'), 'inactive')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Datierung</span>
				</h1>
				<div id="dateBtnRefine">
				    <ul>
				        {$datelistItems}
				    </ul>
				</div>
			</div>
			<div id="refBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'ref') or contains(request:get-cookie-value('ref'), 'none')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Referenzierungen</span>
				</h1>
				<div id="refBtnRefine">
				    <ul>
				        {$reflistItems}
				    </ul>
				</div>
			</div>
			<div id="rotateBtn" class="col-xs-2">
				<input
				    id="RotateSlide"
				    type="range"
				    min="0"
				    max="359"
				    step="5"
				    defaultValue="0"
				    style="height:0;"/>
			</div>
		</div>
};

declare function f-misc:mirador($node as node(), $model as map(*), $n){
let $sandbox := doc( "/db/sade-projects/textgrid/data/xml" || "/sandbox-iiif.xml" )
let $url := "https://textgridlab.org/1.0/iiif/mirador/?uri="
let $book := if(string-length($n) = 2) then substring($n, 1, 1) || "0" || substring($n, 2, 1)  else $n
let $uri := string($sandbox//tgmd:title[matches(., "Notizbuch_" || $book || "-IIIF")]/ancestor::tgmd:generic//tgmd:textgridUri)
return
    <iframe src="{$url || $uri}" height="100%" width="100%"/>
};

declare function f-misc:debug( $node as node(), $model as map(*) ){
    <meta name="debug" content="{ request:get-url() }"/>
};

declare function f-misc:ToolbarExtended($node as node(), $model as map(*)) {
let $develop := config:get('sade.develop')
return
switch ($develop)
    case "true" return
        <div id="ToolbarExtended">
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'hi') or contains(request:get-cookie-value('hi'), 'inactive')) then ' inactive' else ()}" id="hiBtn">hi</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'del') or contains(request:get-cookie-value('del'), 'inactive')) then ' inactive' else ()}" id="delBtn">del</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'add') or contains(request:get-cookie-value('add'), 'inactive')) then ' inactive' else ()}" id="addBtn">add</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'mod') or contains(request:get-cookie-value('mod'), 'inactive')) then ' inactive' else ()}" id="modBtn">mod</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'seq') or contains(request:get-cookie-value('seq'), 'inactive')) then ' inactive' else ()}" id="seqBtn">seq</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'latn') or contains(request:get-cookie-value('latn'), 'inactive')) then ' inactive' else ()}" id="latnBtn">Latn</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'script') or contains(request:get-cookie-value('script'), 'inactive')) then ' inactive' else ()}" id="scriptBtn">script
                <div class="BtnExt">
                    <ul>
                        <li class="clean">clean</li>
                        <li class="hasty">hasty</li>
                        <li class="angular">angular</li>
                    </ul>
                </div>
            </button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'med') or contains(request:get-cookie-value('med'), 'inactive')) then ' inactive' else ()} disabled" id="medBtn">medium</button>
        </div>
    default return <!-- develop feature -->
};

declare function f-misc:textgridStatus($node as node(), $model as map(*)) {
  let $col := "/db/sade-projects/textgrid"
  let $res := "tgstatus.xml"
  let $url := "https://dariah-de.github.io/status/textgrid/repstatus.html"
  let $lastMod := xmldb:last-modified($col, $res)
  let $path := $col || "/" || $res
  let $doc := doc( $path )
  let $active := not( exists( $doc//ok ) )
  let $needUpdate := not(doc-available($path)) or $lastMod < (current-dateTime() - xs:dayTimeDuration("PT6H"))
  let $getStatus :=
      if( $needUpdate )
      then
        let $status := httpclient:get(xs:anyURI($url), false(), ())//xhtml:div[contains(@class, 'repstatus')][not( contains(@class, 'ok') )]
        let $status := if( exists( $status ) ) then $status else <ok/>
        return
            (xmldb:login($col, config:get("sade.user"), config:get("sade.password")),
            xmldb:store($col, $res, $status))
    else if($active and $lastMod < (current-dateTime() - xs:dayTimeDuration("PT2H")))
        then
            let $status := httpclient:get(xs:anyURI($url), false(), ())//xhtml:div[contains(@class, 'repstatus')][not( contains(@class, 'ok') )]
            let $status := if( exists( $status ) ) then $status else <ok/>
            return
              (xmldb:login($col, config:get("sade.user"), config:get("sade.password")), xmldb:store($col, $res, $status))
    else ()
let $doc := doc( $col || "/" || $res )
return
    let $status := $doc/xhtml:div[@class]/tokenize(@class, '\s+')[last()]
    return
    if( $doc//ok ) then () else
        <div class="alert alert-{if($status = "error") then "danger" else $status}">
            <h4>TextGrid-Statusmeldung</h4>
            <p>Es liegt eine aktuelle Meldung zum TextGrid Repository vor. Einige
            Funktionen, z.Bsp die Darstellung der Faksimiles, sind vom TextGrid
            Repository abhängig und daher evtl. davon betroffen. Hier folgt die
            Meldung von TextGrid.</p>
            <h5>Status: {$status}</h5>
            {for $i at $pos in $doc/xhtml:div//xhtml:p[@lang="de"] return (if($pos gt 1) then <hr/> else (),$i)}
        </div>
};

declare function f-misc:serverStatus($node as node(), $model as map(*)) {
  let $col := "/db/sade-projects/textgrid"
  let $res := "serverstatus.xml"
  let $url := "https://dariah-de.github.io/status/index.html"
  let $lastMod := xmldb:last-modified($col, $res)
  let $path := $col || "/" || $res
  let $doc := doc( $path )
  let $active := not( exists( $doc//ok ) )
  let $needUpdate := not(doc-available($path)) or $lastMod < (current-dateTime() - xs:dayTimeDuration("PT6H"))
  let $getStatus :=
      if( $needUpdate )
      then
        let $status := httpclient:get(xs:anyURI($url), false(), ())//*:li[contains(., "Fontane")]/ancestor::*:div[contains(@class, "alert")]
        let $status := if( exists( $status ) ) then $status else <ok/>
        return
            (xmldb:login($col, config:get("sade.user"), config:get("sade.password")),
            xmldb:store($col, $res, $status))
    else if($active and $lastMod < (current-dateTime() - xs:dayTimeDuration("PT2H")))
        then
            let $status := httpclient:get(xs:anyURI($url), false(), ())//*:li[contains(., "Fontane")]/ancestor::*:div[contains(@class, "alert")]
            let $status := if( exists( $status ) ) then $status else <ok/>
            return
              (xmldb:login($col, config:get("sade.user"), config:get("sade.password")), xmldb:store($col, $res, $status))
    else ()
let $doc := doc( $col || "/" || $res )
return
    let $status := $doc/*:div[@class]/tokenize(@class, '\s+')[last()]
    return
    if( $doc//ok ) then () else
        <div class="alert {$status}">
            <h4>Statusmeldung</h4>
            <p>Es liegt eine aktuelle Meldung zum Betrieb dieses Servers vor.</p>
            <h5>Status: {substring-after($status, "-")}<br/>
                Datum: {string-join( ($doc//*:time/string(@datetime)), ";")}</h5>
              <!-- at DARIAH-DE status page no p[@lang='de'] available -->
            <p>Die vollständige Statusmeldung finden Sie unter <a href="https://dariah-de.github.io/status/">dariah-de.github.io/status/</a></p>
        </div>
};

declare function f-misc:nbTitle($id as xs:string, $model as map(*)){
let $doc := doc( config:get("data-dir") || $id)
let $title := $doc//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/string(.)
let $beta := if(contains(($doc//tei:revisionDesc/tei:change)[last()]/text(), 'Version 0.')) then <xhtml:sup>beta</xhtml:sup> else ()
return (
(:    <xhtml:span class="hidden-sm hidden-xs">{substring-before($title, ' ')}</xhtml:span>,:)
    <xhtml:span>{' '||substring-after($title, ' ')}</xhtml:span>,
    $beta
    )
};

declare function f-misc:pageNav($node as node(), $model as map(*), $id){
if(request:get-parameter-names() = 'page')
    then
        let $page := if(request:get-parameter('page', '') = '') then 'outer_front_cover' else request:get-parameter('page', '')
        let $doc:= doc('/db/sade-projects/textgrid/data'||$id)/tei:TEI
        let $pagelist := $doc//tei:sourceDoc//tei:surface[parent::tei:sourceDoc or @type="clipping" or @type="additional"]/string(@n)
        let $page := if ($page = '') then 'outer_front_cover' else $page
        let $index := index-of($pagelist, $page)
        let $return :=
        (
            (
            if ( $index gt 1 )
            then
            <a id="navPrev" href="edition.html?id={$id}&amp;page={$pagelist[$index - 1]}">
			<!-- slash needed in LIVE?  -->
                <i class="fa fa-chevron-left"></i>
                <span id="navPrevInfo"> vorige Seite </span>
            </a> else ()
            )
            ,
            (
            if ( $index lt count($pagelist) )
            then
            <a id="navNext" href="edition.html?id={$id}&amp;page={$pagelist[$index + 1]}">
			<!-- slash needed in LIVE?  -->
                <span id="navNextInfo"> nächste Seite </span>
                <i class="fa fa-chevron-right"></i>
            </a>
            else ()
            ),
            element script {
                '$(document).keyup(function(e){
                    var next = jQuery.isEmptyObject( $("#navNext").attr("href") );
                    var prev = jQuery.isEmptyObject( $("#navPrev").attr("href") );
                    if ( (e.keyCode == 37) &amp;&amp; prev == false )
                        {location.href = $( "#navPrev" ).attr("href"); }
                    else if (e.keyCode == 39 &amp;&amp; next == false )
                     {location.href = $( "#navNext" ).attr("href"); }
                });'
            }
        )
        return $return
else ()
};
declare function f-misc:tocHeader($node as node(), $model as map(*)){
<div id="nb-title" class="col-xs-4 col-md-2">
    <h1 class="animated slideInLeft">
        <span>{f-misc:nbTitle(request:get-parameter('id', ''), $model)}</span>
    </h1>
</div>
};
