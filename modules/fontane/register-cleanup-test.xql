xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ore="http://www.openarchives.org/ore/terms/";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";

declare function local:copy($element as element()) {
  element {node-name($element)}
    {$element/@*,
     for $child in $element/node()
        return if ($child instance of element())
          then local:copy($child)
          else $child
    }
};

declare function local:cleanup($nodes, $param) {
for $node in $nodes return
    typeswitch ( $node )
    case element( tei:person ) return
        if( $node//tei:link/contains(@target, $param) = true() ) then $node
        else ()
    case element( tei:personGrp ) return
        if( $node//tei:link/contains(@target, $param) = true() ) then $node
        else ()
    default return
        local:cleanup($nodes/node(), $param)
};

declare function local:entry($body, $param) {
 ()
};

let $docpath := "/db/sade-projects/textgrid/data/agg/253st.xml"

let $agg := tokenize($docpath, '/')[last()]
let $bodies := for $uri in doc('/db/sade-projects/textgrid/data/xml/agg/'||$agg)//ore:aggregates/substring-after(@rdf:resource, 'textgrid:')
                    where doc-available( '/db/sade-projects/textgrid/data/xml/data/' || $uri ||'.xml')
                    return
                        (doc('/db/sade-projects/textgrid/data/xml/data/'||$uri||'.xml')//tei:body[not(tei:listBibl)])
return local:cleanup($bodies[1], "16b00")