xquery version "3.1";
module namespace fontaneUeberblickskommentar="http://fontane-nb.dariah.eu/Ueberblickskommentar";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare default element namespace "http://www.tei-c.org/ns/1.0";

declare function fontaneUeberblickskommentar:render($docpath as xs:string) {
let $tei := doc($docpath)//tei:teiHeader,
    $items := for $item at $pos in (1 to 23)
                let $title := switch($pos)
                                case 1 return  "Signatur"
                                case 2 return  "Druckgeschichte"
                                case 3 return  "Inhalt"
                                case 4 return  "Textsorten/Gattungen"
                                case 5 return  "Fontanes Quellen"
                                case 6 return  "Beilagen"
                                case 7 return  "Entstehungszeit/Benutzungszeitraum"
                                case 8 return  "Sprachen"
                                case 9 return  "Schreiberhände"
                                case 10 return "Schreibstoffe und -geräte"
                                case 11 return "Duktus"
                                case 12 return "Druckschriften"
                                case 13 return "Layout"
                                case 14 return "Umfang mit Vorsatzblatt"
                                case 15 return "Format"
                                case 16 return "Einband"
(:                                case 17 return "Taschen":)
                                case 17 return "An- und Aufklebungen"
                                case 18 return "Blattfragmente"
                                case 19 return "Vakat-Seiten"
                                case 20 return "Skizzen"
                                case 21 return "Stempel"
                                case 22 return "Foliierung"
                                case 23 return "Besonderheiten"
                                default return "error",
                $path :=    (
                            "//fileDesc/sourceDesc/msDesc/msIdentifier",
                            "//fileDesc/publicationStmt/ab/listBibl/listBibl[1]",
                            "//fileDesc/sourceDesc/msDesc/msContents/ab/list[@type='editorial']/item",
                            "//profileDesc/textClass/keywords",
                            "//profileDesc/textDesc/derivation/linkGrp/link/",
                            "//msPart",
                            "//profileDesc/creation/date",
                            "//profileDesc/langUsage/language",
                            "//profileDesc/handNotes[ number(@n) lt 7 ]",
                            "//profileDesc/handNotes[ number(@n) = (7,8) ]",
                            "//profileDesc/handNotes[ number(@n) = 9 ]",
                            "//sourceDesc/msDesc/physDesc/typeDesc/ab",
                            "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/layoutDesc/layout",
                            "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/measure/",
                            "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/dimensions",
                            "//fileDesc/sourceDesc/msDesc/physDesc/bindingDesc",
(:                            "//fileDesc/sourceDesc/msDesc/physDesc/accMat",:)
                            "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) != 'Vakat-Seiten:'][hi/string(.) != 'Blattfragmente:']",
                            "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) = 'Blattfragmente:']",
                            "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) = 'Vakat-Seiten:']",
                            "//fileDesc/sourceDesc/msDesc/physDesc/decoDesc/ab[hi/string(.) = 'Skizzen:']",
                            "//fileDesc/sourceDesc/msDesc/physDesc/additions/ab[hi/string(.) = 'Stempel:']",
                            "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/foliation",
                            "(//fileDesc/sourceDesc/msDesc/physDesc/handDesc, //fileDesc/sourceDesc/msDesc/physDesc/typeDesc)"
                            ),
                    $content := switch($pos)
                                case 1 return
                                    <ul><li>{$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/repository/text()}</li>
                                        <ul><li>{$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/collection/text()}
                                            <ul><li>{$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/idno/string(@type)}: {$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/idno/text()}</li>
                                                <li>{$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/altIdentifier/idno/string(@type)}: {$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/altIdentifier/idno/text()}</li>
                                                <li>Titel: {if(exists($tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/msName/text())) then $tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/msName/text() else 'ohne'}</li>
                                            </ul></li>
                                        </ul>
                                    </ul>
                                case 2 return
                                    <ul>{
                                        for $listBibl in $tei/tei:fileDesc/publicationStmt/ab/listBibl/listBibl[1]
                                        return <li>{$listBibl/head/text()}<ul>{for $item in $listBibl/bibl return <li>{for $text in $item//text()[not(parent::tei:hi)] return local:textref($text)}</li>}</ul></li>
                                    }</ul>
                                case 3 return try {
                                    <ul>
                                        <li>Herausgeber-Inhaltsverzeichnis
                                            <ul>{for $item in $tei/fileDesc/sourceDesc/msDesc/msContents/ab/list[@type="editorial"]/item return
                                                <li>{for $text in $item//text()[not(parent::tei:hi)] return local:textref($text)}</li> }
                                            </ul>
                                        </li>
                                    </ul> } catch * { 'error at item 3' }
                                case 4 return <ul> {
                                            (if( $tei/profileDesc/textClass/keywords[1]/term/string(text()) = 'non-fiction')
                                                then <li>Nicht-Fiktional</li>
                                            else (),
                                            if($tei/profileDesc/textClass/keywords[2]/term/string(.) = 'fiction')
                                                then <li>Fiktional</li>
                                            else ())}
                                            <hr/> {for $item in $tei/profileDesc/textClass/keywords[not(@scheme)]/term return
                                                    <li>
                                                        {if( $item/string(.) = "fiction" ) then "Fiktional" else $item/text()}
                                                    </li>}
                                                </ul>
                                case 4 return <ul>{for $item in $tei/profileDesc/textClass/keywords return <li>nach {$item/string(@scheme)} <ul>{for $term in $item/term return <li>{$term/text()}</li>}</ul></li>}</ul>
                                case 5 return
                                                if( not($tei//profileDesc/textDesc/derivation/*) ) then "Keine" else
                                                <ul>{for $type in distinct-values($tei//profileDesc/textDesc/derivation/linkGrp/link/string(@type))
                                                    return
                                                        <li>{switch($type)
                                                            case 'mentioned' return 'Im Notizbuch von Fontane genannte Quellen'
                                                            case 'ascertained' return 'Ermittelte Quellen'
                                                            case 'ascertained' return 'Weitere ermittelte Quellen'
                                                            default return 'TODO @type'
                                                        } <ul> {for $link in $tei//profileDesc/textDesc/derivation/linkGrp/link[@type = $type]
                                                            let $target := substring-after(substring-after($link/@target, ' '), ':')
                                                            let $litVZ := doc('/db/sade-projects/textgrid/data/xml/data/25547.xml')/tei:TEI
                                                            return <li><xhtml:a href="literaturvz.html?id={$target}">{ $litVZ//bibl[@xml:id = $target]//expan/text()}</xhtml:a>
                                                                {if($link/@resp) then (' [Vgl. ', <xhtml:a href="literaturvz.html?id={substring-after($link/@resp, ':')}">{$litVZ//bibl[@xml:id = substring-after($link/@resp, ':')]//abbr/text()}</xhtml:a>, '.]') else ()}</li>
                                                        } </ul></li>}</ul>
                                case 6
                                    return
                                        if($tei//msPart/*)
                                        then    <div>
                                                    <div><span style="font-weight: bold;">Beilagen: </span> {
                                                        switch(count($tei//msPart))
                                                            case 1 return "Eine"
                                                            case 2 return "Zwei"
                                                            case 3 return "Drei"
                                                            case 4 return "Vier"
                                                            case 5 return "Fünf"
                                                            case 6 return "Sechs"
                                                            case 7 return "Sieben"
                                                            case 8 return "Acht"
                                                            case 9 return "Neun"
                                                            default return "Übersetzung von Zahl nur bis 9."}
                                                    </div>
                                                    <ul>
                                                        {   for $msPart in $tei//tei:msPart[tei:msIdentifier][tei:ab]
                                                            return
                                                                <li> {$msPart/msIdentifier/idno/text()}:  {$msPart/ab/text()}</li>
                                                        }
                                                    </ul>

                                                </div>
                                        else "Keine"
                                case 7 return
                                                let $lis := (<li>{if (exists($tei/profileDesc/creation/date[@type = 'authorial']//text()))then () else attribute class {'OutOfOrder'} }Fontanes Angabe: {if(exists($tei/profileDesc/creation/date[@type = 'authorial']//text())) then $tei/profileDesc/creation/date[@type = 'authorial']//text() else 'Keine'}</li>,
                                                            <li>{if (exists($tei/profileDesc/creation/date[@type = 'Friedrich_Fontane']//text()))then () else attribute class {'OutOfOrder'} }Friedrich Fontanes Angabe: {if(exists($tei/profileDesc/creation/date[@type = 'Friedrich_Fontane']//text())) then $tei/profileDesc/creation/date[@type = 'Friedrich_Fontane']//text() else 'Keine'}</li>,
                                                            <li>{if (exists($tei/profileDesc/creation/date[@type = 'editorial']//text()))then () else attribute class {'OutOfOrder'} }Ermittelt: {if(exists($tei/profileDesc/creation/date[@type = 'editorial']//text())) then $tei/profileDesc/creation/date[@type = 'editorial']//text() else 'Keine'}</li>)
                                                return
                                                <ul>{$lis}</ul>
                                case 8 return <ul>{for $lang in $tei/profileDesc/langUsage/language return <li>{$lang/text()}</li>}</ul>
                                case 9 return <ul>{for $hand in $tei/profileDesc/handNotes return
                                    switch($hand/number(@n))
                                        case 1 return <li>Zeitgenössische Schreiberhände &#x2013; Autor <ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
                                        case 2 return <li>Zeitgenössische Schreiberhände &#x2013; Schreiber<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
                                        case 3 return <li>Postume Schreiberhände &#x2013; Schreiber<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
                                        case 4 return <li>Postume Schreiberhände &#x2013; Archivare<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
                                        case 5 return <li>Zeitgenössiche und postume Schreiberhände &#x2013; Stempel<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
                                        case 6 return <li>Zeitgenössische und postume Schreiberhände &#x2013; Drucke<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
                                    default return ()
                                 }</ul>
                                case 10 return <ul>{for $hand in $tei/profileDesc/handNotes return
                                    switch($hand/number(@n))
                                        case 7 return for $h in $hand/handNote return <li>{$h//text()}</li>
                                        case 8 return (<hr/>, for $h in $hand/handNote return <li>{$h//text()}</li>)
                                    default return ()
                                 }</ul>
                                case 11 return <ul>{for $hand in $tei/profileDesc/handNotes return
                                    switch($hand/number(@n))
                                        case 9
                                        return
                                            for $h in $hand/handNote
                                            return
                                                <li>
                                                    {switch($h/string(@script))
                                                        case 'clean' return 'Reinschriftlich'
                                                        case 'standard' return 'Standard'
                                                        case 'hasty' return "Unruhig"
                                                        case 'angular' return "Gezackt"
                                                        default return 'TODO'}<ul> <li>{$h//text()}</li> </ul></li>
                                    default return ()
                                 }</ul>
                                case 12 return <ul>{for $item in $tei//sourceDesc/msDesc/physDesc/typeDesc/ab return <li>{replace(string-join($item//text()[not(parent::tei:hi)]), '^\s+', '')}</li>}</ul>
                                case 13 return $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/layoutDesc/layout/text()
                                case 14 return $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/measure/@quantity || ' ' || (switch(string($tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/measure/@unit)) case 'leaf' return 'Blatt' default return 'TODO: Einheit übersetzen!!!')
                                case 15 return <ul>{for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/dimensions return <li>{switch(string($item/@type)) case 'leaf' return 'Blatt' case 'binding' return 'Einband' default return 'TODO: @type übersetzen!!!'} <ul>{for $i in $item/* return <li>{switch($i/local-name()) case 'width' return 'Breite' case 'height' return 'Höhe' case 'depth' return 'Tiefe' default return 'TODO: local-name() übersetzen!!!'}&#x20;{string($i/@quantity)} {string($i/@unit)}</li>}</ul></li>}</ul>
                                case 16 return <p>{$tei/fileDesc/sourceDesc/msDesc/physDesc/bindingDesc//text()}</p>
(:                                case 17 return (if($tei/fileDesc/sourceDesc/msDesc/physDesc/accMat/text()) then $tei/fileDesc/sourceDesc/msDesc/physDesc/accMat/text() else 'Keine'):)
                                case 17 return  let $types := distinct-values( $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab/hi/string(.) )
                                                let $out :=
                                                <ul>{for $item in $types
                                                where $item != 'Vakat-Seiten:' and $item != 'Blattfragmente:'
                                                return <li>{$item} <ul>{for $i in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/text() = $item]
                                                                        return substring-after(string-join( $i//text() ), $item)} </ul> </li> }</ul>
                                                return if(not($out//li)) then 'Keine' else $out
(:                                case 18 return let $clist:= :)
(:                                                    <ul>{for $item in ('Blätter', 'Broschüren', 'Karten', 'Zeitungsausschnitte') :)
(:                                                    return <li>{$item}<ul>:)
(:                                                        {for $subitem in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[starts-with(hi/text(), $item)] return <li class="{if(starts-with(replace(string-join($subitem//text()[not(parent::hi)]), '\s', ''), 'Kein' )) then 'OutOfOrder' else ()}">{$subitem//text()[not(parent::hi)]}</li>}</ul></li>}</ul>:)
(:                                                return for $node in $clist/node() return typeswitch($node) case element(ul) return element ul {$node} default return $node:)
(:                                                    return $clist:)
                                case 18 return <ul>{for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) = 'Blattfragmente:']
                                                    let $empty := starts-with(($item//text())[2], ' Keine')
                                                    return
                                                        <li class="{if( $empty = true() ) then 'OutOfOrder' else '' }">
                                                            {if( $empty = true() )
                                                            then substring-after(($item//text())[2], 'Keine ')
                                                            else tokenize(($item//text())[2], ';\s')[2]  }
                                                            <ul>
                                                                { if( $empty = true() )
                                                                then <li>Keine</li>
                                                                else
                                                                    <li>{tokenize(string-join($item//text()[not(parent::tei:hi)][not(parent::tei:ref)]), ';\s')[1]};
                                                                        {for $text in ($item//text()[not(parent::tei:hi)])[position() != 1]
                                                                        return local:textref($text)}</li>
                                                                }</ul></li>
                                                }</ul>
                                case 19 return <ul>{for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) = 'Vakat-Seiten:'] return <li>{$item//text()[not(parent::tei:hi)]}</li>}</ul>
                                case 20 return <ul>{for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/decoDesc/ab[hi/string(.) = 'Skizzen:'] return <li>{for $text in $item//text()[not(parent::tei:hi)] return local:textref($text)}</li>}</ul>
                                case 21 return <ul>{for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/additions/ab[hi/string(.) = 'Stempel:'] return <li>{for $text in $item//text()[not(parent::tei:hi)] return local:textref($text)}</li>}</ul>
                                case 22 return $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/foliation//text()
                                case 23 return let $out := <ul>
                                                            {for $item in ($tei/fileDesc/sourceDesc/msDesc/physDesc/handDesc, $tei/fileDesc/sourceDesc/msDesc/physDesc/typeDesc)
                                                                                return <li>{
                                                                                        if($item//text()[not(parent::tei:hi)] = ' Keine') then 'Keine' else $item//text()[not(parent::tei:hi)]}
                                                                                        </li>
                                                            }
                                                        </ul>
                                                return if (string-join($out) = 'KeineKeine') then <xhtml:ul><xhtml:li>Keine</xhtml:li></xhtml:ul> else $out
                                default return "error"
                return
                <tr class="teiHeaderRow{if(replace(string-join($content), "\s", "") = 'Keine') then ' OutOfOrder' else ''}" id="r{$pos}">
                    <th scope="row"><span class="rowNum"><a class="headerLink" href="ueberblickskommentar.html?{request:get-query-string()}#r{$pos}"><i class="fa fa-link"></i></a>{$pos}</span></th>
                    <td><span title="{$path[$pos]}">{$title}</span></td>
                    <td>{$content}</td>
                </tr>

return
    <div class="container">
    {   for $ab in $tei//abstract/ab
        return <xhtml:p class="intro">{ $ab/node() }</xhtml:p>
    }
    {if(doc-available($docpath)) then
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Information</th>
                    <th>Beschreibung</th>
                </tr>
            </thead>
            <tbody>
                {$items}
            </tbody>
        </table>
    else <xhtml:div>The requested document is not available.</xhtml:div> }
    </div>
};

declare function local:textref($text as node()*) as node()*{
for $item in $text return
    if($item/parent::tei:ref)
    then
        let $target := string($item/parent::tei:ref/@target),
            $id := if(matches($target, '^#[A-E]\d\d_'))
                    then substring-after($target, '#')
                    else (),
            $page :=    if($id)
                        then $text/ancestor::tei:TEI//tei:surface[//@xml:id = $id]/string(@n)
                        else tokenize($target, "'")[2],
            $doc := request:get-parameter('id', ''),
            $hl := if($id) then '#'||$id else (),
            $link := 'edition.html?id='||$doc||'&amp;page='||$page||$hl
    return
        <xhtml:a href="{$link}">{$item}</xhtml:a>
    else if($item/parent::tei:rs) then
            let $ref := $item/parent::tei:rs/string(@ref)
            let $type := substring-before($ref, ':')
            let $link := switch($type)
                            case 'lit' return 'literaturvz.html?id='||substring-after($ref, ':')
                            default return 'TODOvz.html'
            return <xhtml:a href="{$link}">{$item}</xhtml:a>
    else $item
};
