xquery version "3.1";

module namespace f-render="http://fontane-nb.dariah.eu/ns/SADE/render";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace f-transform="http://fontane-nb.dariah.eu/Transfo" at "transform.xqm";
import module namespace templates="http://exist-db.org/xquery/templates" ;

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare variable $f-render:id := request:get-parameter("id", "/xml/data/16b00.xml");
declare variable $f-render:page :=
    let $para := request:get-parameter("page", "outer_front_cover")
    return
        if($para = "") then "outer_front_cover" else $para
    ;
declare variable $f-render:doc := doc( config:get("data-dir") || $f-render:id );
declare variable $f-render:surface := $f-render:doc//tei:surface[@n = $f-render:page];
declare variable $f-render:xhtml := doc( config:get("data-dir") || "/xml/xhtml/" ||tokenize($f-render:id, "/|\.xml")[4] || "/" || $f-render:page || ".xml" );
declare variable $f-render:notebook := substring-after($f-render:doc//tei:sourceDoc/@n, 'er_');

declare function f-render:initView($node as node(), $model as map()) {
    let $nodes := for $n in $node/node() return templates:process($n, $model)
    return
        if( doc-available( config:get("data-dir") || request:get-parameter("id", "0")) )
        then
            element {$node/name()} {
                attribute id {$f-render:notebook},
                $node/@*[not(local-name() => starts-with("data-") )],
                $nodes
            }
        else <div>The requested document is not available.</div>
};

declare function f-render:facs($node as node(), $model as map()) {
    <xhtml:div>
        {$node/@*[not(local-name() => starts-with("data-") )]}
        {f-transform:facs($f-render:surface)}
    </xhtml:div>
};
declare function f-render:tran($node as node(), $model as map()) {
    <xhtml:div>
        {$node/@*[not(local-name() => starts-with("data-") )]}
        {($f-render:xhtml//xhtml:div[contains(@class, "surface")])[1]}
    </xhtml:div>
};
declare function f-render:code($node as node(), $model as map()) {
    <xhtml:div>
        {$node/@*[not(local-name() => starts-with("data-") )]}
        {$f-render:xhtml//xhtml:div[@class = "teixml"]}
    </xhtml:div>
};
