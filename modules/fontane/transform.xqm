xquery version "3.0";
module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo";

declare boundary-space preserve;
declare default element namespace "http://www.tei-c.org/ns/1.0";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace digilib="digilib:digilib";
declare namespace svg="http://www.w3.org/2000/svg";
declare namespace xlink="http://www.w3.org/1999/xlink";

import module namespace console="http://exist-db.org/xquery/console";

declare variable $fontaneTransfo:tooltipReplacementPattern := '\s$|,$|\.|\($|\)$|;|&#x2003;';

declare variable $fontaneTransfo:bracket_left.svg := <svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   version="1.1"
   id="svg3383"
   viewBox="20 20 40 250"
   style="transform: scaleX(0.5);"
   height="100%"
   width="1cm">
  <metadata
     id="metadata3390">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <defs
     id="defs3388" />
  <path
     stroke-width="1"
     d="M 30.87,261.00 C 30.87,261.00 30.00,220.00 30.00,220.00 30.00,220.00 30.00,179.00 30.00,179.00 29.99,173.57 29.22,165.11 27.53,160.00 25.78,154.70 23.34,153.50 23.18,149.91 22.98,145.60 26.14,143.08 27.78,138.00 29.18,133.65 29.99,123.73 30.00,119.00 30.00,119.00 30.00,72.00 30.00,72.00 30.00,60.01 29.29,47.86 31.28,36.00 32.35,29.70 33.52,24.34 40.00,22.00 40.00,22.00 35.93,36.00 35.93,36.00 35.93,36.00 34.00,53.00 34.00,53.00 34.00,53.00 34.00,125.00 34.00,125.00 34.00,125.00 31.45,141.00 31.45,141.00 31.45,141.00 28.13,150.00 28.13,150.00 28.13,150.00 31.09,159.00 31.09,159.00 31.09,159.00 34.00,179.00 34.00,179.00 34.00,179.00 34.00,244.00 34.00,244.00 34.00,244.00 36.16,263.00 36.16,263.00 36.16,263.00 40.00,276.00 40.00,276.00 32.45,274.30 31.81,267.69 30.87,261.00"
     id="bracket" />
</svg>;

declare function fontaneTransfo:magic($nodes as node()*) {
    fontaneTransfo:magic(($nodes), "undefined")
};

declare function fontaneTransfo:magic($nodes as node()*, $inital as xs:string?) {
for $node in $nodes return
typeswitch($node)
    case element(tei:TEI)
            return
                element xhtml:div {
                    attribute class {'TEI', 'clearfix' (: damn bootstrap in this code? damn it! :) },
                    fontaneTransfo:magic($node/node())
                }

    case element(tei:teiHeader) return ()

    case element(tei:sourceDoc)
        return
            element xhtml:div {
                attribute id {substring-after($node/@n, 'er_')},
                attribute class {'sourceDoc'},
                fontaneTransfo:magic($node/node())
            }

    case element(tei:surface)
        return fontaneTransfo:surfaceDiv($node, $inital)
    case element(tei:zone)
        return
            try {(
            element xhtml:div {
(:                if ($node/parent::tei:line) :)
(:                    then:)
(:                        fontaneTransfo:zone-inline($node):)
(:                    else:)
                fontaneTransfo:zone($node),
                if( $node/contains(@style, "border-left-style:double") and $node/contains(@rend,"border-left-style:wavy") )
                then
                    element xhtml:div {
                        attribute class {"border-left border-left-style wavy"},
                        fontaneTransfo:magic($node/node())
                    }
                else
                    fontaneTransfo:magic($node/node()),
                fontaneTransfo:postZone($node)
            }
            )}
            catch * { ("zone: ", $err:code , $err:description, $err:value)  }
    case element(tei:figure)
        return
            if( $node/parent::zone/@points ) then
                    let $coordinates := tokenize( $node/ancestor::zone[1]/@points, ' ' )
                    return
                        (<svg class="{tokenize($node//tei:figDesc/tei:ref/text(), ' ')[2]}"
                        width="{$node/ancestor::tei:TEI/tei:teiHeader//tei:dimensions[@type="leaf"]/tei:width/string(@quantity)}mm"
                        height="{$node/ancestor::tei:TEI/tei:teiHeader//tei:dimensions[@type="leaf"]/tei:height/string(@quantity)}mm"
                        style="position:absolute; top:0;">
                            {for $pair in 1 to (count($coordinates) - 1)
                            let $x1 := tokenize($coordinates[$pair], ',')[1]
                            let $y1 := tokenize($coordinates[$pair], ',')[2]
                            let $x2 := tokenize($coordinates[$pair + 1], ',')[1]
                            let $y2 := tokenize($coordinates[$pair + 1], ',')[2]
                                return
                                    <line style="stroke-width:1" x1="{$x1}cm" y1="{$y1}cm" x2="{$x2}cm" y2="{$y2}cm" />
                            }
                        </svg>,
                        element xhtml:div {
                            attribute class {'hrHover'},
                            '<'||tokenize($node//tei:figDesc/tei:ref/text(), ' ')[2]||'>'
                            }
                        )
            else
                if(contains($node//tei:figDesc/tei:ref/text(), 'Absatzlinie') and contains($node//tei:figDesc/tei:ref/text(), 'nachgezogen')) then
                    (<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="auto" viewBox="0 0 300 20">
                        <path d="M130 4 C 130 4, 110 2, 90 6" fill="transparent"/>
                        <path d="M90 6 C 90 4, 100 10, 295 3" fill="transparent"/>
                        <path d="M295 3 C 300 -2, 300 8, 5 20" fill="transparent"/>
                        <path d="M5 20 C 4 17, 8 22, 295 14" fill="transparent"/>
                    </svg>,
                element xhtml:div {
                    attribute class {'hrHover'},
                    'Absatzlinie'
                })
            else
                if(contains($node//tei:figDesc/tei:ref/text(), "einfache Absatzlinie (hin und her)"))
                then
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 451 46">
                        {element svg:path {
                           attribute id {"ahihe"},
                           attribute fill {"none"},
                           attribute stroke {"darkslategrey"},
                           attribute stroke-width {"1"},
                           attribute d {"M 440.67,14.67 " ||
                            "C 440.67,14.67 25.33,2.00 7.33,21.33 " ||
                             "11.33,28.00 422.00,11.33 414.67,20.00 " ||
                             "392.67,8.67 32.00,36.67 32.00,36.67" }}}
                    </svg>
            else
                if(contains($node//tei:figDesc/tei:ref/text(), "horizontale Halbschleife von links nach rechts unten"))
                then
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 114 24">
                    <path id="Unnamed" fill="none" stroke="darkslategrey" stroke-width="1"
                            d="M 2.75,20.00 C 12.88,23.38 91.88,-1.50 106.75,4.12  113.62,6.88 112.00,12.12 110.50,13.25  107.25,15.00 105.00,11.12 105.00,11.12" />
                    </svg>
            else
                if(contains($node//tei:figDesc/tei:ref/text(), "horizontale Halbschleife von links oben nach rechts"))
                then
                    <svg xmlns="http://www.w3.org/2000/svg" style="bottom:0; position:absolute;" viewBox="0 0 743 76" height="100%" width="100%" preserveAspectRatio="none">
                        <path id="Unnamed" fill="none" stroke="darkslategrey" stroke-width="3"
                            d="M 181.33,35.33 C 181.33,35.33 257.33,20.67 232.67,10.67  222.67,-3.33 4.67,27.33 6.67,42.00  4.67,73.33 380.00,68.00 730.00,52.67" />
                    </svg>
            else
                if(contains($node//tei:figDesc/tei:ref/text(),"horizontale Schleife von links oben nach rechts unten"))
                then
                  <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="auto" viewBox="0 0 220 29">
                      {element svg:path {
                          attribute id {"sl2r1"},
                          attribute fill {"none"},
                          attribute stroke {"darkslategrey"},
                          attribute stroke-width {"1.5"},
                          attribute d {"M 14.00,18.18 " ||
                             " C 4.55,19.82 -2.73,27.09 5.27,26.55 " ||
                               " 65.64,20.91 197.64,-20.00 218.18,16.36 " ||
                               " 213.64,19.82 203.45,13.64 203.45,13.64" }}}
                  </svg>
            else
                if(contains($node//tei:figDesc/tei:ref/text(),"horizontale Halbschleife von links unten nach rechts"))
                then
                  <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="auto" viewBox="0 0 59 16">
                     {element svg:path {
                         attribute id {"hsl2r"},
                         attribute fill {"none"},
                         attribute stroke {"black"},
                         attribute stroke-width {"1"},
                         attribute d {"M 20.55,10.82" ||
                             " C 20.55,10.82 -8.18,5.55 6.91,5.09 " ||
                             " 35.18,4.00 57.00,3.91 57.00,3.91"} }
                     }
                 </svg>
            else
                if(contains($node//tei:figDesc/tei:ref/text(), 'Absatzlinie')) then
                    (element xhtml:hr {attribute class {'Absatzlinie'}
                },
                element xhtml:div {
                    attribute class {'hrHover'},
                    'Absatzlinie'
                })
            else
                if(contains($node//tei:figDesc/tei:ref/text(), 'Abgrenzungslinie')) then
                (element xhtml:hr {
                    attribute class {
                        'Abgrenzungslinie',
                        (let $medium := fontaneTransfo:getMedium($node)
                        return if($medium != ("pencil", "")) then $medium else ())
                    }
                },
                element xhtml:div {
                    attribute class {'hrHover'},
                    'Abgrenzungslinie'
                })
            else
                if(contains($node//tei:figDesc/tei:ref/text(), 'Schlusslinie')) then
                    (
                    if(contains($node//tei:figDesc/tei:ref/text(), "horizontale Halbschliefe von links oben nach rechts"))
                    then
                          <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="auto" viewBox="0 0 418 40">
                            {element svg:path {
                              attribute fill {"none"},
                              attribute stroke {"darkslategrey"},
                              attribute stroke-width {"1"},
                              attribute d {"M 408.00,13.50" ||
                                 " C -6.50,51.50 -4.50,22.50 17.00,16.00" ||
                                   " 17.00,16.00 35.50,12.50 35.50,12.50"}
                              }
                            }
                          </svg>
                    else
                        element xhtml:hr {
                            attribute class {'Schlusslinie'}
                        },
                element xhtml:div {
                    attribute class {'hrHover'},
                    'Schlusslinie'
                })
            else ()
    case element(tei:line)
        return
            try {
            element xhtml:div  {
                attribute class {'line', fontaneTransfo:segStyle($node), if($node/@type) then string($node/@type) else ()},
                attribute style {
                    string-join((
                        ($node/@style),
                        (if($node//text()[not(parent::tei:fw)]) then () else ('height:0.6cm;')),
                        ( (: line-height given in div.zone[style] has no effect wheren large rendering is needed :)
                        if($node//*[contains(string(@style), 'large')] or $node//ancestor::tei:zone[contains(string(@style), 'large')]) then replace( fontaneTransfo:lineHeight($node/parent::tei:zone), 'line-height', 'max-height') else ())
                ), ';')
                },
                try {fontaneTransfo:magic($node/node())} catch * { ($err:code , $err:description, $err:value) }
            }}
            catch * { ($err:code , $err:description, $err:value)  }

    case element(tei:seg)
        return
            if ($node/tei:g[@ref="#hb"]) then fontaneTransfo:fraction($node)
            else
            element xhtml:span {
                attribute class {
                    'seg',
                    if ($node/@style or $node/@rend or $node/@type) then fontaneTransfo:segStyle($node) else ()
                },
                if(contains($node/@style, 'letter-spacing')) then
                    attribute style { 'letter-spacing:'||substring-before(substring-after($node/@style, 'letter-spacing:'), 'cm')||'cm;'}
                    else (),
                if(contains($node/@rend, 'line-through-style:triple_oblique')) then (<xhtml:span class="triple_oblique1"/>,<xhtml:span class="triple_oblique2"/>,<xhtml:span class="triple_oblique3"/>) else(),
                fontaneTransfo:magic($node/node()),
                if($node/@type = "abort")
                then
                    element xhtml:div {
                        attribute class {'segHover italic'},
                        'Schreibabbruch'}
                else (),
                if($node/@type = "multiphrase")
                then
                    element xhtml:div {
                        attribute class {'segHover italic'},
                        'Mehrfachformulierung'}
                else ()
            }
    case element(tei:fw)
        return
            if ($node/preceding::tei:handShift[@new][@new != '#Fontane'] or not( exists( $node/preceding::tei:handShift[@new] ) ))
            then
                (element xhtml:div { attribute class {'fwWrapper'},
                element xhtml:span {
                    attribute class {'fw', (if ($node = ($node/ancestor::tei:surface//tei:fw[preceding::tei:handShift/@new != '#Fontane'])[position() gt 1] ) then (
                        (:'fw2':) ()
                        ) else () )
                    }, fontaneTransfo:magic($node/node())}
                })
            else
                element xhtml:span {
                    attribute class {'fw-fontane'},
                    fontaneTransfo:magic($node/node())
                }
    case element(tei:mod)
        return
            element xhtml:span {
                attribute class {
                    'mod',
                    $node/@type,
                    if($node/@rend) then "mod"||replace($node/@rend, ":", "_") else ()
                },
                if($node/@style) then attribute style {
                    if($node/@seq = "1")
                    then string($node/@style)
                    else
                        if($node/@seq = "2" and $node/@style = "text-decoration:underline" )
                        then "border-bottom: 1px solid darkslategrey;"
                        else ()
                } else (),
                if($node//del[@rend='overwritten']) then
                    element xhtml:div {
                        attribute class {'modHover italic', if($node/ancestor::tei:*/@rotate or $node/ancestor::tei:zone/preceding-sibling::tei:addSpan) then () else 'hoverTop'},
                        if($node/ancestor::tei:*/@rotate) then attribute style {'top:30px; transform:rotate(-'|| sum($node/ancestor::tei:*/@rotate) ||'deg);', '-webkit-transform:rotate(-'|| sum($node/ancestor::tei:*/@rotate) ||'deg);'} else (),
                        if(ends-with($node/preceding::text()[1][parent::tei:line = $node/parent::tei:line], ' ') or ends-with($node/preceding::text()[1][parent::tei:line = $node/parent::tei:line], ' '))
                            then ()
                            else
                               element xhtml:span { local:preText($node)},
                        '<',
                        element xhtml:span {
                            if( $node//tei:g[@ref="#hb"] )
                                then fontaneTransfo:magic($node/tei:del/node())
                                else fontaneTransfo:magic($node/tei:del/node())//text(),
                            (if ($node/tei:del/tei:gap) then fontaneTransfo:magic($node/tei:del/tei:gap) else ())
                        },
                        ' überschrieben ',
                        element xhtml:span {
                            string-join($node/tei:add//text())
                        },
                        '>', if(matches(string-join($node//tei:add/text()), '^\.$|^,$|^:$|^;$')) then () else element xhtml:span { local:postText($node) }
                    }
                else if($node//del[not(@rend='overwritten')]) then
                    element xhtml:div {
                        attribute class {'modHover italic', if($node/ancestor::tei:*/@rotate or $node/ancestor::tei:zone/preceding-sibling::tei:addSpan) then () else 'hoverTop'},
                        if($node/ancestor::tei:*/@rotate) then attribute style {'top:30px; transform:rotate(-'|| sum($node/ancestor::tei:*/@rotate) ||'deg);', '-webkit-transform:rotate(-'|| sum($node/ancestor::tei:*/@rotate) ||'deg);'} else (),
                        ('Ersetzung', if( contains(string-join($node//@rend), 'caret') ) then 'mit Einweisungszeichen' else ())
                    }
                else (),
                fontaneTransfo:magic($node/node())
            }
    case element(tei:del)
        return
            element xhtml:span {
                attribute class {'del', replace($node/@rend, '#', 'hash'), if($node/@instant)then'instant'else()},
                (
                    if(local:modTest($node/parent::tei:mod))
                    then
                        attribute style {'text-align: center;display: inline-block;position: absolute;width:100%;'}
                    else ()),
                fontaneTransfo:magic($node/node()),
                if($node/@instant) then
                    element xhtml:div {
                        attribute class {'instantHover italic'},
                        'Sofortkorrektur'}
                else()
            }
    case element(tei:hi)
        return
            element xhtml:span {
                attribute class {'hi'},
                fontaneTransfo:magic($node/node())
            }
    case element(tei:add)
        return
            if( $node/@corresp and not($node/@type="multiphrase") ) then () else
            fontaneTransfo:addPreselection($node)
    case element(tei:retrace)
        return
            (
            fontaneTransfo:retrace($node),
            fontaneTransfo:magic($node/node())
            )
    case element(tei:restore)
        return
            element xhtml:span {
                attribute class {'restore'},
                element xhtml:div {attribute class {'restoreHover italic'},"Durchstreichung zurückgenommen"},
                fontaneTransfo:magic($node/node())
            }
    case element(tei:choice)
        return
            element xhtml:span {
                attribute class {'choice'},
                if ($node/tei:expan) then
                    element xhtml:div {
                        attribute class {'expan italic'},
                        let $count := count($node//tei:expan)
                        return
                            if($count = 1) then $node//tei:expan/text()
                            else for $expan at $pos in $node//tei:expan
                                    return
                                        if($pos = ($count - 1)) then $expan || '&#x2003;oder&#x2003;'
                                        else if($pos = $count)then $expan
                                        else $expan || ',&#x2003;'
                    }
                else (),
                fontaneTransfo:magic($node/node())
            }
    case element(tei:abbr)
        return
            element xhtml:span {
                attribute class {'abbr'},
                fontaneTransfo:magic($node/node())
            }
    case element(tei:rs)
        return
            element xhtml:span {
                attribute class {'rs', if($node/@type) then $node/@type else (), if($node/@ref) then substring-before($node/@ref, ':') else ()},
                attribute data-ref { string($node/@ref) },
                fontaneTransfo:magic($node/node())
            }
    case element(tei:date)
        return
            element xhtml:span {
                attribute class {'date'},
                fontaneTransfo:magic($node/node())
            }
    case element(tei:stamp)
        return
            element xhtml:div {
                attribute class {'stamp'},
                fontaneTransfo:stamp($node)
            }
    case element(tei:metamark)
        return fontaneTransfo:metamark($node)
    case element(tei:g)
        return
            if ($node/@ref='#mgem') then
                element xhtml:span {
                    attribute class {'g mgem'},
                    fontaneTransfo:magic($node/node())
                }
            else if ($node/@ref='#ngem') then
                element xhtml:span {
                    attribute class {'g ngem'},
                    fontaneTransfo:magic($node/node())
                }
            else if ($node/@ref="#rth") then
                element xhtml:span {
                    attribute class {'g rth'},
                    <xhtml:div class="hover">Reichstaler</xhtml:div>,
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="auto" viewBox="0 0 82 107">
                        {element path {
                            attribute fill {"none"},
                            attribute stroke {"darkslategrey"},
                            attribute stroke-width {"5"},
                            attribute d {"M 24.73,35.82" ||
                               " C 24.73,35.82 30.36,30.73 32.00,26.55" ||
                                " 46.73,45.27 80.55,37.45 70.00,38.18" ||
                                " 16.73,50.73 -2.55,99.09 19.09,101.82" ||
                                " 52.00,103.45 57.09,80.00 42.91,80.91" ||
                                " 11.82,83.64 35.45,105.45 52.00,99.64" }}}
                    </svg>
                    }
            else ()
    case element(tei:unclear)
    return
        element xhtml:span {
            attribute class {'unclear'},
            fontaneTransfo:magic($node/node())
        }
    case element(tei:gap)
        return
            fontaneTransfo:gap($node)
    case element(tei:milestone) return
        element xhtml:span {
            attribute class {'milestone', $node/@type, $node/@unit},
            attribute style {'display:none;'}
        }
    case element(tei:ref)
        return
            if($node/parent::tei:figDesc) then () else
            element xhtml:span {
                attribute class {'ref'},
                attribute id {'ref'||substring-after($node/@target, '#')},
                (: mouseover :)
                if(not(contains($node/@target, 'refxpath')) and substring-before(substring-after($node/@target, 'surface[@n'), "']")
                    = $node/ancestor::tei:surface[last()]/string(@n))
(: productive = $node/ancestor::tei:surface/tei:surface/string(@n)) :)
                then attribute onmouseover {
                    '$("[id=ref'|| substring-after($node/@target, '#') ||'], #'||substring-after($node/@target, '#')||'").toggleClass("highlight")'
                } else (),
                fontaneTransfo:magic($node/node()),
                (: link :)
                if(starts-with($node/@target, '#xpath(//surface') or not(contains($node/@target, $node/ancestor::tei:surface[last()]/@n)))
                then
                    element xhtml:span {
                        attribute class {'href'},
                        element xhtml:a {
                            if(not( contains($node/@target, 'xpath') ))
                            then (  attribute href {'edition.html?id=/xml/data/'|| $node/ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:ab/tei:idno/substring-after(., ':') ||'.xml&amp;page='|| substring-before(substring-after($node/@target, "_"), "_") ||'&amp;target='||substring-after($node/@target, "#")},
                                    <i class="fa fa-link"/>
                                )
                            else
                                (   attribute href {'edition.html?id=/xml/data/'|| $node/ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:ab/tei:idno/substring-after(., ':') ||'.xml&amp;page='||substring-before(substring-after($node/@target, "[@n='"), "']")},
                                    <i class="fa fa-link"/>
                                )
                        }
                    }
                else()
            }
    case element(tei:expan) return ()
    case element(tei:addSpan) return ()
    case element(tei:handShift) return ()
    case element(tei:lb) return ()
    case element(tei:anchor) return
        if( $node/ancestor::tei:sourceDoc//tei:add[@corresp = '#'||$node/@xml:id] )
        then
            element xhtml:span {
                attribute class { 'anchored' },
                fontaneTransfo:addPreselection( $node/ancestor::tei:sourceDoc//tei:add[@corresp = '#'||$node/@xml:id] )
            }
        else ()
    case text()
        return
            fontaneTransfo:text($node)
    default return fontaneTransfo:magic($node/node())
};
declare function fontaneTransfo:surfaceDiv($node as node(), $inital as xs:string) {(
            if($node/parent::tei:sourceDoc or string($node/@n) = $inital) then
                    element xhtml:div {
                    attribute class {'rowWrapper', 'clearfix', 'butthead'},
(:                    attribute id { $node/string(@n) },:)
                    fontaneTransfo:facs($node),
(:                    if($node/string(@type) = ("clipping", "additional")):)
                    element xhtml:div {
                        fontaneTransfo:surface($node, $inital),
                        fontaneTransfo:magic($node/node())
                    },
                    element xhtml:div {
                        attribute class {'teixml'},
                        element xhtml:pre {
                            element xhtml:b {
                                <xhtml:h3 class="xmlTitle">Auszug aus dem TEI/XML-Dokument</xhtml:h3>,
                                <xhtml:div>
                                    <xhtml:a href="xml.html?id=/xml/data/{$node/preceding::tei:teiHeader//tei:idno[@type="TextGrid"]/substring-after(., 'textgrid:')}.xml">Gesamtes TEI/XML-Dokument</xhtml:a>
                                    |
                                    <xhtml:a target="_blank" href="rest/data/{$node/preceding::tei:teiHeader//tei:idno[@type="TextGrid"]/substring-after(., 'textgrid:')}.xml">(via REST)</xhtml:a>
                                </xhtml:div>
                            },
                            element xhtml:code {
                                attribute class {'html'},
                                serialize($node)
                            }
                        }
                        }
                    }
            else
            (
            element xhtml:div {
                fontaneTransfo:surface($node, $inital),
                if($node/@type = ('clipping', 'additional'))
                then
                    fontaneTransfo:magic($node/node())
                else fontaneTransfo:magic($node/node())
            }),
            let $ids := $node//@xml:id,
                $notes := $node/ancestor::tei:TEI//tei:note[@type="editorial"]/tokenize(replace(@target, "#", ""), " "),
                $test := (($ids = $notes) and string($node/@n) = $inital)
            return
                if($test) then
                    element xhtml:div {
                        attribute class {"notes"},
                        element xhtml:ul {
                            for $i in $node/ancestor::tei:TEI//tei:note[@type="editorial"][tokenize(replace(@target, "#", ""), " ") = $ids]
                            return
                                element xhtml:li {
                                    fontaneTransfo:noteParser($i)
                                }
                        }

                    }
                else ()

)};
declare function fontaneTransfo:surface($n, $inital) {
attribute class {
    'surface',
    (if($n/@type) then (' tei'||$n/@type) else ()),
    (if($n/@subtype) then string($n/@subtype) else ()),
    if( string($n/@subtype)="Kalenderblatt") then if(ends-with($n/@n, 'v')) then 'verso' else 'recto' else (),
    if( $n/parent::tei:surface ) then "nested" else ()
},
(: dont call it a label (bootstrap) :)
(if($n/@n) then attribute id {"s"||$n/@n} else()),
attribute style {
  let $values := distinct-values((
    if ($n/(@ulx or @uly)) then ('position:absolute;', 'top:'|| $n/@uly ||'cm;', 'left:'|| $n/@ulx ||'cm;') else (),
    if ($n/(@ulx and @lrx)) then ('position:absolute;' ,'left:'|| $n/@ulx ||'cm;' , 'width:'||$n/@lrx - $n/@ulx||'cm;') else (),
    if ($n/(@uly and @lry)) then ('position:absolute;', 'top:'|| $n/@uly ||'cm;' , 'height:'||$n/@lry - $n/@uly||'cm;') else (),
    if ($n/@subtype = 'Kalenderblatt') then ( ('top:'|| $n/@uly ||'cm;', 'left:'|| $n/@ulx ||'cm;', 'width:'||$n/@lrx - $n/@ulx||'cm;', 'height:'||$n/@lry - $n/@uly||'cm;', "position:relative;") ) else ()
    )) (: distinct values :)
  return
    (: omit top, left and position in case of a nested surface :)
      if( string($n/@n) = $inital )
      then $values[not( contains(., "absolute") ) and not( starts-with(., "top:") ) and not( starts-with(., "left:") )]
      else $values
}
};

declare function fontaneTransfo:zone($n) {
attribute class {
            'zone',
            string($n/@type),
            string($n/@subtype),
            if (exists($n/preceding-sibling::tei:addSpan) and $n/preceding-sibling::tei:addSpan/substring-after(@spanTo, '#') = $n/following-sibling::tei:anchor/@xml:id ) then 'addSpan' else(),
            if (exists($n/preceding-sibling::tei:mod[@seq])
              and
                  $n/preceding-sibling::tei:mod[@seq]/substring-after(@spanTo, '#') = $n/following-sibling::tei:anchor/@xml:id
                  )
              then 'mod-seq-'|| string($n/preceding-sibling::tei:mod[@seq][substring-after(@spanTo, '#') = $n/following-sibling::tei:anchor/@xml:id]/@seq) else(),
            if ($n/tei:figure) then 'figure' else(),
            fontaneTransfo:segStyle($n),
            if( $n/@type="illustration" and $n/parent::tei:line) then "verticalMiddel" else (),
            if(not( $n//tei:line ) and $n/tei:figure[string(.//tei:ref) = "Diagonale Streichung"]) then "deletion-diagonal" else (),
            let $id := string($n/@xml:id)
            let $metamark := $n/ancestor::tei:surface[1]//tei:metamark[contains(string(@corresp), $id)]
            return
                if( ($id != "") and ( $metamark//tei:ref/ends-with(., 'gestrichen; erledigt') = true()) ) then 'hover' else ()
    },
attribute style {
(: place it! :)
    if (($n/@ulx or $n/@uly or $n/@lrx or $n/@lry) and not($n/ancestor::tei:zone/@rotate))
        then
            if ($n/ancestor::tei:zone[last()]//tei:figure)
                then
                ('position:absolute;',
                'top:'|| $n/@uly - sum($n/ancestor::tei:zone/@uly)||'cm;',
                'left:'||$n/@ulx -sum($n/ancestor::tei:zone/@ulx)||'cm;',
(:  'width:'|| (if($n/@lry) then $n/@lrx - (if($n/@ulx) then $n/@ulx else 0) - sum($n/ancestor::tei:zone/@lrx) ||'cm;'  else '' )||:)
                    'width:'||(if($n/@lry) then $n/@lrx - (if($n/@ulx) then $n/@ulx else 0) - (if ($n/parent::tei:line) then 0 else sum($n/ancestor::tei:zone/@lrx)) ||'cm;'  else '' ),
(:  'height:'|| $n/@lry - (if($n/@uly) then $n/@uly else 0)  - sum($n/ancestor::tei:zone/@lry)||'cm;':)
                   'height:'|| $n/@lry - (if($n/@uly) then $n/@uly else 0)  - (if ($n/parent::tei:line) then 0 else sum($n/ancestor::tei:zone/@lry)) ||'cm;'
                ) else
            if (not($n/@ulx|$n/@uly|$n/@lrx) and $n//tei:fw)
                    then 'width:100%;'
(:                then if( $n//text()[matches(., '\w')][not(./ancestor::tei:fw)]) then 'width:100%;' else 'width:100%;height:0;':)
            else
                'position:absolute;'||
                'top:'|| $n/@uly ||'cm;'||
                'left:'||$n/@ulx ||'cm;'||
                'width:'||$n/@lrx - (if($n/@ulx) then $n/@ulx else 0) ||'cm;'||
                'height:'|| $n/@lry - (if($n/@uly) then $n/@uly else 0)  ||'cm;'
    else(),
    if( $n/not(@ulx|@lrx|@rotate) and $n//tei:fw)
(:        then 'width:100%;':)
        then if( $n//text()[matches(., '\w')][not(./ancestor::tei:fw)]) then 'width:100%;' else 'width:100%;'
(: edited: 2016-12-07
 : cause: email  "Probleme mit Seitenzahlen; dringend"
 : was: then if( $n//text()[matches(., '\w')][not(./ancestor::tei:fw)]) then 'width:100%;' else 'width:100%;height:0;'
 :)
    else (),
    if($n/ancestor::tei:zone/@rotate) then
        let $rotation := sum( $n/ancestor::tei:zone/@rotate )
        let $value:=
            if($rotation gt 89 and $rotation lt 180) then 'position:absolute;'|| 'top:-'|| $n/@ulx ||'cm;left:'|| $n/@uly ||'cm;'
            else ()
        return if(contains($value, '-cm') or contains($value, ':cm')) then () else $value
    else(),
(:    if( preceding-sibling::tei:*[1]/local-name="line" ) then 'position:absolute;' else(),:)
(: rotate it! :)
(:    if ($n/@rotate and not( $n/parent::tei:surface/count(tei:zone) = 1)) :)
    if ($n/@rotate)
        then
            'transform:rotate('|| $n/@rotate ||'deg);'||'-webkit-transform:rotate('|| $n/@rotate ||'deg);'
    else (),
    fontaneTransfo:lineHeight($n),
(: look 4 TILE objects :)
    if ($n/tei:figure[@xml:id]) then
        let $id := string($n/tei:figure/@xml:id)
        let $uri := string($n/ancestor::tei:TEI//tei:idno[@type="TextGrid"])

        let $link := fontaneTransfo:newestTBLELink($uri, $id)
        let $shape := $link/substring-before(substring-after(@targets, '#'), ' ')
(:        let $image := $tei//svg:g[@id = $link/parent::tei:linkGrp/substring-after(@facs, '#')]/svg:image/@xlink:href:)
(:        let $image := ($n/ancestor::tei:surface)[1]/substring-after(@facs, "textgridrep.org/") || ".1":)

        let $svgg := $link/ancestor::tei:TEI//svg:g[@id = $link/parent::tei:linkGrp/substring-after(@facs, '#')]
        let $image := substring-before($svgg//svg:image/@xlink:href, ".") || ".1"
        let $x := number($svgg//svg:rect[@id = $shape]/substring-before(@x, '%'))
        let $y := number($svgg//svg:rect[@id = $shape]/substring-before(@y, '%'))
        let $w := number($svgg//svg:rect[@id = $shape]/substring-before(@width, '%'))
        let $h := number($svgg//svg:rect[@id = $shape]/substring-before(@height, '%'))


        let $rotation := sum( ($n/ancestor-or-self::tei:*/@rotate) )
        let $rotation := if($rotation != 0) then 360 - $rotation else 0
        let $furtherAttributes :=
                if($n/ancestor::tei:line)
                then "width:"|| $n/@lrx - $n/@ulx ||"cm; height:"||$n/@lry - $n/@uly ||"cm;"
                else ""
        return ("background-image: url('https://textgridlab.org/1.0/digilib/rest/IIIF/"|| $image || '/pct:'||$x||','||$y||','||$w||','||$h||"/,1000/"|| $rotation ||"/default.jpg'); background-repeat: no-repeat; background-size: 100% 100%;"||$furtherAttributes)
(:        digilib/"|| $image || '?dh=500&amp;dw=500&amp;wx='||$x||'&amp;wy='||$y||'&amp;ww='||$w||'&amp;wh='||$h||"&amp;mo=png&amp;rot="||$rotation||"'); background-repeat: no-repeat; background-size: 100% auto;"):)
    else (),

    (: like on C07 15r, a zone can contain a single (nonword) character that belongs to nearby lines. in this cases, we increase the font size by rule.
    this stays against our practise to encode a font-size if semantic matters. we should discuss this.
    :)
    if (sum($n/tei:line//text()/string-length(.)) = 1 and $n/tei:line//text()/matches(., '\W' ) ) then 'font-size:'||$n/@lry - $n/@uly||'cm' else (),
    if (sum($n/tei:line//text()/string-length(.)) = 1 and $n/tei:line//text()/matches(., '\W' ) and ($n/@lry - $n/@uly gt 1)) then 'transform:scaleX(0.5)' else (),

    (: override height statements before, if we want to prepare an inline zone :)
    if($n/parent::tei:line)
        then
            (
                'display:inline-block; position:relative; left:0; top:0;'
(:                ,:)
(:                if(sum( ($n/ancestor-or-self::tei:*/@rotate) ) = 90) then 'height:'|| $n/@lrx - $n/@ulx ||'cm; width:'|| $n/@lry - $n/@uly  ||'cm;' else ():)
            )
        else ()
},
if($n/@xml:id)
then
    attribute id {$n/@xml:id}
else (),
if($n/@xml:id and $n/ancestor::tei:TEI//tei:ref[substring-after(@target, '#')=$n/@xml:id])
then
    (: edit same line in ref too! :)
    attribute onmouseover {'$("[id=ref'|| $n/@xml:id ||'], #'||$n/@xml:id||'").toggleClass("highlight")'}
else (),
if (exists($n/preceding-sibling::tei:addSpan) and $n/preceding-sibling::tei:addSpan/substring-after(@spanTo, '#') = $n/following-sibling::tei:anchor/@xml:id ) then
    element xhtml:div {attribute class {'addSpanHover italic'}, 'Hinzufügung'}
    else(),
if($n/@rend="border-left-style:brace") then $fontaneTransfo:bracket_left.svg else (),
    let $id := string($n/@xml:id)
    let $metamark := $n/ancestor::tei:surface[1]//tei:metamark[contains(string(@corresp), $id)]
    return
if( ($id != "") and ( $metamark//tei:ref/ends-with(., 'gestrichen; erledigt') = true()) )
    then
        element xhtml:div {attribute class {'zoneHover'}, 'Erledigungsstreichung'}
    else ()
};

declare function fontaneTransfo:segStyle($node) {
(: returns a sequence of class names according to the given styles
 : not only for tei:seg, also used in tei:zone
:)
let
$style := $node/@style,
$rend :=  $node/@rend,

$type:= if(($node/local-name() = ("seg", "zone")) and ($node/@type = "cancel")) then
            if(not($node/@prev|$node/@next)) then "cancelLeft cancelRight"
            else if($node/@next) then "cancelLeft"
            else if($node/@prev) then "cancelRight" else ()
        else (),

$noLineThroughInCaseOfDeletionViaMetamark :=  if(
    contains($node/@style, 'text-decoration:line-through') and
    ($node/ancestor-or-self::tei:*/concat('#', @xml:id) = tokenize( $node/ancestor::tei:TEI//tei:metamark/@corresp, ' ')) )
    then " noLineThrough"
    else ()

 let $preMedium := fontaneTransfo:text( $node//text()[1])/tokenize(string(@class), ' ')
(: let $test := console:log( $preMedium ):)
(: let $preMedium := ():)

let $rend := replace($rend, '\s', '')

let $seq :=
    for $s in tokenize(replace(replace($style, 'radius:', 'radius'), '\s|\.|%|-style', ''), ';')
    return
        tokenize($s, ':')
(: -stlye now in pattern to be replaced
 :         for $t at $pos in tokenize($s, ':'):)
(:            return if (contains($t, '-style')) then substring-before($t, '-style') else $t:)

let $rendSeq :=
    for $r in tokenize(replace($rend, '\s', ''), ';')
    return
        replace(replace($r, ':', ' '), '[\(\)]', '')

let $medium := if ($preMedium = ('blue_pencil', 'violet_pencil', 'black_ink', 'blue_ink', 'brown_ink')) then $preMedium[. = ('blue_pencil', 'violet_pencil', 'black_ink', 'blue_ink', 'brown_ink')] else ()
let $test:= if($seq = ()) then ('TODO', $medium) else ($seq, $rendSeq, $medium, $type, $noLineThroughInCaseOfDeletionViaMetamark)

return
    $test ! replace(replace(., '/', '_'), '3_4', 'd3_4')
};

declare function fontaneTransfo:retrace($node){

element xhtml:span {
    attribute class {'retrace'},
    element xhtml:span {
        attribute class {'retraced'},
        fontaneTransfo:magic($node/text()),
        element xhtml:div {
            attribute class {'retraceHover', if($node/ancestor::tei:*/@rotate and $node/ancestor::tei:zone/preceding-sibling::tei:addSpan) then () else 'hoverTop'},
            (if ($node/ancestor::tei:zone/@rotate) then attribute style {'transform: rotate('|| 360 - sum($node/ancestor::tei:zone/@rotate) ||'deg);'}
            else ()),

            local:preText($node),

            element xhtml:span {
                attribute class {'italic'},
                '<'},
            $node//text(),
            element xhtml:span {
                attribute class {'italic'},
                ' nachgezogen '},
                $node//text(),
                element xhtml:span {
                attribute class {'italic'},
                '>'},
            local:postText($node)
        }
    }
}
};

declare function local:preText($n) {
let $directText := $n/preceding::text()
                [ancestor::tei:line = $n/ancestor::tei:line]
                [not(parent::tei:add[@place = ('below', 'above')])]
let $prvLineText := if($n/preceding::text()[ancestor::tei:line = $n/ancestor::tei:line][last()]/parent::*/@prev)
                    then ($n/ancestor::tei:TEI//*[@xml:id = substring-after($n/preceding::text()[ancestor::tei:line = $n/ancestor::tei:line][last()]/parent::*/@prev, '#')]//text())
                    else ()
let $prvLineText := if(
                        (string-join($prvLineText)="")
                        and ( not(matches(string-join($n/preceding::text()[ancestor::tei:line = $n/ancestor::tei:line]), '\s')))
                        and (
                               ends-with($n/ancestor::tei:line/preceding::tei:line[1], '-')
                            or ends-with($n/ancestor::tei:line/preceding::tei:line[1], '⸗')
                            )
                        )
                    then tokenize( string($n/ancestor::tei:line/preceding::tei:line[1]), ' ' )[last()]
                    else $prvLineText
return
    replace(tokenize(string-join( ($prvLineText, $directText) ), '\s|\.|,|;|&#x2003;' )[last()], $fontaneTransfo:tooltipReplacementPattern, '' )
};

declare function local:postText($n) {
(:  former mod hover::)
let $text := if(starts-with($n/following::text()[1][ancestor::tei:line = $n/ancestor::tei:line], ' '))
                            then ()
                            else
                                if( matches(string-join($n/following::text()[ancestor::tei:line = $n/ancestor::tei:line]), '\s' ))
                                then tokenize(string-join($n/following::text()[ancestor::tei:line = $n/ancestor::tei:line]), '\s' )[1]
                                else if(not($n/@next) and matches($n/ancestor::tei:line, '-$|⸗$' ))
                                then
                                    tokenize(string-join(
                                        ($n/following::text()[ancestor::tei:line = $n/ancestor::tei:line],
                                        $n/following::text()[ancestor::tei:line = $n/ancestor::tei:line/following::tei:line[1]])), ' ')[1]
                                else ()
let $text :=    if( $n/@next )
                then
                    let $completeTEI := doc( '/db/sade-projects/textgrid/data/xml/data/' || substring-after($n/preceding::tei:idno[@type="TextGrid"], 'textgrid:') || '.xml')//tei:TEI
                    return
                        $text || string($completeTEI//*[@xml:id= substring-after($n/@next, '#')])
                else $text

return if($text = "(") then $text else replace( $text , $fontaneTransfo:tooltipReplacementPattern, '' )

(:    former retrace hover::)
(:    if(starts-with($n/following::text()[ancestor::tei:line][1], '\s|\.|,|;|-|\)|\(')) then () else tokenize($n/following::text()[ancestor::tei:line][1], ' ')[1]:)
};

declare function fontaneTransfo:stamp($node) {
switch ($node/string(.))
case 'FONTANE.' return
                    <svg xmlns="http://www.w3.org/2000/svg" width="23mm" height="4mm" data-info="Stempel: FONTANE.">
                        <g alignment-baseline="baseline">
                            <text x="0mm" y="3.5mm" style="fill: purple;stroke: none;font-size:4mm; font-family:Ubuntu-Light, Verdana, sans-serif;">FONTANE.</text>
                        </g>
                    </svg>

case 'STAATSBIBLIOTHEK •BERLIN•' return
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="24.2mm" height="15.2mm" data-info="Stempel: STAATS- BIBLIOTHEK BERLIN">
                        <defs>
                            <filter id="unschaerfe" color-interpolation-filters="sRGB">
                                <feGaussianBlur stdDeviation="0.0"/>
                            </filter>
                        </defs>
                        <g alignment-baseline="baseline">
                            <g filter="url(#unschaerfe)">
                                <rect x="0.5mm" y="0.5mm" width="23mm" height="14mm" rx="1mm" fill="none" stroke="black" stroke-width="1mm"/>
                                <text
                                    style="stroke:none; font-family:FreeSerif, serif; font-size: 3.4mm; font-weight: bold;">
                                    <tspan id="zeile1" x="5mm" y="4mm">STAATS-</tspan>
                                    <tspan id="zeile2" x="1mm" y="8.5mm">BIBLIOTHEK</tspan>
                                    <tspan id="zeile3" x="3mm" y="13mm">∙ BERLIN ∙</tspan>
                                </text>
                            </g>
                        </g>
                    </svg>

case 'DSB Font.-Arch. Potsdam' return
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="15.2mm" height="15.2mm" data-info="Stempel: DSB Font.-Arch. Potsdam">
                        <defs>
                            <filter id="unschaerfe" color-interpolation-filters="sRGB">
                                <feGaussianBlur stdDeviation="0.3"/>
                            </filter>
                        </defs>
                        <g alignment-baseline="baseline">
                            <circle cx="7.5mm" cy="7.5mm" r="7.25mm" fill="none" stroke="darkblue" stroke-width="0.5mm"/>
                            <text style="font-size:65%; font-family:FreeSans;" fill="darkblue">
                                <tspan id="zeile1" x="4.5mm" y="4mm">DSB</tspan>
                                <tspan id="zeile2" x="0.9mm" y="7.5mm">Font.-Arch.</tspan>
                                <tspan id="zeile3" x="2mm" y="11mm">Potsdam</tspan>
                            </text>
                        </g>
                    </svg>
case "BERLIN W.West" return
                    <img src="/public/img/stempel-berlin-w.png" width="100%" height="100%"/>
default return
                    <svg xmlns="http://www.w3.org/2000/svg" width="23mm" height="3.5mm">
                        <g alignment-baseline="baseline">
                            <text x="0mm" y="3.5mm" style="font-size:4mm; font-family:Ubuntu-Light, Verdana, sans-serif;" fill="purple">Stempel nicht unterstützt oder Fehler im Code.</text>
                        </g>
                    </svg>
};
declare function fontaneTransfo:text($n) {
let $followingAnchorWithModBeforeThisNode :=  $n/following::tei:anchor[@xml:id = $n/preceding::tei:mod[@seq]/substring-after(@spanTo, '#')]/('#'||@xml:id)
let $allModBefore := $n/preceding::tei:mod[@spanTo = $followingAnchorWithModBeforeThisNode]
let $modClasses := for $mod in $allModBefore return ("modSeq" || string($mod/@seq), fontaneTransfo:segStyle($mod))

(:if( $n/preceding::tei:mod[@seq][1] ) :)
(:                then :)
(:                    let $mod := $n/preceding::tei:mod[@seq][1]:)
(:                    let $spanTo := $mod/substring-after(@spanTo, '#'):)
(:                    return:)
(:                        if( $n/following::tei:anchor[@xml:id = $spanTo] ):)
(:                        then ("seq", string($mod/@seq)):)
(:                        else ():)
(:                else ():)
return
(: steht Text nur innerhalb von tei:line? ja. :)
if ($n/ancestor::tei:line) then
element xhtml:span {
    attribute class {
        $modClasses,
        if(not(
            starts-with(
                $n/preceding::handShift[@new][1]/substring-after(@new, '#')
            , 'Archivar')
            )) then (
            $n/preceding::handShift[@medium][1]/@medium,
            $n/preceding::handShift[@new][1]/substring-after(@new, '#'),
            if ($n/preceding::handShift[@new][1]/substring-after(@new, '#') = '') then 'Fontane' else (),
            $n/preceding::handShift[@rendition][1]/@rendition,
            (if (contains($n/preceding::handShift[@script][1]/@script, ' '))
            then $n/preceding::handShift[@script][1]/@script
            else
                if($n/preceding::handShift[@script][1]/@script = ('standard', 'clean', 'hasty', 'angular'))
                then $n/preceding::handShift[contains(@script, 'Latn') or contains(@script, 'Latf')][1]/tokenize(@script, ' ')[. = ('Latn', 'Latf')] || ' ' || $n/preceding::handShift[@script][1]/@script
                (: ^^^ no test for an additional whitespace together with Latn or Latf, characteristics will be overwritten by the current @script :)
            else
                if ($n/preceding::handShift[@script][1]/@script = ('Latn', 'Latf'))
                then $n/preceding::handShift[contains(@script, 'standard') or contains(@script, 'clean') or contains(@script, 'hasty') or contains(@script, 'angular')][1]/tokenize(@script, ' ')[. = ('standard', 'clean', 'hasty', 'angular')] || ' ' || $n/preceding::handShift[@script][1]/@script
            else ''
            ))
        else (
            $n/preceding::handShift[@new][1]/substring-after(@new, '#')
            )
    },
    attribute data-info {
        let $hand := $n/preceding::handShift[@new][1]/substring-after(@new, '#')
        let $hand := switch($hand)
                        case '' return 'Theodor Fontane'
                        case 'Fontane' return 'Theodor Fontane'
                        case 'Friedrich_Fontane' return 'Friedrich Fontane'
                        default return $hand
        let $medium := tokenize(string($n/preceding::handShift[@medium][1]/@medium), ' ')
        let $medium := for $m in $medium
                        return
                            switch($m)
                                case 'pencil' return 'Bleistift'
                                case 'thick_pencil' return 'dicker Bleistift'
                                case 'blue_pencil' return 'Blaustift'
                                case 'violet_pencil' return 'Violettstift'
                                case 'black_ink' return 'schwarze Tinte'
                                case 'blue_ink' return 'blaue Tinte'
                                case 'brown_ink' return 'braune Tinte'
                                case 'thin_pen' return 'feine Feder'
                                case 'thick_pen' return 'breite Feder'
                                case 'medium_pen' return 'mittlere Feder (Standard)'
                                default return $m
        let $medium := string-join($medium, ', ')
        let $style := $n/preceding::handShift[@script][tokenize(@script, ' ') = ('standard', 'clean', 'hasty', 'angular')][1]/tokenize(@script, ' ')[. =('standard', 'clean', 'hasty', 'angular')]
        let $style := for $s in $style
                        return
                            switch($s)
                            case 'standard' return 'Standard'
                            case '' return  'Standard'
                            case 'clean' return 'Reinschriftlich'
                            case 'hasty' return 'Unruhig'
                            case 'angular' return 'Gezackt'
                            default return $s
        let $script := $n/preceding::handShift[@script][tokenize(@script, ' ') = ('Latn', 'Latf')][1]/tokenize(@script, ' ')[. = ('Latn', 'Latf')]
        let $script := for $s in $script
                        return
                            switch($s)
                            case 'Latn' return 'Latein'
                            case 'Latf' return 'Kurrent'
                            default return $s
        return
        ('Schreiberhand:', replace($hand, '_', ' '),
        '|',
        'Medium:', $medium,
        '|',
        'Schrift:', $script,
        '|',
        'Duktus:', $style)
    },
    if ($n/matches(., '.[\w\[\d]') and
        string($n/preceding::tei:handShift[@new][1]/@new) != string($n/preceding::text()[1]/preceding::tei:handShift[@new][1]/@new) )
    then
        (attribute id { 'new'||$n/preceding::handShift[@new][1]/substring-after(@new, '#') },
        attribute data-handtoggle { $n/preceding::handShift[@new][1]/substring-after(@new, '#') })
    else (),

    (: manipulate the string in case of Gemination :)
    if($n/parent::tei:g) then (
        switch ($n/parent::tei:g/@ref)
            case '#mgem' return '&#xe095;'
            case '#ngem' return '&#xe096;'
            default return ()
        )
    else
        let $stringSeq := tokenize( $n/string() , '&#x20b0;|&#x2114;|&#x2032;|&#x271d;|&#x3003;|&#x204a;c\.')
        return if(count($stringSeq) gt 1) then
            if(matches( $n,'&#x20b0;'))
            then (substring-before($n, '&#x20b0;'), <xhtml:span class="sonderzeichen">&#x20b0;<xhtml:div class="hover">Pfennig</xhtml:div></xhtml:span> ,substring-after($n, '&#x20b0;'))
            else if(matches( $n,'&#x2114;'))
            then (
                let $seq := tokenize($n, '&#x2114;')
                return
                    switch( count($seq) )
                    case 2 return
                        ( substring-before($n, '&#x2114;'), <xhtml:span class="sonderzeichen">&#x2114;<xhtml:div class="hover">Pfund</xhtml:div></xhtml:span> ,substring-after($n, '&#x2114;'))
                    case 3 return
                        ( substring-before($n, '&#x2114;'), <xhtml:span class="sonderzeichen">&#x2114;<xhtml:div class="hover">Pfund</xhtml:div></xhtml:span> ,
                            substring-before(substring-after($n, '&#x2114;'), '&#x2114;'), <xhtml:span class="sonderzeichen">&#x2114;<xhtml:div class="hover">Pfund</xhtml:div></xhtml:span> ,
                            substring-after(substring-after($n, '&#x2114;'), '&#x2114;') )
                    default return 'TODO: add item to sequence'
                )
            else if(matches( $n,'&#x2032;'))
            then (substring-before($n, '&#x2032;'), <xhtml:span class="sonderzeichen">&#x2032;<xhtml:div class="hover">Fuß</xhtml:div></xhtml:span> ,substring-after($n, '&#x2032;'))
            else if(matches( $n,'&#x271d;'))
            then (substring-before($n, '&#x271d;'), <xhtml:span class="sonderzeichen">&#x271d;<xhtml:div class="hover">Kirche</xhtml:div></xhtml:span> ,substring-after($n, '&#x271d;'))
            else if(matches( $n,'&#x3003;'))
            then (substring-before($n, '&#x3003;'), <xhtml:span class="sonderzeichen">&#x3003;<xhtml:div class="hover">Unterführung</xhtml:div></xhtml:span> ,substring-after($n, '&#x3003;'))
            else if(matches( $n,'&#x204a;c\.'))
            then (substring-before($n, '&#x204a;c.'), <xhtml:span class="sonderzeichen">&#x204a;c.<xhtml:div class="hover">etc.</xhtml:div></xhtml:span> ,substring-after($n, '&#x204a;c.'))
            else ()
        else $stringSeq
    }
else ()
};

declare function fontaneTransfo:facs($node) {

    element xhtml:div {
        attribute class {'facs'},
        if($node/parent::tei:sourceDoc or not($node/tei:graphic/@xml:id)) then () else
            attribute style {
                "width:"|| number($node/@lrx) - number($node/@ulx) ||"cm;",
                "height:"|| number($node/@lry) - number($node/@uly) ||"cm;"
            },
        element xhtml:a {
            attribute href { 'digilib/'||( string($node/tei:graphic[1]/@n) || '.jpg')||'?m2' },
            attribute target {'_blank'},
            if($node/@subtype="Kalenderblatt" and ends-with($node/@n, "v"))
            then
                fontaneTransfo:KalenderblattSVG(
                    string($node/tei:graphic[@xml:id][1]/@xml:id),
                    string($node/ancestor::tei:TEI//tei:publicationStmt//tei:idno[@type="TextGrid"]))
            else
            element xhtml:img {
                attribute class {('facs', string( ($node/@subtype)[1] ))},
                attribute src {fontaneTransfo:digilib($node)}
            }
        }
    }
};

(: test :)

declare function fontaneTransfo:digilib($node){

(: TILE object available :)
if($node/tei:graphic/@xml:id)
  then

      let $id := ($node/tei:graphic/string(@xml:id))[1]
      let $uri := $node/ancestor::tei:TEI//tei:publicationStmt//tei:idno[@type="TextGrid"]/string(.)
      let $link := collection('/db/sade-projects/textgrid/data/xml/tile/')//tei:link[contains(@targets, $uri)][ends-with(@targets, $id)][last()]
      let $facs := $link/parent::tei:linkGrp/substring-after(@facs, '#')
      let $tei := $link/ancestor::tei:TEI
      let $shape := $link/parent::tei:linkGrp/tei:link[contains(@targets, 'shape')][ends-with(@targets, $id)]/substring-before(substring-after(@targets, '#'), ' ')

      let $svgg := $tei//svg:g[@id = $facs]
      let $image := string($svgg/svg:image/@xlink:href)
      let $image := if ($image = "") then $node/tei:graphic/substring-after(@url, "http://textgridrep.org/") else $image

      let $x := number($svgg//svg:rect[@id = $shape]/substring-before(@x, '%')) (: div 100 :)
      let $y := number($svgg//svg:rect[@id = $shape]/substring-before(@y, '%')) (: div 100 :)
      let $w := number($svgg//svg:rect[@id = $shape]/substring-before(@width, '%')) (: div 100 :)
      let $h := number($svgg//svg:rect[@id = $shape]/substring-before(@height, '%')) (: div 100 :)

    return
(:        'https://textgridlab.org/1.0/digilib/rest/digilib?fn='|| $image || '&amp;dh=500&amp;dw=500&amp;wx='||$x||'&amp;wy='||$y||'&amp;ww='||$w||'&amp;wh='||$h||"&amp;mo=png":)

         'https://textgridlab.org/1.0/digilib/rest/IIIF/'|| $image || '/pct:' || string-join(($x, $y, $w, $h), ",") || '/,500/0/default.jpg'
(:        https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164g9.1/pct:7.747474581830108,5.289308105687416,84.44747294194818,89.91823779668606/,1000/0/default.jpg:)

else
(: typical cover :)
let
$currentNotebook := substring-before( string(($node/tei:graphic/@n)[1]), '_'),
$n := $node/@n,
$type := local-name($node[1]),
$facs := if (not(contains($node/tei:graphic/@n, ' '))) then ($node/tei:graphic/@n)[1] || '.jpg' else substring-before( ($node/tei:graphic/@n)[1] , ' ') || '.jpg',
$facs := $node/tei:graphic/substring-after(@url, "http://textgridrep.org/"), (: complete base uri :)
$surfaceWidth := $node/ancestor::tei:TEI//extent[1]/dimensions[@type = 'leaf']/width[1]/@quantity,
$surfaceHeight := $node/ancestor::tei:TEI//extent[1]/dimensions[@type = 'leaf']/height[1]/@quantity,
$bindingWidth := $node/ancestor::tei:TEI//extent[1]/dimensions[@type = 'binding']/width[1]/number(@quantity),
$bindingHeight := $node/ancestor::tei:TEI//extent[1]/dimensions[@type = 'binding']/height[1]/@quantity,
$dpcm := 236.2205, (:  600 dpi = 236.2205 dpcm  :)
$exif := doc('/db/sade-projects/textgrid/data/xml/data/217qs.xml'),
$covertble := doc('/db/sade-projects/textgrid/data/xml/tile/218r2.xml'),
$image-width := number($exif//digilib:image[@uri = $facs]/@width),
$image-height := number($exif//digilib:image[@uri = $facs]/@height),
$plusX := $exif//digilib:offset[@notebook = $currentNotebook][@x]/@x,
$plusX := if ($plusX = '') then 1 else $plusX,
$plusY := $exif//digilib:offset[@notebook = $currentNotebook][@y]/@y,
$plusY := if ($plusY = '') then 1 else $plusY,
$shape := if($type = 'cover' and $exif//digilib:image[@name = $facs]/@xml:id) then (substring-after(substring-before($covertble//tei:link[ends-with(@targets, $exif//digilib:image[@name = $facs]/@xml:id)][1]/@targets, ' '), '#')) else 0,
$scaler := 'https://textgridlab.org/1.0/digilib/rest/digilib?fn=',
$scaler :=  'https://textgridlab.org/1.0/digilib/rest/IIIF',

$ulx := $node/@ulx,
$uly := $node/@uly,
$lrx := $node/@lrx,
$lry := $node/@lry,

(: lets start building our url :)
$image := $facs,
$resolution := switch ($type)
                    case 'thumb' return ("", 150)
                    case 'thumb-link' return ("", 1500)
                    case 'surface-empty' return ("", 300)
                    case 'figure' return ("", 500)
                    default return ("",1000),

(: wx , wy :)
$offset := switch($type)
                    case 'figure' return
                                    if (matches($n, '\d[a-z]*r|Ir$')) then (
                                        (($plusX + $ulx + $surfaceWidth div 10) * $dpcm div $image-width) * 100,
                                        (($plusY + $uly) * $dpcm div $image-height) * 100 )
                                    else if(matches($n, '\d[a-z]*v|Iv$')) then (
                                        (($plusX + $ulx) * $dpcm div $image-width) * 100,
                                        (($plusY + $uly) * $dpcm div $image-height) * 100 )
                                    else ()
                    case 'cover' return (
                                    number(substring-before($covertble//svg:rect[@id = $shape][1]/@x, '%')),
                                    number(substring-before($covertble//svg:rect[@id = $shape][1]/@y, '%')))
                    default return
                                if(contains($n, 'outer')) then (
                                    $dpcm div $image-width * 100,
                                    $dpcm div $image-height * 100 )
                                else if($n = 'inner_back_cover') then (
                                    number($plusX * $dpcm div $image-width + ($surfaceWidth div 10) * ($dpcm div $image-width)) * 100,
                                    number($plusY * $dpcm div $image-height) * 100)
                                else if($n = 'inner_front_cover') then (
                                    ($dpcm div $image-width) * 100,
                                    ($dpcm div $image-height) * 100 )
                                else if (matches($n, '\d[a-z]*r|Ir$')  and not(matches($n, "\dv[a-z]r"))) then (
                                    number($plusX * ($dpcm div $image-width) + ($surfaceWidth div 10) * ($dpcm div $image-width)) * 100,
                                    ($plusY * $dpcm div $image-height) * 100)
(:                                    ||console:log($plusX):)
                                else if(matches($n, '\d[a-z]*v|Iv$') or matches($n, "\dv[a-z]r")) then (
                                    ($plusX * $dpcm div $image-width) * 100,
                                    ($plusY * $dpcm div $image-height) * 100 )
                                else (0, 0),

(: ww , wh :)
$range := if ($n = 'none') then (1, 1)
        else if ($type = 'figure') then ( (($lrx - $ulx) * $dpcm div $image-width) * 100,  (($lry - $uly) * $dpcm div $image-height ) * 100 )
        else if ($type = 'cover' and $exif//digilib:coveroffset[@notebook = $currentNotebook]) then (
                    number(substring-before($covertble//svg:rect[@id = $shape][1]/@width, '%')) ,
                    number(substring-before($covertble//svg:rect[@id = $shape][1]/@height, '%')) )
        else if (contains($n, 'outer')) then (

            number(($bindingWidth div 10) * $dpcm div $image-width) * 100,
            number(($bindingHeight div 10) * $dpcm div $image-height) * 100 )
        else (
            number((($surfaceWidth div 10) + 0.5) * $dpcm div $image-width) * 100,
            number((($surfaceHeight div 10) + 0.5) * $dpcm div $image-height) * 100 )

return if($type = 'total') then concat($scaler, $image, $resolution, '&amp;mo=png')
else
    string-join(($scaler, $image, "pct:" || string-join(($offset[1], $offset[2], $range[1], $range[2]), ","), string-join($resolution, ","), 0, "default.jpg" ), "/")
(:        https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164g9.1/pct:7.747474581830108,5.289308105687416,84.44747294194818,89.91823779668606/,1000/0/default.jpg:)


};

declare function fontaneTransfo:KalenderblattSVG($id, $uri){
let $link := collection('/db/sade-projects/textgrid/data/xml/tile/')//tei:link[contains(@targets, $uri)][ends-with(@targets, $id)][last()]
let $facs := $link/parent::tei:linkGrp/substring-after(@facs, '#')
let $tei := $link/ancestor::tei:TEI
let $shape := $link/parent::tei:linkGrp/tei:link[contains(@targets, 'shape')]//substring-before(substring-after(@targets, '#'), ' ')

let $svgg := $tei//svg:g[@id = $facs]
let $image := string($svgg/svg:image/@xlink:href)

let $width := number($svgg/svg:image/@width)
let $height :=number($svgg/svg:image/@height)

let $x := round(number($svgg//svg:rect[@id = $shape]/substring-before(@x, '%')) div 100 * $width)
let $y := round(number($svgg//svg:rect[@id = $shape]/substring-before(@y, '%')) div 100 * $height)
let $w := round(number($svgg//svg:rect[@id = $shape]/substring-before(@width, '%')) div 100 * $width)
let $h := round(number($svgg//svg:rect[@id = $shape]/substring-before(@height, '%')) div 100 * $height)

let $transform := tokenize( substring-before(substring-after($svgg//svg:rect[@id = $shape]/@transform, 'rotate('), ')'), ',\s')
let $rotateDeg:= round(number($transform[1]))
let $rotateX := round(number(substring-before($transform[2], '%')) div 100 * $width)
let $rotateY := round(number(substring-before($transform[3], '%')) div 100 * $height)

let $url := 'digilib/'|| $image || "?dh=1000&amp;mo=png"
return
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25%" height="auto" transform="rotate(180deg, {$rotateX}, {$rotateY})" viewBox="{$x} {$y} {$w} {$h}" preserveAspectRatio="none">
<!--  -->
<g id="image" transform="rotate({360 - $rotateDeg}, {$rotateX}, {$rotateY})">
	<image width="{$width}" height="{$height}" xlink:href="{$url}" clip-path="url(#shape1)"/>
	<!-- calculate relative points to aboslute coordinates -->
	<clipPath id="shape1">
		<rect x="{$x}" y="{$y}" width="{$w}" height="{$h}" transform="rotate({$rotateDeg}, {$rotateX}, {$rotateY})"/>
	</clipPath>
</g>
</svg>
};

declare function fontaneTransfo:fraction($n) {
element xhtml:span {
    attribute class {'fraction', fontaneTransfo:magic( $n/tei:seg[@style='vertical-align:super']/text() )/@class},
    attribute style {$n/@style},
    element xhtml:span { attribute class {'top'},
        fontaneTransfo:magic( $n/tei:seg[@style='vertical-align:super']/text() )
    },
    element xhtml:span { attribute class {'bottom'},
        fontaneTransfo:magic( $n/tei:seg[@style='vertical-align:sub']/text() )
    }
}

};

declare function fontaneTransfo:caret($node) {
let $bow := <svg:svg width="{substring-before(substring-after($node/@rend, 'caret:bow('), ',pos-')}" height="1cm" viewBox="0 0 100 500" style="{concat(substring-before(substring-after($node/@rend, 'pos-'), ')'), ':-20;')}">
           <svg:path d="M450,250 C500,500 0,250 0,500" stroke="grey" fill="transparent" stroke-width="20"/>
           </svg:svg>

let $curvedV :=
    let $width:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ','),
        $w1:=number(substring-before($width[1], 'cm')),
        $w2:=number(substring-before($width[2], 'cm')),
        $x:=($w1 * 100) div ($w1 + $w2)
    return
        <svg:svg style="{(if(not(contains($node/@rend, 'pos'))) then 'left:0;' else() ), 'top: 10px;'}" width="{concat((number(substring-before($width[1], 'cm')) + number(substring-before($width[2], 'cm'))), 'cm')}" height="1cm" viewBox="0 0 100 100">
                        <svg:line style="stroke:grey;stroke-width:5" y2="100" x2="{$x}"
                            y1="0" x1="0"/>
                        <svg:line style="stroke:grey;stroke-width:5" y2="0" x2="100"
                            y1="100" x1="{$x}"/>
        </svg:svg>

let $lineHeight := fontaneTransfo:lineHeight( ($node/ancestor::tei:zone)[1] ),
    $lineHeight := number(replace($lineHeight, '[^\d.\d]', '')),
    $lineHeight := if(string(number($lineHeight)) = 'NaN') then 1 else $lineHeight,

    $caretType:=    if($node/local-name() != "metamark")
                    then tokenize( $node/string(@rend), '\(' )[starts-with(., 'caret')]
                    else "caret:" || tokenize($node/string(@rend), "\(")[1],
    $caret := switch($caretType)
                case 'caret:V'
                    return
                        let (: $test := console:log($node/string(@rend)) , :)
                            $parameter := substring-before( substring-after( $node/string(@rend), '('), ')'),
                            (: $test := console:log($parameter) , :)
                            $parameter := tokenize(replace($parameter, 'cm', ''), ','),
                            (: $test := console:log($parameter) , :)
                            $l := number($parameter[1]) * 100,
                            $r := number($parameter[2]) * 100,
                            $height := '100',
                            $viewbox := '0 0 '||string($l+ $r)||' '||$height,
                            (: $test := console:log($viewbox ) , :)
                            $style := $node/string(@style),
(:                            Unabhängig von margin-left in style, muss bei einem V der margin-left wert gleich dem 1. Wert aus der Funktion sein. :)
                            $style := $style || ';margin-left:-'||$parameter[1]||'cm;'
                        return
                            <svg xmlns="http://www.w3.org/2000/svg" class="caretV" style="{$style}" width="{string(($l + $r) div 100)}cm" height="0.6cm" viewbox="{$viewbox}">
                                <polyline points="0,0 {string($l)},{$height} {string($l + $r)},0" fill="none" stroke-width="3"/>
                            </svg>
                case 'caret:slash' return
                            (: also used when place=superimposed, see function add :)
                            <svg class="teicaret slash" xmlns="http://www.w3.org/2000/svg" style="right: -8px;" height="20" width="20">
                                <line y2="0" x2="20" y1="20" x1="5"/>
                            </svg>
                case 'caret:backslash' return
                            let $seq := tokenize($node/string(@style), ':|\s')
                            let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1
                            return
                            <svg xmlns="http://www.w3.org/2000/svg" style="left: calc({$seq[$mlnum]} - 6px);" height="20" width="20">
                                <line x1="0" y1="0" x2="15" y2="20"/>
                            </svg>
                case 'caret:curved_V' return
                            let $seq := tokenize($node/string(@style), ':|\s')
                            let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
                                $mlnum := string(number( substring-before( $seq[$mlnum], 'cm') ) - 0.1) || 'cm'
                            let $width:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ','),
                                $w1:=number(substring-before($width[1], 'cm')),
                                $w2:=number(substring-before($width[2], 'cm')),
                                $x:=($w1 * 100) div ($w1 + $w2),
                                $width:= $w1 + $w2,
                                $viewBoxHeight := 100,
                                $viewBoxWidth := 300,
                                $turn1 := round( (($viewBoxWidth div 2) * ($w1 div $w2)) ),
                                $turn := ($viewBoxWidth div $width * $w1) + 10,

                                (: margin top according to https://rechneronline.de/function-graphs/graph.php?a0=2&a1=0%2C0375x%2B1%2C0375&a2=&a3=&a4=1&a5=4&a6=8&a7=1&a8=1&a9=1&b0=500&b1=500&b2=-10&b3=0&b4=0.75&b5=1.25&b6=10&b7=10&b8=5&b9=5&c0=3&c1=0&c2=1&c3=1&c4=1&c5=1&c6=1&c7=0&c8=0&c9=0&d0=1&d1=20&d2=20&d3=0&d4=&d5=&d6=&d7=&d8=&d9=&e0=&e1=&e2=&e3=&e4=14&e5=14&e6=13&e7=12&e8=0&e9=0&f0=0&f1=1&f2=1&f3=0&f4=0&f5=&f6=&f7=&f8=&f9=&g0=&g1=1&g2=1&g3=0&g4=0&g5=0&g6=Y&g7=ffffff&g8=a0b0c0&g9=6080a0&h0=1&z :)
                                $top := round( (($lineHeight - 1.0375) div 0.0375) - 6 )

                            return
                            (<svg xmlns="http://www.w3.org/2000/svg" class="curved_V" height="100%" width="{$width}cm"
                            viewBox="0 0 {$viewBoxWidth} {$viewBoxHeight}" preserveAspectRatio="none"
                            style="left:{ 0 - $width div $viewBoxWidth * $turn }cm; top:{$top}px;">
                                <path d="
                                M  10,40 C   0, 45   0, 55  10,60
                                M  10,60 C 10, 60 {$turn + 5}, 60 {$turn + 5},80
                                M {$turn + 5},80 C {$turn + 5}, 80 {$turn + 2}, 87 {$turn},80
                                M {$turn},80 C {$turn + 2}, 50 {290 - (290 - $turn) div 2}, 60 290,60
                                M 290,60 C 300, 55 300, 45 290,40
                                " stroke-width="2" fill="none" />
                                <data w1="{$w1}" w2="{$w2}" turn1="{$turn1}" turn="{$turn}"  />
                            </svg>)
                case 'caret:bow' return
                    let $params:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ',')
                    let $width := $params[1]
                    let $pos := $params[2]

                    let $seq := tokenize($node/string(@style), ':|\s')
                    let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
                        $mlnum := number( substring-before( $seq[$mlnum], 'cm') )

                    let $viewW := number( substring-before($width, 'cm') ) * 100,
                        $p := $viewW - ($viewW * 0.05),
                        $q := $viewW * (3 div 8)
                    return
                        switch($pos)
                            case 'pos-right' return
(:                                <svg width="{$width}" height="{$lineHeight}cm" viewBox="0 0 {$viewW} 80" preserveAspectRatio="none" style="left:{string($mlnum - 0.1)}cm;top: 0;">:)
                                <svg xmlns="http://www.w3.org/2000/svg" width="{$width}" height="{$lineHeight}cm" viewBox="0 0 {$viewW} 80" preserveAspectRatio="none" style="left:-0.1cm;top: 0;">
                                    <path d="
                                    M{$p},  5 C{$p},  5 {$viewW}, 10 {$p}, 18
                                    M{$p}, 18 C{$p}, 18   0, 10   5, 60
                                    M  5, 60 C {$viewW div 10},100 {$viewW div 3}, 60  {$q - 15}, 70
                                    " fill="none" stroke-width="2"/>
                                </svg>
                            case 'pos-left' return
                                <svg xmlns="http://www.w3.org/2000/svg" width="{$width}" height="{$lineHeight}cm" viewBox="0 0 {$viewW} 80" preserveAspectRatio="none" style="left:{string($mlnum - 0.3)}cm;top: 0;">
                                    <path d="
                                    M10, 5 C10,5 0,15 10,20
                                    M10,20 C{$viewW div 3},35 {$viewW},30 {$viewW - 5},65
                                    M{$viewW - 5},65 C{$viewW - 5},80 {($viewW div 2) + ($viewW * 0.25)},75 {($viewW div 2) + ($viewW * 0.25)},75
                                    " fill="none" stroke-width="2"/>
                                </svg>
                            default return <xhtml:div>{$pos}</xhtml:div>
                case 'caret:retraced_half-bow' return
                    let $params:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ',')
                    let $width := $params[1]
                    let $pos := $params[2]

                    let $seq := tokenize($node/string(@style), ':|\s')
                    let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
                        $mlnum := number( substring-before( $seq[$mlnum], 'cm') )
                    let $viewW := number( substring-before($width, 'cm') ) * 100

                    return
                        switch($pos)
                            case 'pos-right' return
                            <svg xmlns="http://www.w3.org/2000/svg" width="{$width}" height="{$lineHeight}cm" viewBox="0 0 {$viewW} 100" preserveAspectRatio="none" style="left:{string($mlnum - 0.3)}cm;top: 0;">
                                            <path d="
                                            M30,95 C30,95 5,100 5,60
                                            M 5,60 C 5,20 {$viewW div 2},10 {$viewW - 5},5
                                            " fill="none" stroke-width="2"/>
                                            <path d="
                                            M35,95 C35,95 10,100 10,60
                                            M 10,60 C 10,20 {$viewW div 2},10 {$viewW - 5},10
                                            " fill="none" stroke-width="2"/>
                            </svg>
                        default return <xhtml:div>{$pos} no example in Gesamtdoku</xhtml:div>
                case 'caret:half-bow' return
                    let $params:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ',')
                    let $width := $params[1]
                    let $pos := $params[2]

                    let $seq := tokenize($node/string(@style), ':|\s')
                    let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
                        $mlnum := number( substring-before( $seq[$mlnum], 'cm') )
                    let $viewW := number( substring-before($width, 'cm') ) * 100

                    return
                        switch($pos)
                            case 'pos-right' return
                            <svg xmlns="http://www.w3.org/2000/svg" width="{$width}" height="{$lineHeight - 0.2}cm" viewBox="0 0 {$viewW} 100" preserveAspectRatio="none" style="left:{string(if($mlnum lt -0.19) then -0.2 else $mlnum)}cm;top: 3px;">
                                            <path d="
                                            M30,95 C30,95 5,100 5,60
                                            M 5,60 C 5,20 {$viewW div 2},10 {$viewW - 5},5
                                            " fill="none" stroke-width="2"/>
                            </svg>
                            case 'pos-left' return
                              <svg xmlns="http://www.w3.org/2000/svg"
                                   width="{$width}" height="{$lineHeight - 0.2}cm"
                                   viewBox="0 0 100 100"
                                   preserveAspectRatio="none"
                                   style="left:{$mlnum}cm;top: 7px;">
                                <path class="c-halfbow-posleft"
                                      fill="none" stroke-width="2"
                                      d="M 78.36,37.00
                                         C 96.91,33.55 108.45,20.36 86.00,10.45
                                           86.18,10.55 72.27,2.73 4.55,9.09" />
                              </svg>
                        default return <xhtml:div>{$pos} no example in Gesamtdoku</xhtml:div>
                case 'caret:semicircle' return
                    let $pos := substring-before(substring-after($node/@rend, '('), ')')
                    return
                        switch($pos)
                            case 'right' return
                                <svg xmlns="http://www.w3.org/2000/svg" width="20px" height="{$lineHeight}cm" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M75,5 C 75,5 145,100 5,90" fill="none" stroke-width="2"/>
                                </svg>
                            case 'left' return
                                <svg xmlns="http://www.w3.org/2000/svg" width="20px" height="{$lineHeight}cm" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M25,5 C 25,5 -45,100 95,90" fill="none" stroke-width="2"/>
                                </svg>
                            default return ()
                case 'caret:quartercircle' return
                    let $pos := substring-before(substring-after($node/@rend, '('), ')')

                    let $seq := tokenize($node/string(@style), ':|\s')
                    let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
                        $mlnum := number( substring-before( $seq[$mlnum], 'cm') )
                    let $width := if($mlnum lt 0) then $mlnum * -1 else $mlnum
                    let $width := if($width = 0) then 0.7 else $width
                    let $width := if(string($width) = "NaN" ) then 1.2 else $width
                    return
                        switch($pos)
                        case 'upper-right' return
                        <svg xmlns="http://www.w3.org/2000/svg" class="upper-right" width="{$width}cm" height="{$lineHeight - 0.2}cm" viewBox="0 0 100 50" preserveAspectRatio="none" data-pos="{$pos}" style="position: absolute;right: 0;top: 5px;">
                                    <path d="M0,10 C 0,10 95,-10 95,45" fill="none" stroke-width="2"/>
                                </svg>
                        case 'upper-left' return
                        <svg xmlns="http://www.w3.org/2000/svg" class="upper-left" width="1cm" height="{$lineHeight - 0.2}cm" viewBox="0 0 100 50" preserveAspectRatio="none" data-pos="{$pos}" style="position: absolute;right: -0.8cm;top: 0;">
                                    <path d="M3,95 C 3,95 0,35 60,20 M60,20 C60,20 90,15 100,20" fill="none" stroke-width="2"/>
                                </svg>
                        default return <xhtml:div>unknown parameter in function 'caret'</xhtml:div>
                case 'caret:3/4-circle' return '' (: class will be added in fontaneTransfo:add :)
                case 'caret:funnel' return ''
                case 'caret:looped_arc' return ''
                default return ()
return
$caret
};

declare function fontaneTransfo:lineHeight($n){
(: line-height it! :)
(: dont start calcualting in case of a figure :)
if ($n/ancestor::tei:zone[last()]/@type = "illustration") then 'line-height:100%;'
else
(: ok, we are not in a sktech :)
let $cntline :=
        count($n//tei:line) -
(:            count($n//tei:line[child::tei:fw][descendant::tei:*[not(2)]]) -:)
        count($n//tei:zone[@uly or @lry]//tei:line)
(:            -            count($n/tei:line[tei:metamark][not(text()[not(parent::tei:metamark)])]):)
let $surfaceHeight :=   if ($n/parent::tei:surface/parent::tei:sourceDoc)
                        then $n/preceding::tei:teiHeader//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:height[1]/@quantity div 10
                        else $n/parent::tei:surface/number(@lry) - $n/parent::tei:surface/number(@uly)
let $surfaceHeight := if($n/parent::tei:surface[@type = ("clipping", "additional")][parent::tei:sourceDoc]) then $n/parent::tei:surface/@lry - $n/parent::tei:surface/@uly else $surfaceHeight
return
if($cntline gt 1 and count(($n/ancestor::tei:surface)[1]//tei:zone) gt 0) then
    if ($n/@lry and not($n/@rotate)) then
        let $height := $n/@lry - (if($n/@uly) then $n/@uly else 0)
        return
            if (not($n/ancestor-or-self::*/@type = 'illustration') and $cntline != 0)
                then
                    'line-height:' || $height div $cntline
                    || 'cm;'
            else ()
    else
        (: only one zone and no @lry :)
        if (count($n/parent::tei:surface[parent::tei:sourceDoc]/tei:zone) = 1 and not($n/@lry) and $cntline gt 6 and not($n/@rotate))
        then 'line-height:' || ($surfaceHeight - (if($n/@uly) then $n/@uly else 0)) div $cntline ||'cm;'
    else
        if ($n/@uly and not($n/@lry) and not($n/@rotate))
        then 'line-height:' || ($surfaceHeight - $n/@uly) div $cntline ||'cm;'  (: height:'||$surfaceHeight||';' :)
    else
        if ($n/@rotate = '90')
        then
            'line-height:'|| ($n/@lry - $n/@uly)  div $cntline ||'cm;'
(:    else:)
(:        if ($n/@rotate = '180'):)
(:        then:)
(:            'line-height:'|| ($n/@uly - $n/@lry)  div $cntline ||'cm;':)
    else
        if ($n/@rotate = '270')
        then
            if($n[not(@uly)][not(@lry)]) then
                'line-height:' ||
                (number($n/preceding::tei:teiHeader//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity) div 10 - $n/@uly) div $cntline
                ||'cm;'
            else (: usual rotation with coordinates :)
               'line-height:' || (number($n/@lry) - number($n/@uly)) div $cntline ||'cm;'
    else if((number($n/@rotate) gt 270) and (number($n/@rotate) lt 360)) then
        if ($n/@uly and $n/@lry)
        then  'line-height:' || ($n/@lry - $n/@uly) div $cntline ||'cm;'
        else ()
    else
        if($n/parent::tei:surface[not(tei:zone[@rotate|@lrx|@lrx|@ulx|@uly])])
        (: https://projects.gwdg.de/issues/17466 :)
        then
            let $cntline := count($n/parent::tei:surface//tei:line)
            return
                'line-height:' || $surfaceHeight div $cntline ||'cm;'
    else
        if($n[not(@rotate)][not(@lrx)][not(@lrx)][not(@ulx)][not(@uly)] and ($surfaceHeight div $cntline lt 1) )
        then
            'line-height:' || $surfaceHeight div $cntline ||'cm;'
    else
        if(exists($n/@rotate) and exists($n/@uly) and exists($n/@lry)) then
            'line-height:' || ($n/@lry - $n/@uly) div $cntline ||'cm;'
    else ()
else
(:  if($cntline = 1 and count($n/parent::tei:surface//tei:zone) gt 1) :)
(:  https://fontane-nb.dariah.eu/test/edition.html?id=/xml/data/1zzdp.xml&page=inner_front_cover :)
    if($cntline = 1 and count($n/parent::tei:surface/tei:zone) gt 1)
    then
        if($n/parent::tei:surface//tei:zone[not(@rotate)][not(@lrx)][not(@lrx)][not(@ulx)][not(@uly)])
        (: https://projects.gwdg.de/issues/17466 , we need to test for more then one zone ! :)
        then
            let $cntline := count($n/parent::tei:surface//tei:line)
            return
                if($n/@rotate and $n/@uly and $n/@lry)
                then
                'line-height:'|| ($n/@lry - $n/@uly)  div $cntline ||'cm;'
                else
                'line-height:' || $surfaceHeight div $cntline ||'cm;'
        else ()
    else ()
};

declare function fontaneTransfo:addPreselection($node) {
    if($node/parent::tei:line or $node/parent::tei:seg) then
        element xhtml:span {attribute style {'position:relative'},
        (:  may we should add some space before and after some carets.
        (if $node//tei:add[])
        :)
        if($node[string(@place) = ('above', 'below')]/contains(@rend, 'caret')) then <xhtml:span class="smallspace"/> else (),
        fontaneTransfo:add($node),
        if($node[string(@place) = ('above', 'below')]/contains(@rend, 'caret')) then <xhtml:span class="smallspace"/> else ()
        }
    else
        (
        if($node[string(@place) = ('above', 'below')]/contains(@rend, 'caret')) then <xhtml:span class="smallspace"/> else (),
        fontaneTransfo:add($node),
        if($node[string(@place) = ('above', 'below')]/contains(@rend, 'caret')) then <xhtml:span class="smallspace"/> else ()
        )
(:            ,if($node/@prev and $node/ancestor::tei:TEI//tei:*[@xml:id = substring-after($node/@prev, '#')]/ancestor::tei:mod ) :)
(:            then fontaneTransfo:magic($node/ancestor::tei:TEI//tei:*[@xml:id = substring-after($node/@prev, '#')]/ancestor::tei:mod)//xhtml:div[contains(@class, 'addHover')] :)
(:            else () :)

};
declare function fontaneTransfo:add($node) {
let $style:=(
            if(contains($node/@rend, 'caret:V') and contains($node/@style, 'margin-left'))
            then 'left:'||number(analyze-string($node/@style, '-\d+\.\d')/fn:match/text()) + 0.2||'cm;'
            else
                if(contains($node/@style, 'margin-left'))
                then replace($node/@style, 'margin-left', 'left')
            else (),
            if(number(substring-before(tokenize(fontaneTransfo:lineHeight($node/ancestor::tei:zone[1]), ':')[2], 'cm')) lt 0.7) then ';top:-0.3cm;' else ()
            )
return
if ($node/@place = ('above', 'below')) then
    element xhtml:span {
        attribute class {'addWrapper'},
        element xhtml:span {
            attribute class {
                'add',
                $node/@place,
                if(contains($node/@rend, '3/4-circle')) then 'threequatercircle' else ()
            },
            if(not($style = () )) then
                attribute style {$style}
            else (),
        if($node/@rend="caret:looped_arc(pos-left)")
        then <xhtml:img src="/public/img/caret:looped-arc-pos-left.svg" width="140px" height="auto" style="left: -0.7cm; position: absolute;"/>
        else (),
        fontaneTransfo:magic($node/node()),
        if(not($node/ancestor::tei:mod) and not( $node/@copyOf ) ) then
            element xhtml:div {
                attribute class {'addHover italic'},
                'Hinzufügung', if(contains($node/@rend, 'caret')) then 'mit Einweisungszeichen' else ()
            }
        else (),
        if($node/@copyOf) then
            element xhtml:div {
                attribute class {'addHover italic'},
                'Wortwiederholung zur Verdeutlichung'
            }
        else ()
        },
        (if(starts-with(string($node/@rend), 'caret:'))then(fontaneTransfo:caret($node))else())
    }
else
    element xhtml:span {
        attribute class {
            'add',
            $node/@place,
            if($node/@place = "superimposed" and ($node/@next or $node/@prev)) then 'linked' else()
        },
        (
            if($node/parent::tei:mod)
            then
                if(local:modTest($node/parent::tei:mod) )
                then attribute style { 'position: relative;' }
                else ()
            else()
        ),
        fontaneTransfo:magic($node/node()),
        if($node/@place != ('above', 'below') and contains($node/@rend, 'caret')) then fontaneTransfo:caret($node) else (),
        if(not($node/ancestor::tei:mod)) then
            element xhtml:div {
                attribute class {'addHover italic'},
                'Hinzufügung', if(contains($node/@rend, 'caret')) then 'mit Einweisungszeichen' else ()
            }
        else ()
    }
};

declare function fontaneTransfo:gap($node) {
element xhtml:span {
            attribute class {'gap', $node/@reason, ($node/@unit || $node/@quantity (: classes must begin with a letter :)),
            if(($node/@reason = 'damage') and $node/@unit = 'mm') then attribute style {'width:'||$node/@quantity||$node/@unit} else(),
            (let $preMedium := $node/preceding::tei:handShift[@medium][1]/@medium
            return
                if(tokenize($preMedium, ' ') = ('blue_pencil', 'violet_pencil', 'black_ink', 'blue_ink', 'brown_ink')) then $preMedium else ())
            }
        }
};

declare function fontaneTransfo:metamark($node) {(
element xhtml:span {
    attribute class {
        'metamark',
        if(contains($node/@rend, 'bracket_left') and $node/parent::tei:zone/*[1] = $node/parent::tei:zone/*[last()])
            then 'cm' || floor($node/parent::tei:zone/@lry - $node/parent::tei:zone/@uly)
            else (),
        replace(string($node/@function), 'caret', 'teicaret'),
        if($node/@function = "deletion" and $node/parent::tei:zone/@points)
            then "no-border"
            else(),
        if($node/text() = "╒" and $node/ancestor::tei:seg[contains(@style, "vertical-align:super")]) then ("f") else (),
        $node//descendant-or-self::tei:*[@rend]/replace(string(@rend),":"," ")
    },
    fontaneTransfo:magic($node/node()),
    if($node/@rend = "slash") then "/" else (),
    if(contains($node/@rend, 'bracket_left'))
        then
            let $num :=  $node/parent::tei:zone/@lry - $node/parent::tei:zone/@uly
            return
        (
            if($node/parent::tei:zone/*[1] = $node/parent::tei:zone/*[last()])
                then
                        attribute style { 'font-size:' || $num ||'cm;' }
                else (),
            <xhtml:span class="bracket_left" style="transform: scaleX({1 div ceiling($num)});">&#123;</xhtml:span>
        )
        else (),
    if(contains($node/@rend, 'bracket_right'))
        then
            <xhtml:span class="bracket_right">{
                if($node/parent::tei:zone/*[1] = $node/parent::tei:zone/*[last()])
                then
                    let $num :=  $node/parent::tei:zone/@lry - $node/parent::tei:zone/@uly
                    return (
                        attribute style {
                            'font-size:' || $num ||'cm;',
                            if($num gt 3) then 'display:block; transform:scaleX('|| 1 div $num ||');' else ()
                        }
                    )
                else ()
            }&#125;</xhtml:span>
        else (),
    if(matches($node, '#|╒|/|\*')) then
        <xhtml:div class="metamarkHover"> Einweisungszeichen </xhtml:div>
    else (),
    if ($node/@function="etc.") then
        <xhtml:div class="metamarkHover"> und so weiter </xhtml:div>
    else (),
    if ($node/@function="integrate") then
        <xhtml:div class="metamarkHover"> Klammer zusammenfassende Funktion </xhtml:div>
    else (),
    (if($node/@function = "caret" and $node/@rend ) then
        (<xhtml:span class="smallspace"/>,
        fontaneTransfo:caret($node),
        <xhtml:span class="smallspace"/>)
        else()
)},
if(contains($node/@rend, 'bow') and contains($node/@rend, 'pos-right')) then
        <svg xmlns="http://www.w3.org/2000/svg" width="{tokenize($node/@rend, '\(|,')[2]}" height="auto" viewBox="0 20 560 125">
        <path id="Unnamed" fill="none" stroke-width="1.5" d="M 529.00,8.00 C 529.00,8.00 603.00,25.50 507.50,60.50
                                                            507.50,60.50 394.00,101.50 24.50,93.50
                                                            24.50,93.50 9.06,96.85 4.50,103.00
                                                            3.62,104.19 4.50,120.50 4.50,120.50"/>
        </svg>
    else ()
    ),
if($node/@function = "deletion" and $node/parent::tei:zone/@points) then
  <svg xmlns="http://www.w3.org/2000/svg" class="deletion-points" width="100%" height="100%">
      <g transform="scale(35.43307)">
        <polyline fill="none" stroke="darkslategrey" stroke-width="0.04px" points="{$node/parent::tei:zone/@points}"/>
      </g>
    </svg>
    else ()
};

declare function fontaneTransfo:postZone($n) {
if (contains($n/@rend, 'line-through-style:triple_oblique'))
then for $i in (1 to 3) return element svg {
    namespace svg { "http://www.w3.org/2000/svg" },
    attribute class { "tripleoblique"},
    attribute viewbox { "0 0 100 100"},
    element line {
        attribute x1 {"0"},
        attribute y1 {"0"},
        attribute x2 {"100"},
        attribute y2 {"100"},
        attribute style {"stroke-width:2"}
    }
}
else ()
};

(: prerendered toc :)
declare function fontaneTransfo:toc($doc) {
element xhtml:div {
    attribute id {'toc'},
(:
 :  we dont want any output styles in here!
 :     attribute style {'display:none;'},:)
    $doc/tei:fileDesc/tei:titleStmt/tei:title/string(.),
    <xhtml:ol>
        {for $surface in $doc/tei:sourceDoc/tei:surface
        let $vakat :=   if(sum(
                                $surface//text()[preceding::tei:handShift[@new][1]/@new
                                = ('#Fontane', '#Emilie_Fontane', '#Friedrich_Fontane' )]
                                /string-length(replace(. , '\s', ''))
                            ) lt 1)
                        then
                            if(not( $surface//tei:figure[preceding::tei:handShift[@new][1]/@new='#Fontane'])) then 'vakat'
                            else ()
                        else (),
            $fragment :=    if($surface/@type="fragment")
                            then ('Fragment',
                                    if($surface/@attachment)
                                    then
                                        switch($surface/@attachment)
                                        case 'cut' return 'Wegschnitt'
                                        case 'torn' return 'Wegriss'
                                        case 'cut and torn' return 'Wegschnitt und Wegriss'
                                        default return $surface/@attachment
                                    else ())
                            else (),
            $info := string-join(($fragment, $vakat), ', '),
            $childs := for $child in $surface/tei:surface[@type= ('clipping', 'additional')]
                        return
                            <xhtml:li>
                                <xhtml:a href="?id=/xml/data/{substring-after( $doc//tei:teiHeader//tei:publicationStmt/tei:ab//tei:idno[@type="TextGrid"]/text(), 'textgrid:' )}.xml&amp;page={$child/@n}">
                                    {$child/string(@n)} {$child/string(@subtype)}
                                </xhtml:a>
                            </xhtml:li>
        return
            <xhtml:li><xhtml:a href="?id=/xml/data/{substring-after( $doc//tei:teiHeader//tei:publicationStmt/tei:ab//tei:idno[@type="TextGrid"]/text(), 'textgrid:' )}.xml&amp;page={$surface/@n}">{
        if(contains($surface/string(@n), 'endpaper')) then
            let $seq := tokenize($surface/string(@n), '_')
            return
                ((switch($seq[1])
                    case "front" return 'vorderes'
                    case "back" return 'hinteres'
                    default return 'ungültiger Wert in @n')
                || ' Vorsatzblatt ' || $seq[last()])
        else
        switch ($surface/string(@n))
           case "outer_front_cover" return "vordere Einbanddecke außen"
           case "inner_front_cover" return "vordere Einbanddecke innen"
           case "inner_back_cover" return "hintere Einbanddecke innen"
           case "outer_back_cover" return "hintere Einbanddecke außen"
           case "spine" return "Buchrücken"
           default return $surface/string(@n)
            } {if($info = '') then () else
                ' ('||$info||')'}
            </xhtml:a>
                {if($childs = ()) then () else <xhtml:ul> {$childs} </xhtml:ul>}
            </xhtml:li>
        }
    </xhtml:ol>
}
};

declare function local:modTest($mod){
let $delStringLength:=
    if($mod/tei:del//tei:gap)
        then
            sum((
                ($mod/tei:del//tei:gap[@unit="lc_chars"]/@quantity),
                ($mod/tei:del//tei:gap[@unit="uc_chars"]/@quantity),
                ($mod/tei:del//tei:gap[@unit="cap_words"]/(number(@quantity) * 5)),
                ($mod/tei:del//tei:gap[@unit="uncap_words"]/(number(@quantity) * 5))
            )) + string-length(replace(string-join($mod/tei:del//text() ), '\s', '' ) )
        else string-length(replace(string-join($mod/tei:del//text() ), '\s', '' ) )

let $addStringLength :=
    if($mod/tei:add//tei:gap)
        then
            sum((
                ($mod/tei:add//tei:gap[@unit="lc_chars"]/@quantity),
                ($mod/tei:add//tei:gap[@unit="uc_chars"]/@quantity),
                ($mod/tei:add//tei:gap[@unit="cap_words"]/(number(@quantity) * 5)),
                ($mod/tei:add//tei:gap[@unit="uncap_words"]/(number(@quantity) * 5))
            )) + string-length(replace(string-join($mod/tei:del//text() ), '\s', '' ) )
        else string-length( replace(string-join($mod//tei:add[not(@place = 'above' or @place="below")]//text()), '\s', '') )
let $pattern := 'ſ|i|j|l|t|I|,'
return
(
    $delStringLength
    lt
    $addStringLength
)
or
(
    (
        string-length(string-join($mod/tei:add//text() ))
        =
        string-length(string-join($mod/tei:del/text() ))
    )
    and
        count(tokenize(string-join($mod/tei:add//text()), $pattern))
        lt
        count(tokenize(string-join( $mod/tei:del/text() ), $pattern))
)
};

declare function fontaneTransfo:newestTBLELink($uri as xs:string, $id as xs:string) as node() {
let $links := collection('/db/sade-projects/textgrid/data/xml/tile')
            //tei:link
            [matches(@targets, "#shape.*" || $uri || "\.\d+#"|| $id || "$")]

let $maxBase :=  max( $links ! ./base-uri() )

(:let $test := console:log( $uri ):)
(:let $test := console:log( $id ):)
(:let $test := console:log( $links ):)
(:let $test := console:log( (for $l in $links return string($l/base-uri())) ):)
(:let $test := console:log( $maxBase ):)

return
    if( count( $links ) = 1 )
    then
        $links
    else
        let $links := $links[base-uri() = $maxBase]
        return
            if(count($links) = 1)
            then $links
            else $links[last()]
            (: if there are more than one links to a single xml:id in a single TILE object, take the last one :)
};

declare function fontaneTransfo:getMedium($node) as xs:string {
    string($node/preceding::handShift[@medium][1]/@medium)
};

declare function fontaneTransfo:noteParser($note as node()*) {
    for $node in $note return
        typeswitch ( $node )
        case element (tei:note) return
            (
            string-join(for $xmlid in tokenize(replace($node/@target, "#", ""), " ")
            return
                $node/ancestor::tei:TEI//@xml:id[. = $xmlid]/parent::*//text()
            , " ") || "]"
            ,
            element xhtml:ul {
                fontaneTransfo:noteParser($node/node())
            })
        case element( tei:bibl ) return
            element xhtml:a {
                attribute href {"literaturvz.html?id="||$node/tei:ptr/substring-after(@target, ":")},
                //@xml:id[.=$node/tei:ptr/substring-after(@target, ":")]/parent::tei:bibl//tei:abbr/text()
            }
        case text() return $node
        default return
            fontaneTransfo:noteParser($node/node())
};
