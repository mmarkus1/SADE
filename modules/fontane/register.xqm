xquery version "3.1";

module namespace fontaneregister="http://fontane-nb.dariah.eu/register";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ore="http://www.openarchives.org/ore/terms/";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";

declare function local:Links($link as element()) {
    let $targetSf := substring-before(substring-after($link/string(@target), "surface[@n='"), "']")
    let $baseUri := substring(substring-after($link/@target, 'textgrid:'), 1, 5)
    let $title := doc( '/db/sade-projects/textgrid/data/xml/meta/' || $baseUri || '.xml')//tgmd:title/text()
    let $nb := request:get-parameter('nb', 'textgrid')
    return
        if(contains($link/@target, $nb)) then
        <li><a href="edition.html?id=/xml/data/{$baseUri}.xml&amp;page={$targetSf}">{$title} – {$targetSf}</a></li>
        else ()
};

declare function local:Links2($link as element()) {
    let $targetSf := substring-before(substring-after($link/string(@target), "surface[@n='"), "']")
    let $baseUri := substring(substring-after($link/@target, 'textgrid:'), 1, 5)
    let $title := doc( '/db/sade-projects/textgrid/data/xml/meta/' || $baseUri || '.xml')//tgmd:title/text()
    let $nb := request:get-parameter('nb', 'textgrid')
    return
        if(contains($link/@target, $nb)) then
        <a href="edition.html?id=/xml/data/{$baseUri}.xml&amp;page={$targetSf}">{$targetSf}</a>
        else ()
};

declare function local:pointer($pointer, $targetNode) {
(:<ptr target="#Tizian"/>:)
<li> siehe <a href="register.html?{replace(request:get-query-string(), '(&amp;)*e=.+(&amp;)*', '')||(if(request:get-query-string() = "") then () else "&amp;")}e={$pointer/substring-after(@target, '#')}">{ ($targetNode//tei:persName)[1]/text() }</a> </li>
};

declare function fontaneregister:events($eventDoc) {
let $eventURI := tokenize($eventDoc/base-uri(), '/|\.')[8]
return
(: Ereignisregister :)
    <div id="r{ $eventURI }" class="tab-pane {if( request:get-parameter('r', '') = $eventURI ) then "active" else ()}">
        <div id="accordion{ $eventURI }" class="panel-group" aria-multiselectable="true" role="tablist">
        {for $eventList in $eventDoc/tei:listEvent
        return
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">{$eventList/tei:head/text()}</a>
                    </h4>
                </div>
                <div class="panel-body">
                <ul>
                    { for $event in $eventList/tei:event
                    return
                        <li id="{ $event/string(@xml:id) }" class="registerEintrag {if($event/@xml:id = request:get-parameter('e', '')) then 'fhighlighted' else ()}">
                        {for $label at $pos in $event/tei:label/text()
                        return ($label, if($pos = 1) then
                            <span class="registerLink"> <a href="register.html?{replace(request:get-query-string(), '(&amp;)*e=.+(&amp;)*', '')||(if(request:get-query-string() = "") then () else "&amp;")}e={$event/string(@xml:id)}"><i title="Link zu diesem Eintrag" class="fa fa-link"></i></a> </span>
                            else (), <br/>)}
                            <ul>
                                {if( not( exists($event/@xml:id)) ) then local:pointer($event//tei:ptr, $eventList/tei:event[@xml:id = substring-after($event//tei:ptr/@target, '#')]) else
                                if (not($event//tei:link/contains(@target, request:get-parameter('nb', 'textgrid')) = true() )) then ()
                                else
                                if($event/tei:note/text()) then <li>{$event/tei:note/text()}</li> else ()}
                                <li>{if(count($event//tei:link) lt 2) then 'In Notizbuch' else 'In den Notizbüchern'}
                                    <ul>
                                        {for $link in $event//tei:link[@corresp="https://schema.org/mentions"]
                                        return local:Links($link)}
                                    </ul>
                                </li>

                            </ul>
                        </li>
                    }
                </ul>
                </div>
            </div>
        }
        </div>
    </div>
};

declare function fontaneregister:works($wrkDoc) {
let $r := request:get-parameter('r', '')
let $e := request:get-parameter('e', '')
let $wrkURI := tokenize($wrkDoc/base-uri(), '/|\.')[8]
return
(: Werkregister :)
            <div id="r{ $wrkURI }-{$wrkDoc/string(@type)}" class="tab-pane {
                        if(($r = "r253t3-"||string($wrkDoc/@type)) or ($e = $wrkDoc//*[@xml:id]/string(@xml:id)))
                        then 'active'
                        else ()
                    }">
                <div id="accordion{ $wrkURI }" class="panel-group" aria-multiselectable="true" role="tablist">
                {for $wrkList in $wrkDoc
                return
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">{$wrkList/tei:head/text()}</a>
                            </h4>
                        </div>
                        <div class="panel-body">
                            <ul>
                                {for $wrk in $wrkList//tei:item[not(tei:list)]
                                let $nichtermittelt := if($wrk//tei:note/string(.) = "nicht ermittelt") then "(nicht ermittelt)" else ()
                                let $creator := $wrk//tei:link[ends-with(@corresp, "creator")]/substring-after(@target, 'psn:')
                                let $creator := (collection('/db/sade-projects/textgrid/data/xml/data')//tei:person[@xml:id=$creator]/tei:persName/text())[1]
                                let $location := $wrk//tei:link[ends-with(@corresp, "location")]/substring-after(@target, 'plc:')
                                let $klammer := ( $nichtermittelt, $creator, if($creator != "") then () else $location )
                                order by lower-case(($wrk/tei:name[not(@type)]/text())[1])
                                return
                                    if( not( exists($wrk/@xml:id)) ) then local:pointer($wrk//tei:ptr, $wrkList/tei:event[@xml:id = substring-after($wrk//tei:ptr/@target, '#')]) else
                                    if (not($wrk//tei:link/contains(@target, request:get-parameter('nb', 'textgrid')) = true() )) then ()
                                    else
                                    <li class="registerEintrag {if($wrk/@xml:id = request:get-parameter('e', '')) then 'fhighlighted' else ()}">
                                        {$wrk/tei:name[not(@type="variant")]/text()}
                                        {if(count($klammer) gt 0) then " ("|| string-join( $klammer[. != ""] ,';') ||")" else ()}
                                        <span class="registerLink"> <a href="register.html?{replace(request:get-query-string(), '(&amp;)*e=.+(&amp;)*', '')||(if(request:get-query-string() = "") then () else "&amp;")}e={$wrk/string(@xml:id)}"><i title="Link zu diesem Eintrag" class="fa fa-link"></i></a> </span>
                                        {if($wrk/tei:name[@type]) then
                                            <ul>
                                            <li> Im Text: {string-join( (for $i in $wrk/tei:name[@type] return $i/text()), '; ')}
                                            </li></ul> else ()}
                                            <ul><li>{if(count( distinct-values($wrk//tei:link[@corresp="https://schema.org/mentions"]/substring(substring-after(@target, 'textgrid:'), 1, 5)) ) lt 2) then 'In Notizbuch' else 'In den Notizbüchern'}
                                                <ul>
                                                  <!--  {
                                                    for $link in distinct-values($wrk//tei:link[@corresp="https://schema.org/mentions"]/)
                                                    return local:Links($link)} -->
                                                    {if($wrk//tei:linkGrp) then local:linkGroup(($wrk//tei:linkGrp)[1]) else ()}
                                                </ul>
                                            </li></ul>
                                    </li>
                                }
                            </ul>
                        </div>
                    </div>
                }
                </div>
            </div>
};

declare function local:linkGroup($linkGrp as element()) {
let $nbs := distinct-values(
    $linkGrp//tei:link[@corresp="https://schema.org/mentions"][not(contains(@target, "XXX"))]/substring(substring-after(@target, 'textgrid:'), 1, 5) )

return
    for $nb in $nbs
    let $title := doc( '/db/sade-projects/textgrid/data/xml/meta/' || $nb || '.xml')//tgmd:title/text()
    let $links := for $link in $linkGrp//tei:link[contains(@target, $nb)] return local:Links2($link)
    let $cntLinks := count($links)
    return
        <li onclick="javascript:;"> {$title}: {for $l at $pos in $links where $pos lt $cntLinks return ($l, ', '), $links[last()] } </li>


};

declare function local:pointerTest($ref, $nb) as xs:boolean {
    let $id := substring-after($ref, "#")
    let $target := string-join( collection("/db/sade-projects/textgrid/data/xml/data/")//*[@xml:id = $id]//tei:link/@target )
    return
        contains($target, $nb)
};

declare function fontaneregister:persons( $persDoc ) {
let $persURI := tokenize($persDoc/base-uri(), '/|\.')[8]
return
(: Personenregister :)
    <div id="r{ $persURI }" class="tab-pane {if( request:get-parameter('r', '') = $persURI or ($persDoc//@xml:id = request:get-parameter('e', '')) ) then "active" else ()}">
        <div id="accordion{ $persURI }" class="panel-group" aria-multiselectable="true" role="tablist">
        {for $persList in $persDoc
        return
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">{$persList/tei:head/text()}</a>
                    </h4>
                </div>
                <div class="panel-body">
                <ul>
                    { for $pers in $persList/tei:person|$persList/tei:personGrp
                    return

                        (: test if we process for a specific notebook :)
                    (:    if (not(
                                ($pers//tei:link/contains(@target, request:get-parameter('nb', 'textgrid')) = true() )
                            )
                        ) then () else
                            if(not(local:pointerTest(string(string($pers//tei:note/tei:ptr/@target)), request:get-parameter('nb', 'textgrid')))) then () else
                    :)    <li id="{$pers/@xml:id}" class="registerEintrag reg-{$pers/local-name()} {if($pers/@xml:id = request:get-parameter('e', '')) then 'fhighlighted' else ()}">
                            {$pers/tei:persName[1]/text()}
                            {
                                if( not( exists($pers/@xml:id)) ) then local:pointer($pers//tei:ptr, $persList/ancestor::*//*[@xml:id = substring-after($pers//tei:ptr/@target, '#')])
                                else (

                            <span class="registerLink"> <a href="register.html?{replace(request:get-query-string(), '(&amp;)*e=.+(&amp;)*', '')||(if(request:get-query-string() = "") then () else "&amp;")}e={$pers/string(@xml:id)}"><i title="Link zu diesem Eintrag" class="fa fa-link"></i></a> </span>,
                            <ul>
                                {if(exists($pers/tei:persName[@type="variant"]))
                                    then <li>im Text:
                                            <ul>{ for $item in $pers/tei:persName[@type="variant"]/text() return <li> {$item} </li>} </ul>
                                        </li> else ()}
                                {if($pers/tei:birth or $pers/tei:death)
                                then <li>{if($pers/tei:birth) then ('&#8727;',$pers/tei:birth/text()) else ()}{' '}{if($pers/tei:death) then ('✝',$pers/tei:death/text()) else()}</li>
                                else ()}
                                {if($pers/tei:occupation) then <li>{$pers/tei:occupation/text()}</li> else ()}
                                <li>(<a href="http://d-nb.info/gnd/{$pers/tei:idno[@xml:base="http://d-nb.info/gnd/"]/text()}">GND</a>)</li>
                                <li>
                                    <ul>
                                        {if($pers/tei:note/text()) then <li>{$pers/tei:note/text()}</li> else ()}
                                        <li>{if(count( distinct-values($pers//tei:link/substring(substring-after(@target, 'textgrid:'), 1, 5)) ) lt 2) then 'In Notizbuch' else 'In den Notizbüchern'}
                                            <ul>
                                                {for $link in $pers//tei:link[@corresp="https://schema.org/mentions"]
                                                    return local:Links($link)}
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                                )}
                        </li>
                    }
                </ul>
                </div>
            </div>
        }
        </div>
    </div>
};

declare function fontaneregister:places($geoDoc) {
let $geoURI := tokenize($geoDoc/base-uri(), '/|\.')[8]
return
(: Ortsregister :)
    <div id="r{ $geoURI }" class="tab-pane {if( request:get-parameter('r', '') = $geoURI ) then "active" else ()}">
        <div id="accordion{ $geoURI }" class="panel-group" aria-multiselectable="true" role="tablist">
        {for $geoList in $geoDoc
        return
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">{$geoList/tei:head/text()}</a>
                    </h4>
                </div>
                <div class="panel-body">
                <ul>
                    { for $geo in $geoList/tei:place
                    return
                        if( not( exists($geo/@xml:id)) ) then local:pointer($geo//tei:ptr, $geoList/tei:event[@xml:id = substring-after($geo//tei:ptr/@target, '#')]) else
                        if (not($geo//tei:link/contains(@target, request:get-parameter('nb', 'textgrid')) = true() )) then ()
                        else
                        <li class="registerEintrag reg-{$geo/local-name()} {if($geo/@xml:id = request:get-parameter('e', '')) then 'fhighlighted' else ()}">
                        {for $label at $pos in $geo/tei:placeName/text()
                            return ($label, if($pos = 1) then
                                <span class="registerLink"> <a href="register.html?{replace(request:get-query-string(), '(&amp;)*e=.+(&amp;)*', '')||(if(request:get-query-string() = "") then () else "&amp;")}e={$geo/string(@xml:id)}"><i title="Link zu diesem Eintrag" class="fa fa-link"></i></a> </span>
                                else (),<br/>)}
                        {if(exists($geo/tei:persName[@type="variant"])) then (' ',<small>({string-join( $geo/tei:persName[@type="variant"]/text(), '|' )})</small>) else ()}
                            <ul>
                                {for $idno in $geo/tei:idno return <li class="reg-idno">(<a href="{$idno/@xml:base || $idno/string(.)}">{string($idno/@type)}</a>)</li> }
                                {if($geo/tei:note/text()) then <li>{$geo/tei:note/text()}</li> else ()}
                                <li>{if(count($geo//tei:link) lt 2) then 'In Notizbuch' else 'In den Notizbüchern'}
                                    <ul>
                                        {for $link in $geo//tei:link[@corresp="https://schema.org/mentions"]
                                        return local:Links($link)}
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    }
                </ul>
                </div>
            </div>
        }
        </div>
    </div>
};

declare function fontaneregister:orgs($orgDoc) {
let $orgURI := tokenize($orgDoc/base-uri(), '/|\.')[8]
return
(: Koerperschaftenregister :)
    <div id="r{ $orgURI }" class="tab-pane {if( request:get-parameter('r', '') = $orgURI ) then "active" else ()}">
        <div id="accordion{ $orgURI }" class="panel-group" aria-multiselectable="true" role="tablist">
        {for $orgList in $orgDoc
        return
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">{$orgList/tei:head/text()}</a>
                    </h4>
                </div>
                <div class="panel-body">
                <ul>
                    { for $org in $orgList/tei:org
                    return
                        if( not( exists($org/@xml:id)) ) then local:pointer($org//tei:ptr, $orgList/tei:event[@xml:id = substring-after($org//tei:ptr/@target, '#')]) else
                        if (not($org//tei:link/contains(@target, request:get-parameter('nb', 'textgrid')) = true() )) then ()
                        else
                        <li class="registerEintrag {if($org/@xml:id = request:get-parameter('e', '')) then 'fhighlighted' else ()}">
                        {for $label at $pos in $org/tei:orgName[not(@type="variant")]/text()
                        return ($label, if($pos = 1) then
                            <span class="registerLink"> <a href="register.html?{replace(request:get-query-string(), '(&amp;)*e=.+(&amp;)*', '')||(if(request:get-query-string() = "") then () else "&amp;")}e={$org/string(@xml:id)}"><i title="Link zu diesem Eintrag" class="fa fa-link"></i></a> </span>
                            else (), <br/>)}
                            <ul>
                                {if($org/tei:note/text()) then <li>{$org/tei:note/text()}</li> else ()}
                                <li>{if(count($org//tei:link) lt 2) then 'In Notizbuch' else 'In den Notizbüchern'}
                                    <ul>
                                        {for $link in $org//tei:link[@corresp="https://schema.org/mentions"]
                                        return local:Links($link)}
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    }
                </ul>
                </div>
            </div>
        }
        </div>
    </div>

};

declare function fontaneregister:register($id){
let $r := request:get-parameter('r', '')
let $e := request:get-parameter('e', '')
let $agg := '253st.xml'
let $uris := doc('/db/sade-projects/textgrid/data/xml/agg/'||$agg)//ore:aggregates/substring-after(@rdf:resource, 'textgrid:')
let $meta := for $uri in $uris
                    where doc-available( '/db/sade-projects/textgrid/data/xml/data/' || $uri ||'.xml')
                    return
                        doc('/db/sade-projects/textgrid/data/xml/meta/'||$uri||'.xml')
let $docs := $meta//tgmd:textgridUri/tokenize(., ':|\.')[2] ! doc('/db/sade-projects/textgrid/data/xml/data/' || . ||'.xml')

let $tabs := <ul class="nav nav-tabs" style="font-size: smaller;">{
                    for $i at $pos in $meta
                    let $uri := $i//tgmd:textgridUri/tokenize(., ':|\.')[2]
                    where not(matches($uri,"25547|253t3"))
                    return
                        <li>
                            { if(($r = $uri) or $e = doc('/db/sade-projects/textgrid/data/xml/data/' || $uri ||'.xml')//*[@xml:id]/string(@xml:id)) then attribute class {'active'} else () }
                            <a data-toggle="tab" href="#r{ $uri }">{$i//tgmd:title/text()}</a></li>,
                        (: need to hardcode stuff comming from a singe tei file: :)
                        <li>
                            { if(($r = "r253t3-works") or ($e = doc('/db/sade-projects/textgrid/data/xml/data/253t3.xml')
                                    //tei:body//tei:list[@type="works"]
                                        //*[@xml:id]/string(@xml:id))) then attribute class {'active'} else ()
                            }
                            <a data-toggle="tab" href="#r253t3-works">Werke</a></li>,
                        <li>
                            { if(($r = "r253t3-Fontane") or ($e = doc('/db/sade-projects/textgrid/data/xml/data/253t3.xml')
                                    //tei:body//tei:list[@type="Fontane"]
                                        //*[@xml:id]/string(@xml:id))) then attribute class {'active'} else ()
                            }
                            <a data-toggle="tab" href="#r253t3-Fontane">Fontanes Werke</a></li>,
                        <li>
                            { if(($r = "r253t3-periodicals") or ($e = doc('/db/sade-projects/textgrid/data/xml/data/253t3.xml')
                                    //tei:body//tei:list[@type="periodicals"]
                                        //*[@xml:id]/string(@xml:id))) then attribute class {'active'} else ()
                            }
                            <a data-toggle="tab" href="#r253t3-periodicals">Periodika</a></li>
            }</ul>


return
    <div class="container">
        {$tabs}
        <div class="tab-content">
            { fontaneregister:events( $docs//tei:body/tei:listEvent ) }
            { fontaneregister:orgs( $docs//tei:body/tei:listOrg ) }
            { fontaneregister:places( $docs//tei:body/tei:listPlace ) }
            { fontaneregister:persons( $docs//tei:body/tei:listPerson ) }
            { fontaneregister:works( $docs//tei:body//tei:list[@type="works"] ) }
            { fontaneregister:works( $docs//tei:body//tei:list[@type="periodicals"] ) }
            { fontaneregister:works( $docs//tei:body//tei:list[@type="Fontane"] ) }
        </div>
    </div>
};
