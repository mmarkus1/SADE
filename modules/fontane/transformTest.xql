xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo" at "/db/apps/SADE/modules/multiviewer/fontane.xqm";

let $uri := "1zzdp"
let $header := doc( "/db/sade-projects/textgrid/data/xml/data/" || $uri || ".xml" )//tei:teiHeader

let $tei :=
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
        {$header}
        <sourceDoc n="Nachl._Theodor_Fontane,Notizbücher_C05">
            <surface n="outer_front_cover" facs="http://textgridrep.org/textgrid:18wz6">
            <graphic n="C05_001" url="http://textgridrep.org/textgrid:18wz6" mimeType="image/jpeg"/>
            <surface facs="http://textgridrep.org/textgrid:18wz6" type="label" subtype="Signaturen-Klebchen_Archivar_alt" ulx="0.0" uly="2.7" lrx="1.6" lry="6.5">
            <graphic n="C05_001" url="http://textgridrep.org/textgrid:18wz6" mimeType="image/jpeg"/>
                <zone uly="0.3" lry="1.2">
                    <handShift new="#fremde_Hand1" medium="black_ink thin_pen" script="Latn clean"/>
                    <line style="margin-left:0.8cm">C</line>
                    <line style="margin-left:0.8cm">5</line>
                    <handShift new="#Fontane" medium="pencil" script="Latf standard"/>
                </zone>

            </surface>
            <surface facs="http://textgridrep.org/textgrid:18wz6" type="label" subtype="Etikett_vom_Notizbuch-Hersteller_angefertigt" ulx="1.7" uly="4.1" lrx="8.6" lry="8.5">
            <graphic n="C05_001" url="http://textgridrep.org/textgrid:18wz6" mimeType="image/jpeg"/>
                <zone>
                    <handShift new="#fremde_Hand3"/>
                    <line style="margin-left:0.5cm" rend="align(center)">✓  <handShift new="#Fontane" medium="black_ink thick_pen" script="clean"/><seg type="heading"><seg style="letter-spacing:0.2cm; font-size:xx-large; font-weight:bold"><seg><date type="synchronous" when-iso="1873"><hi>1<seg style="text-decoration:underline" rend="underline-style:slightly_wavy">873</seg></hi></date></seg>.</seg></seg></line>
                    <handShift script="standard"/>
                    <zone type="toc" subtype="Fontane">
                        <zone type="item" rend="align(center)">
                            <line style="margin-left:0.3cm">(<seg style="letter-spacing:0.2cm"><seg><rs type="direct" ref="plc:Thueringen_Land"><handShift script="Latn"/>Thüringen</rs></seg><handShift script="Latf"/>.</seg> Auf⸗</line>
                    <lb type="edited_text"/>
                            <line style="margin-left:0.8cm">enthalt in <seg><rs type="direct" ref="plc:Tabarz"><handShift script="Latn"/><seg><ref target="#xpath(//surface[@n='38r'])">Tabarz</ref></seg></rs></seg><handShift script="Latf"/>.)</line>
                   <lb type="edited_text"/>
                            <line style="margin-left:0.8cm"><seg><rs type="direct" ref="plc:Gotha"><seg><ref target="#xpath(//surface[@n='1r'])">Gotha</ref></seg></rs></seg>. <seg><rs type="direct" ref="plc:Eisenach"><seg><ref target="#xpath(//surface[@n='11r'])">Eiſenach</ref></seg></rs></seg>.<seg><rs type="direct" ref="plc:Coburg"><seg><ref target="#xpath(//surface[@n='60r'])">Coburg</ref></seg></rs></seg>.</line>
                    <lb type="edited_text"/>
                            <line style="margin-left:0.5cm"><seg><rs type="direct" ref="plc:Neuses">Neuſeß</rs></seg>. <!--Verlinkung?--><seg><rs type="direct" ref="plc:Schmalkalden"><seg><ref target="#xpath(//surface[@n='41r'])">Schmalkalden</ref></seg></rs></seg></line>
                    <lb type="edited_text"/>
                            <handShift script="Latn"/>
                    <line style="margin-left:2.0cm">etc. etc.</line>
                    <handShift medium="pencil" script="Latf"/>
                </zone>
                    </zone>
                </zone>

            </surface>

            </surface>
        </sourceDoc>
        </TEI>
return
    fontaneTransfo:magic($tei)
