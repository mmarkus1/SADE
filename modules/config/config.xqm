xquery version "3.1";

(:~
 : A set of helper functions to access the application context from
 : within a module.
 :)
module namespace config="http://textgrid.de/ns/SADE/config";

import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace config-params="http://exist-db.org/xquery/apps/config-params" at "config.xql";

declare namespace templates="http://exist-db.org/xquery/templates";
declare namespace repo="http://exist-db.org/xquery/repo";
declare namespace expkg="http://expath.org/ns/pkg";

(:
    Determine the application root collection from the current module load path.
:)
declare variable $config:app-root :=
    let $rawPath := system:get-module-load-path()
    let $modulePath :=
        (: strip the xmldb: part :)
        if (starts-with($rawPath, "xmldb:exist://")) then
            if (starts-with($rawPath, "xmldb:exist://embedded-eXist-server")) then
                substring($rawPath, 36)
            else
                substring($rawPath, 15)
        else
            $rawPath
    return
        substring-before($modulePath, "/modules")
;

declare variable $config:data-root := $config:app-root || "/data";

declare variable $config:repo-descriptor := doc(concat($config:app-root, "/repo.xml"))/repo:meta;

declare variable $config:expath-descriptor := doc(concat($config:app-root, "/expath-pkg.xml"))/expkg:package;

declare variable $config:projects-dir := $config-params:projects-dir;
(:~
 : Resolve the given path using the current application context.
 : If the app resides in the file system,
 :)
declare function config:resolve($relPath as xs:string) {
    if (starts-with($config:app-root, "/db")) then
        doc(concat($config:app-root, "/", $relPath))
    else
        doc(concat("file://", $config:app-root, "/", $relPath))
};

(:~
 : Returns the repo.xml descriptor for the current application.
 :)
declare function config:repo-descriptor() as element(repo:meta) {
    $config:repo-descriptor
};

(:~
 : Returns the expath-pkg.xml descriptor for the current application.
 :)
declare function config:expath-descriptor() as element(expkg:package) {
    $config:expath-descriptor
};

declare %templates:wrap function config:app-title($node as node(), $model as map(*)) as text() {
    $config:expath-descriptor/expkg:title/text()
};

declare function config:app-meta($node as node(), $model as map(*)) as element()* {
    <meta xmlns="http://www.w3.org/1999/xhtml" name="description" content="{$config:repo-descriptor/repo:description/text()}"/>,
    for $author in $config:repo-descriptor/repo:author
    return
        <meta xmlns="http://www.w3.org/1999/xhtml" name="creator" content="{$author/text()}"/>
};


declare variable $config:project :=
  let $value:= try { request:get-url() => substring-after("SADE/") => substring-before("/") } catch * { "textgrid" }
  return if($value="") then "textgrid" else $value;
declare variable $config:configDoc := doc( $config-params:projects-dir || $config:project || "/config.xml" );
declare variable $config:configMap := map:new(
                                for $param in $config:configDoc/config/*[@key]
                                return map:entry(string($param/@key), string($param))
                            );

declare function config:get($key as xs:string) as xs:string {
let $return :=
    switch ($key)
        case "data-dir" return $config-params:projects-dir || $config:project || "/" || $config:configMap($key)
        case "project-dir" return $config-params:projects-dir || $config:project
        default return $config:configMap($key)

return
    if(string($return) != "")
    then $return
    else
        let $doc := doc( $config-params:projects-dir || $config:project || "/config.xml" )
        let $map := map:new(for $param in $doc/config/*[@key] return map:entry(string($param/@key), string($param)))
        return
            switch ($key)
                case "data-dir" return $config-params:projects-dir || $config:project || "/" || $map($key)
                case "project-dir" return $config-params:projects-dir || $config:project
                default return $map($key)

};

declare function config:keys() {
    map:keys($config:configMap)
};

declare function config:module-get($module-name as xs:string, $key as xs:string) as item()? {
    let $module := $config:configDoc//module[string(@key)=$module-name]
    return
        $module/param[ string(@key)=$key ]/node()
};

declare function config:module-get($module-name as xs:string, $key as xs:string, $default as item()?) as item()? {
    let $value := config:module-get($module-name , $key)
    return if (empty($value)) then
            $default
        else
            $value
};

(:~ checks if there is a config-file for given project
 @author SADE-Team
 @param $project project identifier
:)
declare function config:project-exists($projectname as xs:string) as xs:boolean {
    let $project-config-path := $config-params:projects-dir || $project || "/config.xml"
       return doc-available($project-config-path)
};

(:~ lists all existing modules (i.e. present in the modules-collection) :)
declare function config:list-modules() {
    let $modules-dir := //expkg:package[@abbrev="SADE"]/base-uri() => replace("expath\-pkg\.xml", "modules/")
    return
       xmldb:get-child-collections($modules-dir)
};

(:~ lists all existing modules (i.e. present in the modules-collection) :)
declare function config:list-projects() as xs:string+ {
   xmldb:get-child-collections($config-params:projects-dir)
};

(:~
 : STATUS: UNCLEAR! COMES FROM OLDE SADE! -- Returns the scripts and links
 : required by the modules as configured in the project-config  either put code
 : directly into `<container key="html-head">`  or if a module is mentioned in
 : the config, its config is checked for <container key="html-head" >
 :)
declare function config:html-head($node as node(), $model as map(*)) {
    let $head := $model("config")//container[@key='html-head']
    return $head/*
};
