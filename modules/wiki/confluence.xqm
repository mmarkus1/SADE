xquery version "3.1";

module namespace confluence="http://textgrid.de/ns/SADE/wiki";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace app="http://textgrid.de/ns/SADE/templates" at "app.xql";

declare namespace http="http://expath.org/ns/http-client";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare function confluence:confluence($node as node(), $model as map(*), $id as xs:string) as node()* {

let $lastRevinDb := local:confluenceGetLastRevInDb($id)
let $revisionInWiki := local:confluenceNewModification($id, $model, $lastRevinDb)

(: Only for debugging, needs to be commented out for productive state :)
(: let $lastRevinDb := '0' :)

return
if( $revisionInWiki = $lastRevinDb )
    then (
        (:<div>revisionInWiki: {$revisionInWiki} ID: {$id} : {string(local:confluenceGetLastRevInDb($id))}</div>:)
        let $lang := app:getLanguage(),
            $result := doc('/sade-projects/textgrid/data/xml/confluence/' || $id || '_' || $lang || '.rev' || local:confluenceGetLastRevInDb($id) || '.xml' ),
            $console := console:log('returning result of revision=' || local:confluenceGetLastRevInDb($id) || ' with lang=' || $lang || ' :' || $result)
        return $result
    )
    else (
        let $conso := console:log('RevWiki=' || $revisionInWiki || ', RevDB=' || $lastRevinDb)
        let $url:= xs:anyURI(config:get("confluence.url") || $id || '?expand=body.storage'),
            $persist := false(),
            $request-headers:= <headers></headers>,
            $export := httpclient:get(xs:anyURI($url), false(), $request-headers),
            $map := parse-json ( util:base64-decode(
            string(xs:base64Binary($export//httpclient:body/text())) ) ),
            $result := map:get(map:get(map:get($map, "body")[1], "storage"), "value"),
            $console := console:log('getting new input of confluence:' || $result),

            $result := confluence:replaceParsingErrors($result),
            $result := '<ac:confluence xmlns:ac="http://www.atlassian.com/schema/confluence/4/ac/" xmlns:ri="http://www.atlassian.com/schema/confluence/4/ri/" xmlns="http://www.atlassian.com/schema/confluence/4/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.atlassian.com/schema/confluence/4/ac/ ">' || $result || "</ac:confluence>",
            $result := parse-xml( $result ),
            $result := local:remove-element-ns-deep( $result ),
            $result := <div id="dokuwiki">{local:confluenceparser($result/*, $id)}</div>,

            $login := xmldb:login( '/sade-projects/textgrid/data/xml/confluence/', config:get('sade.user') , config:get("sade.password")),
            $allLanguages := app:getAllLanguages(),
            $forLoop := for $el in $allLanguages
                let $resultLangSpecific := <div id="dokuwiki">{local:confluenceparseLanguage($result/*, $el)}</div>
                let $store := xmldb:store( '/sade-projects/textgrid/data/xml/confluence/', $id || '_' || $el || '.rev' || $revisionInWiki || '.xml' , $resultLangSpecific )
                return (),

            $lang := app:getLanguage(),
            $result := <div id="dokuwiki">{local:confluenceparseLanguage($result/*, $lang)}</div>,
            $console := console:log('returning result of new revision with lang=' || $lang || ' :' || $result)
        return ($result)
    )

};

declare function local:confluenceparseLanguage($nodes as node()*, $lang){

for $node in $nodes return
    typeswitch($node)
        case element ( structured-macro ) return
        element xhtml:div {
            if(matches($node/@name, "^[a-zA-Z]")) then (
                if ( string($node/@name) = string("english") ) then
                    (if ($lang = "en") then (local:confluenceparseLanguage($node/rich-text-body/node(), $lang))                            else ())
                else if ( $node/@name = "german" ) then
                    (if ($lang = "de") then (local:confluenceparseLanguage($node/rich-text-body/node(), $lang))                            else ())
                else if ( $node/@name = "french" ) then
                    (if ($lang = "fr") then (local:confluenceparseLanguage($node/rich-text-body/node(), $lang))                            else ())
                else
                    ()
            )
            else ()
        }
        case element( * ) return
            element {local-name($node)} {
                $node/@*,
                local:confluenceparseLanguage($node/node(), $lang)
            }
        case text() return $node
        default return local:confluenceparseLanguage($node/node(), $lang)
};

declare function local:confluenceparser($nodes as node()*, $id){
let $lang := string(request:get-parameter("lang", "bla"))

for $node in $nodes return
    typeswitch($node)
    (:
        case element (p) return
            element xhtml:p {
                attribute class {"lead"},
                for $att in $node/@*
                    let $att-name := name($att)
                    return if ($att-name != "class") then attribute {$att-name} {$att}
                    else (),
                local:confluenceparser($node/node(), $id)
            }:)
        case element ( image ) return
            element xhtml:img {
                attribute src { "https://wiki.de.dariah.eu/download/attachments/" || $id || "/" || ($node/attachment/@filename) },
                attribute alt { ($node/@title) },
                attribute height { ($node/@height) }
                (: attribute width { ($node/@width) } ---->: Width can't be set in Confluence! :)
            }
        (:Transform heading h1 to the theme specific style:)
        case element ( h1 ) return
            element xhtml:h1 {
            attribute class {"block-header"},
                <span>{$node/text()}</span>
            }
        case element ( h2 ) return
            element xhtml:h2 {
            attribute class {"block-header alt"},
              <span>{$node/text()}</span>
            }
        (: Structured Macros of Confluence needs to be transformed :)
        case element ( structured-macro ) return
            if(matches($node/@name, "^[a-zA-Z]")) then (
                if ( string($node/@name) = string("info") ) then (
                    element xhtml:div {
                        attribute class {"info-board info-board-primary"},
                        local:confluenceparser($node/rich-text-body/node(), $id)
                    })
                else if ( string($node/@name) = string("warning") ) then (
                    element xhtml:div {
                        attribute class {"alert alert-danger"},
                        local:confluenceparser($node/rich-text-body/node(), $id)
                    }
                )
                else if ( string($node/@name) = string("tip") ) then (
                    element xhtml:div {
                        attribute class {"alert alert-success"},
                        local:confluenceparser($node/rich-text-body/node(), $id)
                    }
                )
                else if ( string($node/@name) = string("code") ) then (
                    element xhtml:code {
                        local:confluenceparser($node/plain-text-body/node(), $id)
                    }
                )
                else (
                    element {local-name($node)} {
                        $node/@*,
                        local:confluenceparser($node/node(), $id)
                    }
                )
            )
            else ()
        (: Edit links to open external ones in a new tab :)
        case element ( a ) return
            element a {
                if (starts-with($node/@href,"/")) then (
                    attribute href {$node/@href},
                    $node/text())
                else (
                    (:attribute href {$node/@href},:)
                    attribute target {"_blank"},
                    $node/@*,
                    local:confluenceparser($node/node(), $id))
            }
        case element( * ) return
            element {local-name($node)} {
                $node/@*,
                local:confluenceparser($node/node(), $id)
            }
        case text() return $node
        default return local:confluenceparser($node/node(), $id)
};

declare function local:passthru($node as node()*, $id) as item()* {
    element {name($node)} {($node/@*, local:confluenceparser($node/node(), $id))}
};

(:  not needed anymore, but could be handy for further usage :)
declare function local:retrieveImageUrl($pageId, $imageName) {
       let $url:= xs:anyURI('https://wiki.de.dariah.eu/rest/api/content/' || $pageId || '/child/attachment?filename=' || encode-for-uri($imageName)),
            $persist := false(),
            $request-headers:= <headers></headers>,
            $export := httpclient:get(xs:anyURI($url), false(), $request-headers),
            $map := parse-json ( util:base64-decode(
            string(xs:base64Binary($export//httpclient:body/text())) ) ),
            $imageurl := $map("results")(1)("_links")("download")
        return 'https://wiki.de.dariah.eu/' || $imageurl
};

declare function local:confluenceGetLastRevInDb($id ) {
    let $loop := for $item in xmldb:get-child-resources('/sade-projects/textgrid/data/xml/confluence/')
        where substring-before(substring-before($item, '.rev'), '_') = $id
            let $rev := substring-before(substring-after($item, '.rev'), '.xml')
            return $rev
    return $loop[1]
};

declare function local:confluenceNewModification($id, $model, $lastRevinDb) as xs:string {
let $lastRevinWiki := local:getLatestRevision($id)

let $test :=
    if( $lastRevinDb != $lastRevinWiki )
    then
        (
            let $login := xmldb:login( '/sade-projects/textgrid/data/xml/confluence/', config:get('sade.user'), config:get("sade.password")),
            $allLanguages := app:getAllLanguages(),
            $forLoop := for $el in $allLanguages
                let $temp := xmldb:remove('/sade-projects/textgrid/data/xml/confluence/', $id || '_' || $el || '.rev' || $lastRevinDb || '.xml' )
                return ()
            return ()
        )
    else ()
return
    $lastRevinWiki
};

declare function local:HTML2html($node) {
for $node in $node
return
    typeswitch ( $node )
    case element() return element { lower-case($node/local-name()) } { $node/@*, local:HTML2html($node/node()) }
    default return $node
};

declare function local:getLatestRevision($id) as xs:string {

    let $headers := <headers></headers>

    let $rest :=
    httpclient:get(xs:anyURI(config:get("confluence.url")||$id||"/history?expand=lastUpdated"),
    false(), $headers)

    let $map := parse-json ( util:base64-decode(
    string(xs:base64Binary($rest//httpclient:body/text())) ) )

    let $revision := string(map:get (map:get($map, "lastUpdated"), "number"))
    return $revision

};

declare function local:remove-element-ns-deep( $nodes as node()*)  as node()* {
  for $node in $nodes
  return if ($node instance of element())
         then (element
               {QName ("", local-name($node))}
               {
                   for $attr in $node/@*
                   return
                       attribute { local-name($attr) } { string($attr) },
                   local:remove-element-ns-deep($node/node())})
         else if ($node instance of document-node())
         then local:remove-element-ns-deep($node/node())
         else $node
};

declare function confluence:replaceParsingErrors($result) {
    (: Strip HTML comments, causes sometimes error :)
    let $temp := replace($result, "<!--.*?-->", ""),
    (: two dashes inside ac macros also causes an error, needs better fix :)
    $temp := replace($temp, "--", "&#xFE58;&#xFE58;"),
    (: Returns cause errors :)
    $temp := replace($temp, "&amp;nbsp;", " "),
    (: ß cause errors :)
    $temp := replace($temp, "&amp;szlig;","ß"),
    (: Umlauts cause errors :)
    $temp := replace($temp, "&amp;uuml;", "ü"),
    $temp := replace($temp, "&amp;auml;", "ä"),
    $temp := replace($temp, "&amp;ouml;", "ö"),
    $temp := replace($temp, "&amp;Uuml;", "Ü"),
    $temp := replace($temp, "&amp;Auml;", "Ä"),
    $temp := replace($temp, "&amp;Ouml;", "Ö"),
    (: French special chars cause errors :)
    $temp := replace($temp, "&amp;eacute;","é"),
    $temp := replace($temp, "&amp;egrave;","è"),
    $temp := replace($temp, "&amp;Egrave;","È"),
    $temp := replace($temp, "&amp;Eacute;","É"),
    $temp := replace($temp, "&amp;yuml;","ù"),
    $temp := replace($temp, "&amp;Yuml;","Û"),
    $temp := replace($temp, "&amp;ucirc;","û"),
    $temp := replace($temp, "&amp;Ucirc;","Û"),
    $temp := replace($temp, "&amp;Ecirc;","Ê"),
    $temp := replace($temp, "&amp;ecirc;","ê"),
    $temp := replace($temp, "&amp;Euml;","Ë"),
    $temp := replace($temp, "&amp;euml;","ë"),
    $temp := replace($temp, "&amp;Icirc;","Î"),
    $temp := replace($temp, "&amp;icirc;","î"),
    $temp := replace($temp, "&amp;Iuml;","Ï"),
    $temp := replace($temp, "&amp;iuml;","ï"),
    $temp := replace($temp, "&amp;Ocirc;","Ô"),
    $temp := replace($temp, "&amp;ocirc;","ô"),
    $temp := replace($temp, "&amp;OElig;","Œ"),
    $temp := replace($temp, "&amp;oelig;","œ"),
    $temp := replace($temp, "&amp;Ugrave;","Ù"),
    $temp := replace($temp, "&amp;ugrave;","ù"),
    $temp := replace($temp, "&amp;Agrave;","À"),
    $temp := replace($temp, "&amp;agrave;","à"),
    $temp := replace($temp, "&amp;Acirc;","Â"),
    $temp := replace($temp, "&amp;acirc;","â"),
    $temp := replace($temp, "&amp;AElig;","Æ"),
    $temp := replace($temp, "&amp;aelig;","æ"),
    $temp := replace($temp, "&amp;Ccedil;","Ç"),
    $temp := replace($temp, "&amp;ccedil;","ç"),

    (:Other special characters:)
    $temp := replace($temp, "&amp;rsquo;","’"),
    $temp := replace($temp, "&amp;lsquo;","‘"),
    $temp := replace($temp, "&amp;ndash;","-"),
    $temp := replace($temp, "&amp;mdash;","—"),
    $temp := replace($temp, "&amp;laquo;","«"),
    $temp := replace($temp, "&amp;raquo;","»"),
    $temp := replace($temp, "&amp;ldquo;","“"),
    $temp := replace($temp, "&amp;rdquo;","”")

    return $temp
};
