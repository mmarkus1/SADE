(:~
 : This is the main XQuery which will (by default) be called by controller.xql
 : to process any URI ending with ".html". It receives the HTML from
 : the controller and passes it to the templating system.
 :)
xquery version "3.1";

import module namespace app="http://textgrid.de/ns/SADE/templates" at "app.xql";
import module namespace templates="http://exist-db.org/xquery/templates" ;
import module namespace mviewer="http://sade/multiviewer" at "multiviewer/multiviewer.xqm" ;
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace f-misc="http://fontane-nb.dariah.eu/ns/SADE/misc" at "fontane/misc.xqm";
import module namespace f-render="http://fontane-nb.dariah.eu/ns/SADE/render" at "fontane/render.xqm";
import module namespace config="http://textgrid.de/ns/SADE/config" at "config/config.xqm";
import module namespace fsearch = "http://sade/faceted-search" at "faceted-search/faceted-search.xqm";
import module namespace nav="http://sade/tmpl-nav" at "tmpl-nav/tmpl-nav.xqm";
import module namespace lang="http://sade/lang" at "lang/lang.xqm";
import module namespace confluence="http://textgrid.de/ns/SADE/wiki" at "wiki/confluence.xqm";
import module namespace blog="http://sade/blog" at "lang/blog.xqm";

declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "html5";
declare option output:media-type "text/html";


declare function local:linkrewrite($nodes as node()*) as node()* {
for $node in $nodes
where not( $node instance of attribute() )
return
typeswitch ( $node )
case element( a ) return
    if(matches($node/@href, "^[/a-zA-Z]")) then
        element xhtml:a { $node/@*[local-name(.) != "href"],
            attribute href { $node/@href || (if (contains($node/@href, "?")) then "&amp;" else "?") ||"lang=" || request:get-parameter("lang", "blubb") },
            local:linkrewrite($node/node())
        }
        else
            element xhtml:a {
                $node/@*,
                local:linkrewrite($node/node())
            }
    case text() return $node
    case comment() return $node
    case processing-instruction() return $node
    default return
    element {QName("http://www.w3.org/1999/xhtml", local-name($node))} {
        $node/@*,
        local:linkrewrite($node/node())
    }
};


let $config := map {
    $templates:CONFIG_APP_ROOT : $config:app-root,
    $templates:CONFIG_STOP_ON_ERROR : true()
}

(:
 : We have to provide a lookup function to templates:apply to help it
 : find functions in the imported application modules. The templates
 : module cannot see the application modules, but the inline function
 : below does see them.
 :)
let $lookup := function($functionName as xs:string, $arity as xs:int) {
    try {
        function-lookup(xs:QName($functionName), $arity)
    } catch * {
        ()
    }
}
(:
 : The HTML is passed in the request from the controller.
 : Run it through the templating system and return the result.
 :)

let $project-path := request:get-url() => substring-after("/SADE/") => tokenize("/")
let $project := $project-path[1]
let $resource := $project-path[last()]
let $doc-path := $config:projects-dir || $project || config:get("template") || $resource

let $content := doc( $doc-path )

return
    local:linkrewrite(templates:apply($content, $lookup, (), $config))
