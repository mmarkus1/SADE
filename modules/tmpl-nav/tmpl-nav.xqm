xquery version "3.1";
module namespace nav = "http://sade/tmpl-nav" ;

import module namespace console="http://exist-db.org/xquery/console";
import module namespace templates="http://exist-db.org/xquery/templates";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace app="http://textgrid.de/ns/SADE/templates" at "../app.xql";


(: TODO: Nav ohne Link :)

declare variable $nav:module-key := "tmpl-nav";
declare variable $nav:lang := app:getLanguage();
declare variable $nav:langDefault := (config:get("lang.default"));
declare variable $nav:langTest := ($nav:lang = $nav:langDefault);
declare
    %templates:wrap
function nav:navitems($node as node(), $model as map(*)) as map(*) {

    let $confLocation := config:module-get($nav:module-key, "location")

    let $navitems :=
        if($confLocation != "") then
            doc( config:get('project-dir') || "/" || $confLocation)//navigation/*
        else
            $model("config")//module[string(@key)=$nav:module-key]//navigation/*
    return map { "navitems" := $navitems }

};

declare function nav:head($node as node(), $model as map(*)) {

    switch(local-name($model("item")))
        case "submenu" return
            element { node-name($node) } {
                    $node/@*,
                    if ($nav:langTest) then (string($model("item")/@label))
                    else string($model("item")/@*[local-name( . ) = "label-" || $nav:lang]),
                    for $child in $node/node() return $child
            }
        case "item" return
            element { node-name($node) } {
                    attribute href {$model("item")/@link},
                    if ($nav:langTest) then string($model("item")/@label)
                    else string($model("item")/@*[local-name( . ) = "label-" || $nav:lang])
            }
        default return
            <b>not defined: {node-name($model("item"))}</b>
};

declare
    %templates:wrap
function nav:subitems($node as node(), $model as map(*)) as map(*) {
    map{ "subitems" := $model("item")/*}
};

declare
function nav:subitem($node as node(), $model as map(*)) {
    if($model("subitem")/@class) then
        <span class="{$model("subitem")/@class}">&#160;</span>
    else if ($model("subitem")/name() != 'divider') then
        element a {
            if(string($model("subitem")/@link)) then attribute href { string($model("subitem")/@link)} else (),
            if ($nav:langTest) then string($model("subitem")/@label)
            else string($model("subitem")/@*[local-name( . ) = "label-" || $nav:lang])
        }
        else <span>&#160;</span>
};
(: no call for subitemchoice...
declare
function nav:subitemchoice($node as node(), $model as map(*)) {

    let $item := $model("subitem")
    let $type := local-name($item)
    let $choosen := $node/*[@data-nav-choice=$type]

    return
    switch($type)
        case "submenu" return
            <span>todo</span>
        case "item" return
            element { node-name($choosen) } {
                $choosen/@* , <a href="{string($item/@link)}">{string($item/@label)}</a>
            }
        case "divider" return
            element { node-name($choosen) } {
                $choosen/@*
            }
        default return
            <b>not defined: {node-name($model("subitem"))}</b>

};
:)
declare function nav:edit($node as node(), $model as map(*)) {
let $new := request:get-parameter("new", "0")
return
    if (string($new) eq '0')
    then
        <div>
            <form class="form-horizontal" action="edit.html" method="GET">
                <textarea class="form-control" rows="10" name="new" />
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
            <pre>{serialize(doc('/sade-projects/' || config:get("project-id") || '/navigation.xml'))}</pre>
        </div>
    else
        let $item := xmldb:login('/sade-projects/' || config:get("project-id"), config:get("sade.user"), config:get("sade.password")),
            $egal := xmldb:store('/sade-projects/' || config:get("project-id"), 'navigation.xml', $new, "text/xml")
        return
        <div><pre>{serialize(doc('/sade-projects/' || config:get("project-id") || '/navigation.xml'))}</pre></div>
};

declare function nav:textgrid($node as node(), $model as map(*)) {
doc('/sade-projects/' || config:get("project-id") || '/navigation-'|| config:get("template") ||'.xml')
};
